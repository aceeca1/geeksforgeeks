#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<string> a(n);
        for (int i = 0; i < n; ++i) {
            char s[11];
            scanf("%s", s);
            a[i] = s;
        }
        sort(a.begin(), a.end(), [&](const string& s1, const string& s2) {
            return s1 + s2 > s2 + s1;
        });
        for (int i = 0; i < n; ++i) printf("%s", a[i].c_str());
        printf("\n");
    }
    return 0;
}
