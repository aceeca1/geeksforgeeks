#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        for (int i = 0; i < n; ++i) if (a[i] >= 0) {
            int ai = a[i], j = i;
            while (a[j] >= 0 && a[a[j]] >= 0) {
                int jNext = a[j];
                a[j] = ~a[a[j]];
                j = jNext;
            }
            a[j] = ~ai;
        }
        for (int i = 0; i < n; ++i) a[i] = ~a[i];
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
