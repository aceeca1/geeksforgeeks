#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int ans = 0;
        for (int i = 0; i < 32; ++i) {
            int zero = 0, one = 0;
            for (int j = 0; j < n; ++j)
                if ((a[j] >> i) & 1) ++one;
                else ++zero;
            ans += zero * one;
        }
        printf("%d\n", ans << 1);
    }
    return 0;
}
