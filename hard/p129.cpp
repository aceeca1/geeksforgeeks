#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), b;
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            if (b.empty() || a[i] < a[b.back()]) b.emplace_back(i);
            auto p = *lower_bound(b.begin(), b.end(), i, [&](int i1, int i2) {
                return a[i1] > a[i2];
            });
            int t = i - p;
            if (t > ans) ans = t;
        }
        printf("%d\n", ans);
    }
    return 0;
}
