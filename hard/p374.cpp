// Almost the same as p892
#include <cstdio>
#include <cstdint>
#include <cmath>
#include <vector>
using namespace std;

bool head;

void sieve(const vector<int>& a, vector<int>& b, int sh) {
    for (int i = 2; i < a.size(); ++i) if (!a[i]) {
        int j = i * i;
        int j1 = (sh + i - 1) / i * i;
        if (j1 > j) j = j1;
        for (; j < b.size() + sh; j += i) b[j - sh] = true;
    }
    if (&a == &b) return;
    for (int i = 0; i < b.size(); ++i) if (!b[i]) {
        if (i + sh == 1) continue;
        if (!head) putchar(' ');
        head = false;
        printf("%d", i + sh);
    }
}

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> l(t), r(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d%d", &l[ti], &r[ti]);
        if (r[ti] > m) m = r[ti];
    }
    m = sqrt(m);
    vector<int> a(m + 1);
    sieve(a, a, 0);
    for (int ti = 0; ti < t; ++ti) {
        vector<int> b(r[ti] - l[ti] + 1);
        head = true;
        sieve(a, b, l[ti]);
        printf("\n");
    }
    return 0;
}
