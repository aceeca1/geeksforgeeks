#include <cstdio>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        unordered_map<int, int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            ++a[b];
        }
        vector<pair<int, int>> c(a.begin(), a.end());
        sort(c.begin(), c.end(),
            [&](const pair<int, int> p1, const pair<int, int> p2) {
                return p1.second > p2.second ||
                    p1.second == p2.second && p1.first < p2.first;
            });
        bool head = true;
        for (auto& i: c)
            for (int j = 0; j < i.second; ++j) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", i.first);
            }
        printf("\n");
    }
    return 0;
}
