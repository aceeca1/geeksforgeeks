#include <cstdio>
#include <cstring>
using namespace std;

int e[26][26], v[26];

void visit(int no) {
    v[no] = 0;
    for (int i = 0; i < 26; ++i)
        if (v[i] && (e[no][i] || e[i][no])) visit(i);
}

bool is() {
    memset(v, 0, sizeof(v));
    int a1 = 0, a2 = 0;
    for (int i = 0; i < 26; ++i) {
        int b = 0;
        for (int j = 0; j < 26; ++j) {
            v[i] += e[i][j] + e[j][i];
            b += e[i][j] - e[j][i];
        }
        switch (b) {
            case 0: break;
            case -1: ++a1; break;
            case 1: ++a2; break;
            default: return false;
        }
    }
    if (a1 > 1 || a2 > 1) return false;
    for (int i = 0; i < 26; ++i) if (v[i]) {
        visit(i);
        break;
    }
    for (int i = 0; i < 26; ++i)
        if (v[i]) return false;
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        memset(e, 0, sizeof(e));
        int n;
        scanf("%d", &n);
        while (n--) {
            char s[11];
            scanf("%s", s);
            ++e[s[0] - 'a'][s[strlen(s) - 1] - 'a'];
        }
        const char *yes = "Head to tail ordering is possible.";
        const char *no = "Head to tail ordering is not possible.";
        printf("%s\n", is() ? yes : no);
    }
    return 0;
}
