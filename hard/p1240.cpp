#include <cstdio>
#include <vector>
#include <string>
using namespace std;

constexpr int M = 1003;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        n = (n + 1) >> 1;
        vector<vector<int>> aT(n), aF(n);
        for (int i = n - 1; i >= 0; --i) {
            aT[i].resize(n + 1 - i);
            aF[i].resize(n + 1 - i);
            if (s[i + i] == 'T') {
                aT[i][1] = 1;
                aF[i][1] = 0;
            } else {
                aT[i][1] = 0;
                aF[i][1] = 1;
            }
            for (int j = 2; j <= n - i; ++j)
                for (int k = 1; k < j; ++k) {
                    int t1 = aT[i][k] + aF[i][k];
                    int t2 = aT[i + k][j - k] + aF[i + k][j - k];
                    int t3 = t1 * t2;
                    switch (s[((i + k) << 1) - 1]) {
                        case '&': {
                            int u = aT[i][k] * aT[i + k][j - k];
                            aT[i][j] = (aT[i][j] + u) % M;
                            aF[i][j] = (aF[i][j] + t3 - u) % M;
                        } break;
                        case '|': {
                            int u = aF[i][k] * aF[i + k][j - k];
                            aF[i][j] = (aF[i][j] + u) % M;
                            aT[i][j] = (aT[i][j] + t3 - u) % M;
                        } break;
                        case '^': {
                            int u1 = aT[i][k] * aF[i + k][j - k];
                            int u2 = aF[i][k] * aT[i + k][j - k];
                            aT[i][j] = (aT[i][j] + u1 + u2) % M;
                            aF[i][j] = (aF[i][j] + t3 - (u1 + u2)) % M;
                        }
                    }
                }
        }
        printf("%d\n", aT[0][n]);
    }
    return 0;
}
