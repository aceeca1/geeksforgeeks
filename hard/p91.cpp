#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int r, c;
        scanf("%d%d", &r, &c);
        vector<vector<int>> a(r, vector<int>(c));
        for (int i = 0; i < r; ++i)
            for (int j = 0; j < c; ++j)
                scanf("%d", &a[i][j]);
        for (int i = a.size() - 1; i >= 0; --i)
            for (int j = a[0].size() - 1; j >= 0; --j) {
                int ans = 0x7fffffff;
                if (j < a[0].size() - 1) {
                    int k = a[i][j + 1];
                    if (k < ans) ans = k;
                }
                if (i < a.size() - 1) {
                    int k = a[i + 1][j];
                    if (k < ans) ans = k;
                }
                if (ans == 0x7fffffff) ans = 1;
                ans -= a[i][j];
                if (ans < 1) ans = 1;
                a[i][j] = ans;
            }
        printf("%d\n", a[0][0]);
    }
    return 0;
}
