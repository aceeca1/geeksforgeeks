#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a{true};
        while (n--) {
            int b;
            scanf("%d", &b);
            a.resize(a.size() + b);
            for (int i = a.size() - 1; i >= b; --i) a[i] |= a[i - b];
        }
        int i = (a.size() - 1) >> 1;
        while (!a[i]) --i;
        printf("%d\n", int(a.size()) - 1 - i - i);
    }
    return 0;
}
