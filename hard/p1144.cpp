#include <cstdio>
#include <vector>
using namespace std;

// Snippet: powM
long long powM(long long a, long long b, long long m) {
    long long ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % m;
        a = a * a % m;
    }
    return ans;
}

// Snippet: NCrM
namespace NCrM {
    typedef long long LL;

    struct FactorialM {
        LL m, p, a, ap;
        vector<LL> fac;

        FactorialM(LL m_, LL p_): m(m_), p(p_), fac(m_) {
            fac[0] = 1;
            for (LL i = 1; i < m; ++i)
                if (i % p) fac[i] = fac[i - 1] * i % m;
                else fac[i] = fac[i - 1];
        }

        void operator()(LL n) {
            a = 1;
            ap = 0;
            while (n) {
                a = a * fac[n % m] % m * powM(fac[m - 1], n / m, m) % m;
                n /= p;
                ap += n;
            }
        }
    };

    LL nCrMP(LL n, LL r, LL m, LL p) {
        FactorialM fm(m, p);
        fm(r);
        LL b = fm.a, bp = fm.ap;
        fm(n - r);
        b = b * fm.a % m;
        bp += fm.ap;
        fm(n);
        LL c = fm.a * powM(b, m / p * (p - 1) - 1, m) % m;
        return c * powM(p, fm.ap - bp, m) % m;
    }

    struct ReMo {
        LL re, mo;
        ReMo(LL re_ = 0, LL mo_ = 1): re(re_), mo(mo_) {}
        void times(const ReMo& that, LL p) {
            LL a = powM(mo, that.mo / p * (p - 1), mo * that.mo);
            mo *= that.mo;
            re = (that.re * a + re * (mo + 1 - a)) % mo;
        }
    };

    LL nCrM(LL n, LL r, LL m) {
        ReMo ans;
        for (LL i = 2; i * i <= m; ++i) if (!(m % i)) {
            LL cmo = 1;
            while (!(m % i)) { cmo *= i; m /= i; }
            ans.times(ReMo{nCrMP(n, r, cmo, i), cmo}, i);
        }
        if (m > 1) ans.times(ReMo{nCrMP(n, r, m, m), m}, m);
        return ans.re;
    }
};

using NCrM::nCrM;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        long long n, r, u;
        scanf("%lld%lld%lld", &n, &r, &u);
        printf("%lld\n", nCrM(n, r, u));
    }
    return 0;
}
