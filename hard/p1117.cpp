#include <cstdio>
#include <cmath>
#include <utility>
using namespace std;

double a[3][4];
int b[3];

int gauss() {
    for (int i = 0; i < 3; ++i) {
        double max = -1.0 / 0.0;
        int j_max, k_max;
        for (int j = i; j < 3; ++j)
            for (int k = 0; k < 3; ++k) if (b[k] == -1) {
                double t = a[j][k];
                if (t < 0) t = -t;
                if (t > max) {
                    max = t;
                    j_max = j;
                    k_max = k;
                }
            }
        if (max < 1e-12) {
            for (int j = i; j < 3; ++j)
                if (a[j][3] < -1e-12 || 1e-12 < a[j][3]) return 0;
            return 1;
        }
        for (int j = 0; j < 4; ++j) swap(a[i][j], a[j_max][j]);
        double c = a[i][k_max];
        for (int j = 0; j < 4; ++j) a[i][j] /= c;
        for (int j = 0; j < 3; ++j) if (j != i) {
            c = a[j][k_max];
            for (int k = 0; k < 4; ++k) a[j][k] -= c * a[i][k];
        }
        b[k_max] = i;
    }
    return -1;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 4; ++j)
                scanf("%lf", &a[i][j]);
        for (int i = 0; i < 3; ++i) b[i] = -1;
        int u = gauss();
        if (u != -1) printf("%d\n", u);
        else printf("%.0f %.0f %.0f\n",
            floor(a[b[0]][3]), floor(a[b[1]][3]), floor(a[b[2]][3]));
    }
    return 0;
}
