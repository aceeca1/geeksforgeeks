// The same as p960
#include <cstdio>
#include <stack>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11000];
        scanf("%s", s);
        stack<int> st;
        st.emplace(-1);
        int ans = 0;
        for (int i = 0; s[i]; ++i)
            if (s[i] == '(') st.emplace(i);
            else {
                st.pop();
                if (st.empty()) st.push(i);
                else {
                    int a = i - st.top();
                    if (a > ans) ans = a;
                }
            }
        printf("%d\n", ans);
    }
    return 0;
}
