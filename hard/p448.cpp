#include <cstdio>
#include <vector>
using namespace std;

vector<int> a;
int n;

bool pack(int k, int m) {
    int b = 0;
    for (int i = 0; i < n; ++i) {
        if (a[i] > k) return false;
        if (b + a[i] > k) {
            if (--m <= 0) return false;
            b = 0;
        }
        b += a[i];
    }
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int m, l = 0, r = 0;
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            r += a[i];
        }
        scanf("%d", &m);
        if (m > n) { printf("-1\n"); continue; }
        while (l < r) {
            int mid = (l + r) >> 1;
            if (pack(mid, m)) r = mid;
            else l = mid + 1;
        }
        printf("%d\n", l);
    }
    return 0;
}
