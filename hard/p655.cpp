#include <cstdio>
#include <vector>
using namespace std;

struct Edge {
    int t, c;
};

vector<vector<Edge>> out;
vector<int> v;

void put(int no, int k) {
    if (k <= 0) throw true;
    v[no] = true;
    for (Edge &i: out[no]) if (!v[i.t]) put(i.t, k - i.c);
    v[no] = false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, k;
        scanf("%d%d%d", &n, &m, &k);
        out.resize(n);
        for (int i = 0; i < n; ++i) out[i].clear();
        while (m--) {
            int s, t, c;
            scanf("%d%d%d", &s, &t, &c);
            out[s].emplace_back(Edge{t, c});
            out[t].emplace_back(Edge{s, c});
        }
        v.resize(n);
        for (int i = 0; i < n; ++i) v[i] = false;
        bool can = false;
        try { put(0, k); }
        catch (bool e) { can = true; }
        printf("%d\n", can);
    }
    return 0;
}
