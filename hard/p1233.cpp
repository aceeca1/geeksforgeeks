#include <cstdio>
#include <vector>
#include <string>
#include <queue>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, ans = 0;
        scanf("%d%d", &n, &m);
        vector<string> a(n);
        for (int i = 0; i < n; ++i) {
            char s[55];
            scanf("%s", s);
            a[i] = s;
        }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j) {
                if (a[i][j] != 'X') continue;
                ++ans;
                queue<pair<int, int>> q;
                q.emplace(i, j);
                a[i][j] = 'O';
                while (!q.empty()) {
                    auto x = q.front().first;
                    auto y = q.front().second;
                    q.pop();
                    if (x && a[x - 1][y] == 'X') {
                        q.emplace(x - 1, y);
                        a[x - 1][y] = 'O';
                    }
                    if (y && a[x][y - 1] == 'X') {
                        q.emplace(x, y - 1);
                        a[x][y - 1] = 'O';
                    }
                    if (x + 1 < n && a[x + 1][y] == 'X') {
                        q.emplace(x + 1, y);
                        a[x + 1][y] = 'O';
                    }
                    if (y + 1 < m && a[x][y + 1] == 'X') {
                        q.emplace(x, y + 1);
                        a[x][y + 1] = 'O';
                    }
                }
            }
        printf("%d\n", ans);
    }
    return 0;
}
