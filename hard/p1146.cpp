#include <cstdio>
#include <cstdint>
#include <vector>
using namespace std;

constexpr int M = 1000000007;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, y, z;
        scanf("%d%d%d", &x, &y, &z);
        vector<vector<int>> a0(y + 1, vector<int>(z + 1)), a1(a0);
        vector<vector<int>> b0(a0), b1(a0);
        for (int i = 0; i <= x; ++i) {
            for (int j = 0; j <= y; ++j)
                for (int k = 0; k <= z; ++k) {
                    int64_t t1 = 0, t2 = 0;
                    if (i) {
                        t1 += a1[j][k];
                        t2 += int64_t(a1[j][k]) << 2;
                        t2 += int64_t(b1[j][k]) * 10;
                    }
                    if (j) {
                        t1 += a0[j - 1][k];
                        t2 += int64_t(a0[j - 1][k]) * 5;
                        t2 += int64_t(b0[j - 1][k]) * 10;
                    }
                    if (k) {
                        t1 += a0[j][k - 1];
                        t2 += int64_t(a0[j][k - 1]) * 6;
                        t2 += int64_t(b0[j][k - 1]) * 10;
                    }
                    a0[j][k] = (t1 + 1) % M;
                    b0[j][k] = t2 % M;
                }
            swap(a0, a1);
            swap(b0, b1);
        }
        printf("%d\n", b1[y][z]);
    }
    return 0;
}
