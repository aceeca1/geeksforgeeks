#include <cstdio>
#include <vector>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, d;
        scanf("%d%d", &n, &d);
        printf("%d", n / d);
        n %= d;
        if (n) putchar('.');
        string s;
        vector<int> a(d, -1);
        while (n && a[n] == -1) {
            a[n] = s.size();
            n *= 10;
            s += '0' + n / d;
            n %= d;
        }
        if (!n) printf("%s\n", s.c_str());
        else {
            printf("%s", s.substr(0, a[n]).c_str());
            printf("(%s)\n", s.substr(a[n], -1).c_str());
        }
    }
    return 0;
}
