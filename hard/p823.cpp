#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char su[550];
        scanf("%s", su);
        string s(su);
        int k = 0;
        while (k < s.size() && s[k] == 'a') ++k;
        if (k == s.size()) {
            printf("0 0\n");
            continue;
        }
        string sMin = "c";
        int i_min;
        for (int i = k + 1; i < s.size(); ++i) if (s[i] == 'a') {
            string s1 = s.substr(k, i - k + 1);
            reverse(s1.begin(), s1.end());
            s1.append(s, i + 1, -1);
            if (s1 < sMin) { sMin = s1; i_min = i; }
        }
        if (sMin == "c") printf("0 0\n");
        else printf("%d %d\n", k, i_min);
    }
    return 0;
}
