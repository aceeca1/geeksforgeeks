#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

constexpr int M = 1000000007;

struct Matrix {
    int v[3];
    void operator*=(const Matrix& that) {
        auto vv = int64_t(v[1]) * that.v[1];
        v[0] = (int64_t(v[0]) * that.v[0] + vv) % M;
        v[2] = (int64_t(v[2]) * that.v[2] + vv) % M;
        v[1] = (v[0] + M - v[2]) % M;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n;
        scanf("%" SCNd64, &n);
        Matrix a{{1, 1, 0}}, ans{{1, 0, 1}};
        for (; n; n >>= 1) {
            if (n & 1) ans *= a;
            a *= a;
        }
        printf("%d\n", ans.v[1]);
    }
    return 0;
}
