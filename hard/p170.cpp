#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), c(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        for (int i = 0; i < n; ++i) {
            int m = 0x7fffffff;
            for (int j = i; j < n; ++j) {
                if (a[j] < m) m = a[j];
                if (m > c[j - i]) c[j - i] = m;
            }
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", c[i]);
        }
        printf("\n");
    }
    return 0;
}
