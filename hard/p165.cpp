#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        vector<int> b(a);
        sort(b.begin(), b.end());
        int m = unique(b.begin(), b.end()) - b.begin();
        for (int i = 0; i < n; ++i)
            a[i] = lower_bound(b.begin(), b.begin() + m, a[i]) - b.begin();
        m = 1 << (ilogb(m + 2) + 1);
        vector<int> c(m + m);
        for (int i = 0; i < n; ++i) {
            int k = 0;
            for (int j = a[i] + m; j > 1; j >>= 1) {
                if (j & 1) k += c[j - 1];
                ++c[j];
            }
            b[i] = k;
        }
        for (int i = 0; i < m + m; ++i) c[i] = 0;
        int64_t ans = 0;
        for (int i = n - 1; i >= 0; --i) {
            int k = 0;
            for (int j = a[i] + m; j > 1; j >>= 1) {
                if (~j & 1) k += c[j + 1];
                ++c[j];
            }
            ans += int64_t(b[i]) * k;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
