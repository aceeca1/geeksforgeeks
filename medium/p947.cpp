#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int k, n;
        scanf("%d%d", &k, &n);
        vector<int> b(n);
        for (int i = 0; i < n; ++i) scanf("%d", &b[i]);
        vector<int> a(n);
        for (int i = 1; i <= k; ++i) {
            int p = -b[0], a0 = 0, a1;
            for (int j = 1; j < n; ++j) {
                a1 = a0;
                a0 = a[j];
                a[j] = a[j - 1];
                int q = p + b[j];
                if (q > a[j]) a[j] = q;
                q = a1 - b[j];
                if (q > p) p = q;
            }
        }
        printf("%d\n", a.back());
    }
    return 0;
}
