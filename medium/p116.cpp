#include <cstdio>
#include <cstdint>
#include <cstring>
using namespace std;

constexpr int M = 1000003;

char s[22];

int calc() {
    int n = strlen(s), ans = 0;
    for (int i = 0; i < n; ++i) {
        int a = 0;
        for (int j = i + 1; j < n; ++j)
            if (s[j] == s[i]) return 0;
            else if (s[j] < s[i]) ++a;
        ans = (int64_t(ans) * (n - i) % M + a) % M;
    }
    return ans + 1;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        printf("%d\n", calc());
    }
    return 0;
}
