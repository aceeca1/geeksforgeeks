#include <cstdio>
using namespace std;

char a[9];

bool is() {
    int b = 0;
    for (int i = 0; i < 9; ++i)
        if (a[i] == 'X') ++b; else --b;
    return b == 1 && !(
        (a[0] == 'O' && a[1] == 'O' && a[2] == 'O') ||
        (a[3] == 'O' && a[4] == 'O' && a[5] == 'O') ||
        (a[6] == 'O' && a[7] == 'O' && a[8] == 'O') ||
        (a[0] == 'O' && a[3] == 'O' && a[6] == 'O') ||
        (a[1] == 'O' && a[4] == 'O' && a[7] == 'O') ||
        (a[2] == 'O' && a[5] == 'O' && a[8] == 'O') ||
        (a[0] == 'O' && a[4] == 'O' && a[8] == 'O') ||
        (a[2] == 'O' && a[4] == 'O' && a[6] == 'O'));
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        for (int i = 0; i < 9; ++i) scanf(" %c", &a[i]);
        printf("%s\n", is() ? "Valid" : "Invalid");
    }
    return 0;
}
