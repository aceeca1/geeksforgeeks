#include <cstdio>
#include <string>
using namespace std;

void a20(string& s, int num) {
    if (!s.empty()) s += ' ';
    switch (num) {
        case 0: s += "zero"; break;
        case 1: s += "one"; break;
        case 2: s += "two"; break;
        case 3: s += "three"; break;
        case 4: s += "four"; break;
        case 5: s += "five"; break;
        case 6: s += "six"; break;
        case 7: s += "seven"; break;
        case 8: s += "eight"; break;
        case 9: s += "nine"; break;
        case 10: s += "ten"; break;
        case 11: s += "eleven"; break;
        case 12: s += "twelve"; break;
        case 13: s += "thirteen"; break;
        case 14: s += "fourteen"; break;
        case 15: s += "fifteen"; break;
        case 16: s += "sixteen"; break;
        case 17: s += "seventeen"; break;
        case 18: s += "eighteen"; break;
        case 19: s += "nineteen";
    }
}

void a100(string& s, int num) {
    if (num < 20) return a20(s, num);
    if (!s.empty()) s += ' ';
    switch (num / 10) {
        case 2: s += "twenty"; break;
        case 3: s += "thirty"; break;
        case 4: s += "forty"; break;
        case 5: s += "fifty"; break;
        case 6: s += "sixty"; break;
        case 7: s += "seventy"; break;
        case 8: s += "eighty"; break;
        case 9: s += "ninety"; break;
    }
    num %= 10;
    if (num) a20(s, num);
}

void a1000(string& s, int num) {
    if (num < 100) return a100(s, num);
    a20(s, num / 100);
    s += " hundred";
    num %= 100;
    if (num) {
        s += " and";
        a100(s, num);
    }
}

string numbertowords(int num) {
    int k = num / 1000 % 1000;
    int n = num % 1000;
    string ans;
    if (k) {
        a1000(ans, k);
        ans += " thousand";
    }
    if (n || ans.empty()) a1000(ans, n);
    return move(ans);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%s\n", numbertowords(n).c_str());
    }
    return 0;
}
