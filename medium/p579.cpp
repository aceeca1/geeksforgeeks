// The same as p412
#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11000];
        scanf("%s", s);
        int p = 0, q = 0;
        bool a[256]{};
        while (s[q]) {
            if (a[s[q]]) break;
            a[s[q++]] = true;
        }
        int ans = 0;
        for (;;) {
            int t = q - p;
            if (t > ans) ans = t;
            if (!s[q]) break;
            while (s[p] != s[q]) a[s[p++]] = false;
            ++p, ++q;
            while (s[q] && !a[s[q]]) a[s[q++]] = true;
        }
        printf("%d\n", ans);
    }
    return 0;
}
