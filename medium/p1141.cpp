#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

const int M = 1000003;

int64_t powM(int64_t a, int64_t b) {
    int64_t ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % M;
        a = a * a % M;
    }
    return ans;
}

vector<int> fac(M);

void calcFac(int64_t n, int& a, int& am) {
    a = 1;
    am = 0;
    while (n) {
        a = int64_t(a) * fac[n % M] % M;
        n /= M;
        if (n & 1) a = M - a;
        am += n;
    }
}

int main() {
    fac[0] = 1;
    for (int i = 1; i < M; ++i) fac[i] = int64_t(fac[i - 1]) * i % M;
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n, r;
        scanf("%" SCNd64 "%" SCNd64 "%*d", &n, &r);
        int a, am, b, bm;
        calcFac(r, a, am);
        calcFac(n - r, b, bm);
        b = int64_t(b) * a % M;
        bm += am;
        calcFac(n, a, am);
        printf("%d\n", am > bm ? 0 : int(a * powM(b, M - 2) % M));
    }
    return 0;
}
