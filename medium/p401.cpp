#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, c, ans = 1;
        scanf("%d%d%d", &a, &b, &c);
        for (; b; b >>= 1) {
            if (b & 1) ans = int64_t(ans) * a % c;
            a = int64_t(a) * a % c;
        }
        printf("%d\n", ans);
    }
    return 0;
}
