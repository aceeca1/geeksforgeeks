#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110], a = 0;
        scanf("%s", s);
        for (int i = 0; s[i]; ++i)
            a = (a + a + (s[i] - '0')) % 3;
        printf("%d\n", !a);
    }
    return 0;
}
