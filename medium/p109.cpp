// A bit harder than p922
#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        int ans = -1, a = 0, i_a = 0, i_ans, j_ans, sum = 0;
        for (int i = 0; i < s.size(); ++i) {
            sum += s[i] == '1';
            int b = s[i] == '1' ? -1 : 1;
            if (a >= 0) b += a;
            else i_a = i;
            if (b > ans) { ans = b; i_ans = i_a; j_ans = i; }
            a = b;
        }
        if (ans < 0) printf("-1\n");
        else printf("%d %d\n", i_ans + 1, j_ans + 1);
    }
    return 0;
}
