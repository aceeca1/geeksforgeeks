#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

vector<int> a;

int mK() {
    for (int i = (a.size() - 1) >> 1; i; --i)
        for (int j = 0; j < a.size() - i - i; ++j)
            if (a[j + i] - a[j] == a[j + i + i] - a[j + i]) return i;
    return 0;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        a.resize(strlen(s) + 1);
        for (int i = 1; i < a.size(); ++i)
            a[i] = a[i - 1] + (s[i - 1] - '0');
        printf("%d\n", mK() << 1);
    }
    return 0;
}
