#include <cstdio>
#include <vector>
#include <utility>
using namespace std;

vector<vector<int>> out;

pair<int, int> put(int no, int p) {
    int even = 0, odd = 0;
    for (int i: out[no]) if (i != p) {
        auto c = put(i, no);
        even += c.second;
        odd += c.first;
    }
    return make_pair(even + 1, odd);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d",&n);
        out.resize(n);
        for (int i = 0; i < n; ++i) out[i].clear();
        while (--n) {
            int s, t;
            scanf("%d%d", &s, &t);
            --s, --t;
            out[s].emplace_back(t);
            out[t].emplace_back(s);
        }
        auto c = put(0, -1);
        int c1 = c.first * (c.first - 1) >> 1;
        int c2 = c.second * (c.second - 1) >> 1;
        printf("%d\n", c1 + c2);
    }
    return 0;
}
