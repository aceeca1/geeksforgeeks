#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

struct Node {
    Node *ch[26]{};
    bool is = false;
};

struct Trie {
    Node *root = new Node;

    static void dispose(Node* p) {
        if (!p) return;
        for (auto i: p->ch) dispose(i);
        delete p;
    }

    ~Trie() { dispose(root); }

    void insert(const char* s) {
        auto p = root;
        for (int i = 0; s[i]; ++i) {
            auto &c = p->ch[s[i] - 'a'];
            if (!c) c = new Node;
            p = c;
        }
        p->is = true;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        Trie tr;
        while (n--) {
            char s[22];
            scanf("%s", s);
            tr.insert(s);
        }
        char s[1100];
        scanf("%s", s);
        int z = strlen(s);
        vector<int> a(z + 1);
        a[z] = 1;
        for (int i = z - 1; i >= 0; --i) {
            Node *p = tr.root;
            int ans = 0;
            for (int j = i; j < z; ++j) {
                p = p->ch[s[j] - 'a'];
                if (!p) break;
                if (p->is) ans += a[j + 1];
            }
            a[i] = ans;
        }
        printf("%d\n", bool(a[0]));
    }
    return 0;
}
