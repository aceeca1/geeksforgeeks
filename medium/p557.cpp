// Almost the same as p773
#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[44];
        scanf("%s", s);
        int z = strlen(s);
        vector<int> a0(z + 1), a1(z + 1);
        for (int i = z - 1; i >= 0; --i) {
            for (int j = 2; j <= z - i; ++j)
                if (s[i] == s[i + j - 1]) a0[j] = a1[j - 2];
                else a0[j] = min(a0[j - 1], a1[j - 1]) + 1;
            swap(a0, a1);
        }
        printf("%d\n", a1[z]);
    }
    return 0;
}
