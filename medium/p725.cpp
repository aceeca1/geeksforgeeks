#include <cstdio>
#include <cstdint>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 1;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        for (int i = 0; i < n; ++i) {
            if (a[i] < i) ans = 0;
            ans = int64_t(ans) * (a[i] - i) % 1000000007;
        }
        printf("%d\n", ans);
    }
    return 0;
}
