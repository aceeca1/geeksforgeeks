#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <string>
using namespace std;

struct BigInt {
    static constexpr auto M = 100000000;
    vector<uint32_t> v;

    BigInt(uint32_t n = 0): v({n}) {}

    operator string() {
        string ans = to_string(v.back());
        char s[11];
        for (int i = v.size() - 2; i >= 0; --i) {
            sprintf(s, "%08" PRIu32, v[i]);
            ans += s;
        }
        return ans;
    }

    BigInt& operator*=(uint32_t that) {
        int64_t carry = 0;
        for (int i = 0; i < v.size(); ++i) {
            carry += int64_t(v[i]) * that;
            v[i] = carry % M;
            carry /= M;
        }
        if (carry) v.emplace_back(carry);
        return *this;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        BigInt ans(1);
        for (int i = 2; i <= n; ++i) ans *= i;
        printf("%s\n", string(ans).c_str());
    }
    return 0;
}
