#include <cstdio>
#include <vector>
using namespace std;

vector<vector<int>> out;
vector<int> v;
int odd;

void go(int no) {
    v[no] = true;
    if (out[no].size() & 1) ++odd;
    for (int i: out[no]) if (!v[i]) go(i);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        out.resize(n + 1);
        for (int i = 0; i <= n; ++i) out[i].clear();
        while (m--) {
            int s, t;
            scanf("%d%d", &s, &t);
            out[s].emplace_back(t);
            out[t].emplace_back(s);
        }
        v.resize(n + 1);
        for (int i = 0; i <= n; ++i) v[i] = false;
        int ans = 0, t = 0;
        for (int i = 1; i <= n; ++i) {
            if (v[i]) continue;
            odd = 0;
            go(i);
            ans += odd ? odd >> 1 : 1;
            t += odd + 1;
        }
        printf("%d\n", t > 1 ? ans : 0);
    }
    return 0;
}
