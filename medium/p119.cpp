#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0x80000000, p = -1, m = 0, i_ans, len_ans;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i <= n; ++i) {
            if (i < n) scanf("%d", &a[i]);
            if (i < n && a[i] >= 0) {
                if (p == -1) { p = i; m = a[i]; }
                else m += a[i];
            } else if (p != -1) {
                int len = i - p;
                if (m > ans || (m == ans && len > len_ans)) {
                    ans = m;
                    i_ans = p;
                    len_ans = len;
                }
                p = -1;
            }
        }
        bool head = true;
        for (int i = 0; i < len_ans; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i_ans + i]);
        }
        printf("\n");
    }
    return 0;
}
