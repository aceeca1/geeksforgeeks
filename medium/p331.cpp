#include <cstdio>
#include <cctype>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0), s1, s2;
        scanf("%s", &s[0]);
        s.pop_back();
        for (int i = 0; i < n; ++i)
            if (islower(s[i])) s1 += s[i];
            else s2 += s[i];
        sort(s1.begin(), s1.end());
        sort(s2.begin(), s2.end());
        for (int i = n - 1; i >= 0; --i)
            if (islower(s[i])) { s[i] = s1.back(); s1.pop_back(); }
            else { s[i] = s2.back(); s2.pop_back(); }
        printf("%s\n", s.c_str());
    }
    return 0;
}
