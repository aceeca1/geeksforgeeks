#include <cstdio>
#include <vector>
#include <map>
using namespace std;

vector<int> c;
int n;
bool empty;

typedef map<int, int>::iterator MIIIt;

void put(const MIIIt& s, const MIIIt& t, int e) {
    if (s == t) {
        if (!e) {
            empty = false;
            putchar('(');
            bool head = true;
            for (int i = 0; i < c.size(); ++i) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", c[i]);
            }
            putchar(')');
        }
        return;
    }
    for (int i = 0; i < s->second; ++i) {
        c.emplace_back(s->first);
        e -= s->first;
    }
    for (int i = s->second; i >= 0; --i) {
        if (e >= 0) {
            MIIIt s1 = s;
            ++s1;
            put(s1, t, e);
        }
        if (i) {
            c.pop_back();
            e += s->first;
        }
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int b;
        scanf("%d", &n);
        map<int, int> a;
        while (n--) {
            int c;
            scanf("%d", &c);
            ++a[c];
        }
        scanf("%d", &b);
        empty = true;
        put(a.begin(), a.end(), b);
        if (empty) printf("Empty\n");
        else printf("\n");
    }
    return 0;
}
