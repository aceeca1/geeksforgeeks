#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        int a0, a1 = 1, a2 = 0;
        for (int i = 0; i < n; ++i) {
            a0 = s[i] == '0' ? 0 : a1;
            if (i && (s[i - 1] == '1' ||
                (s[i - 1] == '2' && s[i] <= '6'))) a0 += a2;
            a2 = a1;
            a1 = a0;
        }
        printf("%d\n", a1);
    }
    return 0;
}
