#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

string s;
int n, k;

bool can() {
    vector<int> a0(n + 1), a1(n + 1);
    for (int i = n - 1; i >= 0; --i) {
        for (int j = 2; j <= n - i; ++j)
            if (s[i] == s[i + j - 1]) a0[j] = a1[j - 2];
            else a0[j] = min(a0[j - 1], a1[j - 1]) + 1;
        if (a0[n - i] > k) return false;
        swap(a0, a1);
    }
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d", &n, &k);
        s.resize(n + 1);
        scanf("%s", &s[0]);
        s.pop_back();
        printf("%s\n", can() ? "YES" : "NO");
    }
    return 0;
}
