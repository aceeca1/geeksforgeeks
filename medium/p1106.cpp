#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n == 1) printf("1\n");
        else if (n == 2) printf("2\n");
        else if (n & 1)
            printf("%" PRId64 "\n", int64_t(n) * (n - 1) * (n - 2));
        else if (!(n % 6))
            printf("%" PRId64 "\n", int64_t(n - 1) * (n - 2) * (n - 3));
        else
            printf("%" PRId64 "\n", int64_t(n) * (n - 1) * (n - 3));
    }
    return 0;
}
