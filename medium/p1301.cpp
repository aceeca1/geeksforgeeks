#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        scanf("%d", &k);
        nth_element(a.begin(), a.begin() + k - 1, a.end());
        printf("%d\n", a[k - 1]);
    }
    return 0;
}
