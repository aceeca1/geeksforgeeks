#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        int z = n, p = 0;
        for (int i = 1; ; ++i) {
            int b = i % z;
            for (int j = 0; j < b; ++j) {
                p = (p + 1) % n;
                while (a[p]) p = (p + 1) % n;
            }
            a[p] = i;
            --z;
            if (i == n) break;
            p = (p + 1) % n;
            while (a[p]) p = (p + 1) % n;
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
