#include <cstdio>
#include <vector>
using namespace std;

vector<int> a;
int n;

bool can() {
    for (int i = 1; i <= n; ++i)
        if (a[i - 1] == a[n] - a[i]) return true;
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d", &n);
        a.resize(n + 1);
        for (int i = 1; i <= n; ++i) {
            scanf("%d", &a[i]);
            a[i] += a[i - 1];
        }
        printf("%s\n", can() ? "YES" : "NO");
    }
    return 0;
}
