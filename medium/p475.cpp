#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        vector<int> a0(m), a1(m);
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a0[0]);
            if (a0[0] > ans) ans = a0[0];
            for (int j = 1; j < m; ++j) {
                scanf("%d", &a0[j]);
                if (!a0[j]) continue;
                int v = a1[j], v1 = a1[j - 1], v2 = a0[j - 1];
                if (v1 < v) v = v1;
                if (v2 < v) v = v2;
                a0[j] = ++v;
                if (v > ans) ans = v;
            }
            swap(a0, a1);
        }
        printf("%d\n", ans);
    }
    return 0;
}
