#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> bc(m + 1);
    for (int i = 1; i <= m; ++i) bc[i] = bc[i >> 1] + (i & 1);
    for (int i = m; i >= 1; --i) {
        if (i + bc[i] <= m) bc[i + bc[i]] = true;
        bc[i] = false;
    }
    for (int ni: n) printf("%d\n", !bc[ni]);
    return 0;
}
