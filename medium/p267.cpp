// See https://en.wikipedia.org/wiki/Wythoff's_game
#include <cstdio>
#include <cmath>
#include <utility>
using namespace std;

int main() {
    double c = (1 + sqrt(5)) * 0.5;
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        if (a > b) swap(a, b);
        int k = ceil(a / c);
        bool ans = int(k * c) == a && int(k * c * c) == b;
        printf("%s\n", ans ? "Bunty" : "Dolly");
    }
    return 0;
}
