#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    for (int ni: n)
        if (ni == 1 || ni == 2) printf("-1\n");
        else if (~ni & 1) {
            int i = 2;
            while (p[ni - i] < ni - i) i = p[i];
            printf("%d %d\n", i, ni - i);
        } else if (p[ni - 2] > ni - 2) printf("2 %d\n", ni - 2);
        else printf("-1\n");
    return 0;
}
