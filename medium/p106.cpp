#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0x80000000, a0, a1 = 0;
        scanf("%d", &n);
        while (n--) {
            scanf("%d", &a0);
            if (a1 > 0) a0 += a1;
            if (a0 > ans) ans = a0;
            a1 = a0;
        }
        printf("%d\n", ans);
    }
    return 0;
}
