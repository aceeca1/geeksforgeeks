#include <cstdio>
#include <vector>
#include <deque>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        deque<int> d1, d2;
        d1.emplace_back(0);
        d2.emplace_back(0);
        int p = 0, q = 1, ans = 0;
        for (;;) {
            if (a[d1.front()] << 1 > a[d2.front()]) {
                int t = q - p;
                if (t > ans) ans = t;
                if (q >= n) break;
                while (!d1.empty() && a[d1.back()] >= a[q]) d1.pop_back();
                d1.emplace_back(q);
                while (!d2.empty() && a[d2.back()] <= a[q]) d2.pop_back();
                d2.emplace_back(q++);
            } else {
                if (d1.front() == p) d1.pop_front();
                if (d2.front() == p++) d2.pop_front();
            }
        }
        printf("%d\n", n - ans);
    }
    return 0;
}
