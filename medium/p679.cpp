#include <cstdio>
#include <vector>
using namespace std;

vector<int> a;
bool head;

void put(int n) {
    if (!n) {
        for (int i = 0; i < a.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        return;
    }
    int ub = n;
    if (!a.empty() && a.back() < ub) ub = a.back();
    for (int i = ub; i; --i) {
        a.emplace_back(i);
        put(n - i);
        a.pop_back();
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        head = true;
        put(n);
        printf("\n");
    }
    return 0;
}
