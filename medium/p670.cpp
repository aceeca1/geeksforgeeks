#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int p = 0, q = n - 1, ans = 0;
        while (p < q) {
            if (a[p] < a[q]) a[p + 1] += a[p], ++p, ++ans;
            else if (a[p] > a[q]) a[q - 1] += a[q], --q, ++ans;
            else ++p, --q;
        }
        printf("%d\n", ans);
    }
    return 0;
}
