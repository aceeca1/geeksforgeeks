#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

constexpr const char* d = "\
abcdefghijklmnopqrstuvwxyz\
ABCDEFGHIJKLMNOPQRSTUVWXYZ\
0123456789";

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s;
        if (!n) s += '0';
        for (int i = n; i; i /= 62) s += d[i % 62];
        reverse(s.begin(), s.end());
        printf("%s\n%d\n", s.c_str(), n);
    }
    return 0;
}
