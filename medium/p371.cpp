#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int n = strlen(s) + 1;
        string a(n, 0);
        for (int i = 1; i <= n; ++i) a[i - 1] = '0' + i;
        int p = -1;
        for (int i = 0; i <= n; ++i) if (s[i] == 'D') {
            if (p == -1) p = i;
        } else if (p != -1) {
            reverse(&a[p], &a[i + 1]);
            p = -1;
        }
        printf("%s\n", a.c_str());
    }
    return 0;
}
