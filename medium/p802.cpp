#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a, b, c, d;
        scanf("%d%d%d%d%d", &n, &a, &b, &c, &d);
        if (a > d) swap(a, d);
        if (b > c) swap(b, c);
        int lb = c + d - n, ub = a + b - 1;
        printf("%d\n", lb <= ub ? ub - lb + 1 : 0);
    }
    return 0;
}
