#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int s = 0, t = 0, ans = 0;
        while (t < n - 1) {
            int t0 = t;
            for (int i = s; i <= t; ++i) {
                int v = i + a[i];
                if (v > t0) t0 = v;
            }
            if (t0 == t) { ans = -1; break; }
            s = t + 1;
            t = t0;
            ++ans;
        }
        printf("%d\n", ans);
    }
    return 0;
}
