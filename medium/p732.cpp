#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> a;
int n, x;

bool can() {
    for (int i = 0; i < n; ++i) {
        int k = n - 1;
        for (int j = i + 1; j < k; ++j) {
            while (j < k && a[i] + a[j] + a[k] > x) --k;
            if (j >= k) break;
            if (a[i] + a[j] + a[k] == x) return true;
        }
    }
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d", &n, &x);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        printf("%d\n", can());
    }
    return 0;
}
