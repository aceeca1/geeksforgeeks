#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<vector<int>> a(t);
    vector<int> u(t);
    for (int ti = 0; ti < t; ++ti) {
        int n;
        scanf("%d", &n);
        a[ti].resize(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[ti][i]);
            if (a[ti][i] > u[ti]) u[ti] = a[ti][i];
        }
        if (u[ti] > m) m = u[ti];
    }
    vector<int> c(m + 1);
    for (int i = 1; i <= m; ++i)
        c[i] = c[i >> 1] + (i & 1);
    for (int ti = 0; ti < t; ++ti) {
        int ans = ilogb(u[ti]);
        for (int i = 0; i < a[ti].size(); ++i)
            ans += c[a[ti][i]];
        printf("%d\n", ans);
    }
    return 0;
}
