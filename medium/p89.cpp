#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        scanf("%d", &k);
        vector<double> b(k + 1);
        b[0] = 1.0;
        for (int i: a)
            for (int j = i; j <= k; ++j)
                b[j] += b[j - i];
        printf("%.0f\n", b[k]);
    }
    return 0;
}
