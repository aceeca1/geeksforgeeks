#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<vector<int>> a(n, vector<int>(n));
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                scanf("%d", &a[i][j]);
        bool head = true;
        for (int i = 0; i < n; ++i)
            for (int j = n - 1; j >= 0; --j) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", a[j][i]);
            }
        printf("\n");
    }
    return 0;
}
