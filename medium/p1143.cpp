#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int m;

int64_t powM(int64_t a, int64_t b) {
    int64_t ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % m;
        a = a * a % m;
    }
    return ans;
}

vector<int> fac;

void calcFac(int64_t n, int& a, int& am) {
    a = 1;
    am = 0;
    while (n) {
        a = int64_t(a) * fac[n % m] % m;
        n /= m;
        if (n & 1) a = m - a;
        am += n;
    }
}

int nCr(int n, int r) {
    fac.resize(m);
    fac[0] = 1;
    for (int i = 1; i < m; ++i) fac[i] = int64_t(fac[i - 1]) * i % m;
    int a, am, b, bm;
    calcFac(r, a, am);
    calcFac(n - r, b, bm);
    b = int64_t(b) * a % m;
    bm += am;
    calcFac(n, a, am);
    return am > bm ? 0 : int(a * powM(b, m - 2) % m);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n, r, u;
        scanf("%" SCNd64 "%" SCNd64 "%" SCNd64, &n, &r, &u);
        vector<int> p;
        while (u > 1) {
            for (int i = 2; i <= u; ++i) if (!(u % i)) {
                u /= i;
                p.emplace_back(i);
                break;
            }
        }
        int m0 = 1, k0 = 0;
        for (int i: p) {
            m = i;
            int k = nCr(n, r);
            if (m0 == 1) { m0 = m; k0 = k; continue; }
            m *= m0;
            int a = powM(m0, i - 1);
            k0 = (k0 * int64_t(m + 1 - a) + k * int64_t(a)) % m;
            m0 = m;
        }
        printf("%d\n", k0);
    }
    return 0;
}
