#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, q;
        scanf("%d", &n);
        vector<int64_t> a(n + 1), b(n + 1), c(n + 1);
        for (int i = 1; i <= n; ++i) {
            scanf("%" SCNd64, &a[i]);
            b[i] = b[i - 1] + a[i] * (i + 1);
            c[i] = c[i - 1] + a[i] * (i + 1) * (i + 1);
            a[i] += a[i - 1];
        }
        scanf("%d", &q);
        bool head = true;
        while (q--) {
            int l, r;
            scanf("%d%d", &l, &r);
            int64_t s1 = l * l * (a[r] - a[l - 1]);
            int64_t s2 = l * (b[r] - b[l - 1]) << 1;
            int64_t s3 = c[r] - c[l - 1];
            if (!head) putchar(' ');
            head = false;
            printf("%" PRId64, s1 - s2 + s3);
        }
        printf("\n");
    }
    return 0;
}
