#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        if (n == 1) { printf("1\n"); continue; }
        vector<vector<int>> b(n, vector<int>(n));
        int ans = 0;
        for (int i = n - 1; i; --i) {
            int k = i + 1;
            for (int j = i - 1; j >= 0; --j) {
                int t = a[i] + a[i] - a[j];
                while (k < n && a[k] < t) ++k;
                t = k >= n || a[k] > t ? 2 : b[i][k] + 1;
                b[j][i] = t;
                if (t > ans) ans = t;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
