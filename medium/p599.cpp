#include <cstdio>
#include <vector>
using namespace std;

void read(int n, vector<int>& a, vector<int>& c) {
    for (int i = 0; i < n; ++i) {
        int b;
        scanf("%d", &b);
        if (!a.empty() && a.back() == b) ++c.back();
        else {
            a.emplace_back(b);
            c.emplace_back(1);
        }
    }
}

void calc1(vector<int>& a, vector<int>& b, vector<int>& c, int& p) {
    b[p] = a[p] * c[p];
    if (p) b[p] += b[p - 1];
    ++p;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        vector<int> a1, a2, c1, c2;
        read(n1, a1, c1);
        read(n2, a2, c2);
        vector<int> b1(a1.size()), b2(a2.size());
        int p1 = 0, p2 = 0;
        for (;;)
            if (p1 == a1.size())
                if (p2 == a2.size()) break;
                else calc1(a2, b2, c2, p2);
            else if (p2 == a2.size()) calc1(a1, b1, c1, p1);
            else if (a1[p1] < a2[p2]) calc1(a1, b1, c1, p1);
            else if (a1[p1] > a2[p2]) calc1(a2, b2, c2, p2);
            else {
                int t1 = a1[p1] * c1[p1];
                int t2 = a2[p2] * c2[p2];
                int e1 = t1 + t2 - a1[p1], e2 = e1;
                int k = e1 - a1[p1];
                if (c1[p1] > 1 && k > t1) t1 = k;
                if (c2[p2] > 1 && k > t2) t2 = k;
                if (p1) { t1 += b1[p1 - 1]; e2 += b1[p1 - 1]; }
                if (p2) { t2 += b2[p2 - 1]; e1 += b2[p2 - 1]; }
                b1[p1++] = max(t1, e1);
                b2[p2++] = max(t2, e2);
            }
        printf("%d\n", max(b1.back(), b2.back()));
    }
    return 0;
}
