#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cstring>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[220], s2[220]; // workaround
        scanf("%s%s", s1, s2);
        int z1 = strlen(s1), z2 = strlen(s2);
        vector<int64_t> a(z2 + 1);
        a[0] = 1;
        for (int i = 0; s1[i]; ++i)
            for (int j = z2; j; --j)
                if (s1[i] == s2[j - 1]) a[j] += a[j - 1];
        printf("%" PRId64 "\n", a[z2]);
    }
    return 0;
}
