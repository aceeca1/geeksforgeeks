#include <cstdio>
#include <vector>
using namespace std;

vector<int> a, b;
int n;

void calc() {
    b = a;
    for (int i = (n + 1) >> 1; i < n; ++i) b[i] = b[n - 1 - i];
    if (b > a) return;
    int k = (n - 1) >> 1;
    while (k >= 0 && b[k] == 9) b[k--] = 0;
    if (k < 0) {
        b[0] = 1;
        for (int i = 1; i < b.size(); ++i) b[i] = 0;
        b.emplace_back(1);
        return;
    }
    ++b[k];
    for (; k <= (n - 2) >> 1; ++k) b[n - 1 - k] = b[k];
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        calc();
        bool head = true;
        for (int i = 0; i < b.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", b[i]);
        }
        printf("\n");
    }
    return 0;
}
