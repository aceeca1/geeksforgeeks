#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int64_t ans = 1;
        for (int i = 1; i <= n; ++i)
            ans = ans * (n + n + 1 - i) / i;
        printf("%" PRId64 "\n", ans / (n + 1));
    }
    return 0;
}
