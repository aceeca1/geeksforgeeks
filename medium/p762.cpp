// See https://oeis.org/A013928
#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    m = sqrt(m);
    vector<int> p(m + 1), mu(m + 1);
    int u = 0;
    mu[1] = 1;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
        int c = i / v;
        int pc = p[c] > c ? c : p[c];
        mu[i] = pc == v ? 0 : -mu[c];
    }
    for (int ni: n) {
        int ans = ni;
        for (int j = 1; j * j <= ni; ++j)
            ans -= mu[j] * ni / (j * j);
        printf("%d\n", ans);
    }
    return 0;
}
