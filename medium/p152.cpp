#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        string s1(n1 + 1, 0), s2(n2 + 1, 0);
        scanf("%s%s", &s1[0], &s2[0]);
        s1.pop_back();
        s2.pop_back();
        vector<int> a0(n2), a1(n2);
        for (int i = 0; i < n1; ++i) {
            for (int j = 0; j < n2; ++j) {
                if (s1[i] == s2[j]) a0[j] = (j ? a1[j - 1] : 0) + 1;
                else a0[j] = max(j ? a0[j - 1] : 0, a1[j]);
            }
            swap(a0, a1);
        }
        printf("%d\n", a1[n2 - 1]);
    }
    return 0;
}
