#include <cstdio>
#include <vector>
#include <algorithm>
#include <map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n), b(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        for (int i = 1; i <= k; ++i) {
            map<int, int> m;
            m.emplace(0x80000000, i == 1 ? 0 : 0x80000000);
            for (int j = 0; j < n; ++j) {
                auto ma = --m.lower_bound(a[j]);
                auto av = b[j];
                b[j] = ma->second + a[j];
                auto p = m.emplace(a[j], av);
                auto pf = p.first;
                if (!p.second && av > pf->second) pf->second = av;
                auto q = pf; --q;
                if (q->second >= pf->second) { m.erase(pf); continue; }
                for (;;) {
                    auto q = pf; ++q;
                    if (q == m.end() || q->second > pf->second) break;
                    m.erase(q);
                }
            }
        }
        int ans = *max_element(b.begin(), b.end());
        printf("%d\n", ans > 0 ? ans : -1);
    }
    return 0;
}
