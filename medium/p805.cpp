#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> a(m + 1);
    for (int i = 1; i <= m; ++i)
        if ((i >= 2 && !a[i - 2]) || (i >= 3 && !a[i - 3]) ||
            (i >= 4 && !a[i - 4]) || (i >= 5 && !a[i - 5]) ||
            !a[i >> 1] || !a[i / 3] || !a[i / 4] || !a[i / 5]) a[i] = true;
    for (int ni: n) printf("%s\n", a[ni] ? "Jon" : "Arya");
    return 0;
}
