#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110000];
        scanf("%s", s);
        int z = strlen(s);
        vector<int> a(z + 1);
        int k = a[0] = -1;
        for (int i = 1; i <= z; ++i) {
            while (k >= 0 && s[k] != s[i - 1]) k = a[k];
            a[i] = ++k;
        }
        printf("%d\n", k);
    }
    return 0;
}
