#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a = 0;
        scanf("%d ", &n);
        for (int i = 0; i < n; ++i) {
            char c;
            scanf("%c", &c);
            a += c == '1';
        }
        printf("%d\n", a * (a - 1) >> 1);
    }
    return 0;
}
