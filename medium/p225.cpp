#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a;
        scanf("%d%d", &n, &a);
        vector<int> b(a + 1, 0x7ffffffe);
        b[0] = 0;
        while (n--) {
            int c;
            scanf("%d", &c);
            for (int i = c; i <= a; ++i) {
                int t = b[i - c] + 1;
                if (t < b[i]) b[i] = t;
            }
        }
        printf("%d\n", b[a] == 0x7ffffffe ? -1 : b[a]);
    }
    return 0;
}
