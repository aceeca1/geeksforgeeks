#include <cstdio>
#include <string>
using namespace std;

constexpr int M = 1000000007;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf(("%" + to_string(n) + "s").c_str(), &s[0]);
        s.pop_back();
        int a[6]{1};
        for (char i: s) switch (i) {
            case 'G': a[1] = (a[1] + a[0]) % M; break;
            case 'E':
                a[3] = (a[3] + a[2]) % M;
                a[2] = (a[2] + a[1]) % M; break;
            case 'K':
                a[4] = (a[4] + a[3]) % M; break;
            case 'S':
                a[5] = (a[5] + a[4]) % M;
        }
        printf("%d\n", a[5]);
        scanf("%*[^\n]");
    }
    return 0;
}
