#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> l(t), r(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d%d", &l[ti], &r[ti]);
        if (r[ti] > m) m = r[ti];
    }
    vector<int> p(m + 1);
    vector<int64_t> q(m + 1), a(m + 1);
    int u = 0;
    a[1] = 1;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
        int c = i / v;
        int pc = p[c] > c ? c : p[c];
        q[i] = pc == v ? q[c] * v : v;
        int64_t t = (1 - q[i] * v) / (1 - v);
        a[i] = a[i / q[i]] * t;
    }
    a[0] = 0;
    for (int i = 1; i <= m; ++i) a[i] += a[i - 1];
    for (int ti = 0; ti < t; ++ti)
        printf("%" PRId64 "\n", a[r[ti]] - a[l[ti] - 1]);
    return 0;
}
