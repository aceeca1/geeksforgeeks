#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        unordered_set<int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            a.emplace(b);
        }
        int ans = 0;
        while (!a.empty()) {
            int lb = *a.begin(), ub = lb + 1;
            while (a.erase(lb)) --lb;
            while (a.erase(ub)) ++ub;
            ub -= lb + 1;
            if (ub > ans) ans = ub;
        }
        printf("%d\n", ans);
    }
    return 0;
}
