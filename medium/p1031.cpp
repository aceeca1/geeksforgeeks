#include <cstdio>
#include <cmath>
#include <cstring>
#include <vector>
using namespace std;

int bitCount(int n) {
    n = ((n >> 1) & 0x55555555) + (n & 0x55555555);
    n = ((n >> 2) & 0x33333333) + (n & 0x33333333);
    n = ((n >> 4) & 0x0f0f0f0f) + (n & 0x0f0f0f0f);
    n = ((n >> 8) & 0x00ff00ff) + (n & 0x00ff00ff);
    return (n >> 16) + (n & 0xffff);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11000];
        scanf("%s", s);
        int n = strlen(s), q;
        int m = 1 << (ilogb(n + 2) + 1);
        vector<int> a(m + m);
        for (int i = 0; i < n; ++i) a[m + i + 1] = 1 << (s[i] - 'a');
        for (int i = m - 1; i >= 1; --i) a[i] = a[i + i] | a[i + i + 1];
        scanf("%d", &q);
        bool head = true;
        while (q--) {
            int l, r;
            scanf("%d%d", &l, &r);
            l += m - 1;
            r += m + 1;
            int ans = 0;
            while ((l ^ r) != 1) {
                if (~l & 1) ans |= a[l + 1];
                if ( r & 1) ans |= a[r - 1];
                l >>= 1;
                r >>= 1;
            }
            if (!head) putchar(' ');
            head = false;
            printf("%d", bitCount(ans));
        }
        printf("\n");
    }
    return 0;
}
