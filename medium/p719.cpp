#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

bool is(const int* s, const int* t, int lb, int ub) {
    if (s == t) return true;
    if (s[0] < lb || ub < s[0]) return false;
    bool b = is(s + 1, lower_bound(s + 1, t, s[0]), lb, s[0]);
    return b && is(upper_bound(s + 1, t, s[0]), t, s[0], ub);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        printf("%d\n", is(&a[0], &a[n], 0x80000000, 0x7fffffff));
    }
    return 0;
}
