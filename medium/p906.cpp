#include <cstdio>
#include <vector>
#include <utility>
#include <algorithm>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        unordered_map<int, vector<int>> a;
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            a[b].emplace_back(i);
        }
        unordered_map<int, vector<pair<int, int>>> u;
        for (auto i = a.begin(); i != a.end(); ++i) {
            auto &ai1 = i->first;
            auto &ai2 = i->second;
            if (ai2.size() >= 4) {
                u[ai1 + ai1].emplace_back(ai2[0], ai2[1]);
                u[ai1 + ai1].emplace_back(ai2[2], ai2[3]);
            } else if (ai2.size() >= 2)
                u[ai1 + ai1].emplace_back(ai2[0], ai2[1]);
            auto j = i;
            ++j;
            for (; j != a.end(); ++j) {
                auto &aj1 = j->first;
                auto &aj2 = j->second;
                if (ai2[0] < aj2[0])
                    u[ai1 + aj1].emplace_back(ai2[0], aj2[0]);
                else
                    u[ai1 + aj1].emplace_back(aj2[0], ai2[0]);
            }
        }
        pair<int, int> ans0(n, n), ans1(n, n);
        for (auto &ui: u) {
            auto &uis = ui.second;
            if (uis.size() <= 1) continue;
            sort(uis.begin(), uis.end());
            if (uis[0] < ans0) {
                ans0 = uis[0];
                ans1 = uis[1];
            }
        }
        if (ans0.first < n) printf("%d %d %d %d\n",
            ans0.first, ans0.second, ans1.first, ans1.second);
        else printf("no pairs\n");
    }
    return 0;
}
