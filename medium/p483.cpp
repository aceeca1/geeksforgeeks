#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x1, y1, x2, y2;
        scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
        x2 -= x1;
        y2 -= y1;
        while (y2) swap(x2 %= y2, y2);
        if (x2 < 0) x2 = -x2;
        printf("%d\n", x2 - 1);
    }
    return 0;
}
