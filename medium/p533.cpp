#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cmath>
#include <vector>
#include <random>
using namespace std;

bool isWitness(int64_t n, int64_t d, int r, int64_t a) {
    int64_t x = 1;
    while (d) {
        if (d & 1) x = x * a % n;
        a = a * a % n;
        d >>= 1;
    }
    if (x == 1 || x == n - 1) return false;
    for (int i = 1; i < r; ++i) {
        x = x * x % n;
        if (x == 1) return true;
        if (x == n - 1) return false;
    }
    return true;
}

bool isPrime(int64_t n) {
    static default_random_engine rand;
    if (n <= 1) return false;
    if (n == 2) return true;
    int64_t d = n - 1;
    int r = 0;
    while (!(d & 1)) { d >>= 1; ++r; }
    uniform_int_distribution<int64_t> u(2, n - 2);
    for (int i = 0; i < 10; ++i)
        if (isWitness(n, d, r, u(rand))) return false;
    return true;
}

vector<int> p;

bool is(int64_t n) {
    if (n == 1) return false;
    int64_t s = sqrt((long double)n);
    if (s * s == n) return true;
    if (isPrime(n)) return false;
    for (int i = 0; i < p.size() && p[i] * p[i] * p[i] <= n; ++i)
        if (!(n % p[i])) return is(n / p[i]);
    return false;
}

int main() {
    int t;
    int64_t mu = 0;
    scanf("%d", &t);
    vector<int64_t> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%" SCNd64, &n[ti]);
        if (n[ti] > mu) mu = n[ti];
    }
    int m = ceil(cbrt((long double)mu));
    p.resize(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    u = 0;
    for (int i = 2; i <= m; i = p[i]) p[u++] = i;
    p.resize(u);
    for (int ti = 0; ti < t; ++ti)
        printf("%s\n", is(n[ti]) ? "YES" : "NO");
    return 0;
}
