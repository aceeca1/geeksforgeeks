#include <cstdio>
#include <cstdint>
using namespace std;

constexpr int M = 1000000007;

int64_t powM(int64_t a, int64_t b) {
    int64_t ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % M;
        a = a * a % M;
    }
    return ans;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, r;
        int64_t ans = 1, d = 1;
        scanf("%d%d", &n, &r);
        for (int i = 1; i <= r; ++i) {
            ans = ans * (n + 1 - i) % M;
            d = d * i % M;
        }
        ans = ans * powM(d, M - 2) % M;
        printf("%d\n", int(ans));
    }
    return 0;
}
