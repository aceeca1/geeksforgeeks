#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m = 0;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            if (a[i] > m) m = a[i];
        }
        int ans = 0;
        for (; m; m >>= 1) {
            int b[2]{};
            for (int i = 0; i < n; ++i) {
                ++b[a[i] & 1];
                a[i] >>= 1;
            }
            ans += b[0] * b[1] << 1;
        }
        printf("%d\n", ans);
    }
    return 0;
}
