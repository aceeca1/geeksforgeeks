#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

constexpr int m = 1100;
vector<int> p(m + 1);
char s[1100];
int z;

bool can(int no) {
    for (int i = no; i < z; i += no)
        if (strncmp(s, s + i, no)) return false;
    return true;
}

bool can() {
    for (int i = 2; i <= z; i = p[i])
        if (!(z % i) && can(z / i)) return true;
    return false;
}

int main() {
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        z = strlen(s);
        printf("%s\n", can() ? "True" : "False");
    }
    return 0;
}
