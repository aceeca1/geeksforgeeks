#include <cstdio>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        vector<int> a1(n1);
        unordered_map<int, int> a2;
        for (int i = 0; i < n1; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n2; ++i) {
            int b;
            scanf("%d", &b);
            a2.emplace(b, i);
        }
        sort(a1.begin(), a1.end(), [&](int c1, int c2) {
            auto p1 = a2.find(c1), p2 = a2.find(c2);
            c1 = p1 == a2.end() ? n2 + c1 : p1->second;
            c2 = p2 == a2.end() ? n2 + c2 : p2->second;
            return c1 < c2;
        });
        bool head = true;
        for (int i = 0; i < n1; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a1[i]);
        }
        printf("\n");
    }
    return 0;
}
