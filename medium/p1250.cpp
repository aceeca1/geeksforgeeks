#include <cstdio>
#include <vector>
#include <map>
using namespace std;

typedef map<int, int>::iterator mIIIt;

vector<int> a;

void put(const mIIIt& s, const mIIIt& t) {
    if (s == t) return;
    for (int i = 1; i <= s->second; ++i) {
        a.emplace_back(s->first);
        putchar('(');
        bool head = true;
        for (int j = 0; j < a.size(); ++j) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[j]);
        }
        putchar(')');
    }
    mIIIt s1 = s;
    ++s1;
    for (int i = s->second; i >= 0; --i) {
        put(s1, t);
        if (i) a.pop_back();
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        map<int, int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            ++a[b];
        }
        printf("()");
        put(a.begin(), a.end());
        printf("\n");
    }
    return 0;
}
