#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[11000], s2[11000];
        scanf("%s%s", s1, s2);
        int z = strlen(s2);
        vector<int> a(z + 1);
        int k = a[0] = -1;
        for (int i = 1; i <= z; ++i) {
            while (k >= 0 && s2[k] != s2[i - 1]) k = a[k];
            a[i] = ++k;
        }
        k = 0;
        bool head = true;
        for (int i = 0; s1[i]; ++i) {
            while (k >= 0 && s2[k] != s1[i]) k = a[k];
            if (++k == z) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", i - z + 2);
            }
        }
        if (head) printf("-1\n");
        else printf("\n");
    }
    return 0;
}
