#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> a, c;
int n;
bool empty;

void put(int k, int s) {
    if (k == a.size()) {
        if (!s) {
            empty = false;
            putchar('(');
            bool head = true;
            for (int i = 0; i < c.size(); ++i) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", c[i]);
            }
            putchar(')');
        }
        return;
    }
    while (s >= a[k]) {
        s -= a[k];
        c.emplace_back(a[k]);
    }
    for (;;) {
        put(k + 1, s);
        if (c.empty() || c.back() != a[k]) break;
        c.pop_back();
        s += a[k];
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int b;
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        a.resize(unique(a.begin(), a.end()) - a.begin());
        scanf("%d", &b);
        empty = true;
        put(0, b);
        if (empty) printf("Empty\n");
        else printf("\n");
    }
    return 0;
}
