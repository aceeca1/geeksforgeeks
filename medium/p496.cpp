#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n, 0);
        for (int i = 0; i < n; ++i) scanf(" %c", &s[i]);
        sort(s.begin(), s.end());
        for (int i = 0; i < 2; ++i) {
            bool head = true;
            for (int i = 0; i < n; ++i) {
                if (!head) putchar(' ');
                head = false;
                putchar(s[i]);
            }
            printf("\n");
        }
        while (n--) scanf(" %*c");
    }
    return 0;
}
