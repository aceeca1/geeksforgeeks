#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, q;
        scanf("%d", &n);
        vector<int> a(n + 1);
        for (int i = 1; i <= n; ++i) {
            scanf("%d", &a[i]);
            a[i] += a[i - 1];
        }
        scanf("%d", &q);
        bool head = true;
        while (q--) {
            int l, r;
            scanf("%d%d", &l, &r);
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[r] - a[l - 1]);
        }
        printf("\n");
    }
    return 0;
}
