#include <cstdio>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a0, a1 = 0, a2 = 0, a3 = 0;
        for (int i = 0; i <= n; ++i) {
            int b = 0;
            if (i < n) scanf("%d", &b);
            a0 = b + min(min(a1, a2), a3);
            a3 = a2;
            a2 = a1;
            a1 = a0;
        }
        if (n == 1) printf("%d\n", a2);
        else if (n == 2) printf("%d\n", min(a2, a3));
        else printf("%d\n", a1);
    }
    return 0;
}
