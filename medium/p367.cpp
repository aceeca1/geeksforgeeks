#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<pair<int, int>> a(n + n);
        for (int i = 0; i < n + n; ++i) {
            scanf("%d", &a[i].first);
            if (i < n && a[i].first == 2400) a[i].first = 0;
            if (i >= n && !a[i].first) a[i].first = 2400;
            a[i].first = a[i].first / 100 * 60 + a[i].first % 100;
            a[i].second = i < n ? 1 : -1;
        }
        sort(a.begin(), a.end());
        int b = 0, ans = 1;
        for (int i = 0; i < n + n; ++i) {
            b += a[i].second;
            if (b > ans) ans = b;
        }
        printf("%d\n", ans);
    }
    return 0;
}
