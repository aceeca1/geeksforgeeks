#include <cstdio>
using namespace std;

char s[55];

int calc() {
    int n = 4, a = 1;
    for (int i = 0; s[i]; ++i) {
        if (a > n) return -1;
        switch (s[i]) {
            case 'W': n += a; a = 1; break;
            case 'L': n -= a; a += a;
        }
    }
    return n;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        printf("%d\n", calc());
    }
    return 0;
}
