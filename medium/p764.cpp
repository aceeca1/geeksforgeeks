#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

vector<int> a;
int n, k;

int solve() {
    int free = n;
    for (;;) {
        int max = 0;
        for (int i = 0; i < n; ++i)
            if (a[i] > max) max = a[i];
        if (!max) break;
        for (int i = 0; i < n; ++i) {
            int t = a[i] ^ max;
            if (t < a[i]) a[i] = t;
        }
        int tM = ilogb(max), tA = ilogb(k);
        if (tM < tA) return 0;
        if (tM == tA) k ^= max;
        --free;
    }
    return k ? 0 : 1 << free;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d", &n, &k);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        printf("%d\n", solve());
    }
    return 0;
}
