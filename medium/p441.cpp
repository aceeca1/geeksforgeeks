#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <string>
using namespace std;

struct BigInt {
    static constexpr auto M = 1000000000;
    vector<uint32_t> v;

    BigInt(uint32_t n = 0): v({n}) {}

    operator string() {
        string ans = to_string(v.back());
        char s[11];
        for (int i = v.size() - 2; i >= 0; --i) {
            sprintf(s, "%09" PRIu32, v[i]);
            ans += s;
        }
        return ans;
    }

    BigInt& operator-=(const BigInt& that) {
        int carry = 0;
        for (int i = 0; i < v.size(); ++i) {
            carry += v[i];
            if (i < that.v.size()) carry -= that.v[i];
            v[i] = (carry + M) % M;
            carry -= v[i];
            carry /= M;
        }
        while (v.size() > 1 && !v.back()) v.pop_back();
        return *this;
    }

    BigInt& operator*=(uint32_t that) {
        int64_t carry = 0;
        for (int i = 0; i < v.size(); ++i) {
            carry += int64_t(v[i]) * that;
            v[i] = carry % M;
            carry /= M;
        }
        if (carry) v.emplace_back(carry);
        return *this;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        BigInt ans(n);
        ans *= m;
        ans -= BigInt(1);
        ans *= m;
        ans *= n;
        if (n >= 1 && m >= 2) {
            BigInt a(n - 1);
            a *= (m - 2) << 2;
            ans -= a;
        }
        if (m >= 1 && n >= 2) {
            BigInt a(m - 1);
            a *= (n - 2) << 2;
            ans -= a;
        }
        printf("%s\n", string(ans).c_str());
    }
    return 0;
}
