#include <cstdio>
#include <cstdint>
using namespace std;

constexpr int M = 1000000007;

int64_t powM(int64_t a, int64_t b) {
    int64_t ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % M;
        a = a * a % M;
    }
    return ans;
}

int m, n, p;

int calc() {
    if (m < 0) return -1;
    int64_t ans = 1, d = 1;
    for (int i = 1; i < n; ++i) {
        ans = ans * (m + i) % M;
        d = d * i % M;
    }
    return ans * powM(d, M - 2) % M;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d%d", &m, &n, &p);
        m -= p;
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            m -= b;
        }
        printf("%d\n", calc());
    }
    return 0;
}
