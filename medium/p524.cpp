#include <cstdio>
#include <queue>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        priority_queue<int, vector<int>, greater<int>> pq;
        while (n--) {
            int a;
            scanf("%d", &a);
            pq.emplace(a);
        }
        int ans = 0;
        while (pq.size() > 1) {
            int k1 = pq.top();
            pq.pop();
            int k2 = pq.top();
            pq.pop();
            k1 += k2;
            pq.emplace(k1);
            ans += k1;
        }
        printf("%d\n", ans);
    }
    return 0;
}
