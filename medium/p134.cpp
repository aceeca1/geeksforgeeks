#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), c(1);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            int t = lower_bound(c.begin() + 1, c.end(), a[i]) - c.begin();
            if (t == c.size()) c.emplace_back(a[i]);
            else if (a[i] < c[t]) c[t] = a[i];
            if (t > ans) ans = t;
        }
        printf("%d\n", ans);
    }
    return 0;
}
