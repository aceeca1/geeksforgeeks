#include <cstdio>
#include <vector>
using namespace std;

int ans;

void sub(const vector<int>& a1, const vector<int>& a2) {
    vector<pair<int, int>> v1, v2;
    for (int i = 0; i < a1.size(); ++i) {
        int se = i + a1[i] - 1;
        if (v1.empty() || se > v1.back().second)
            v1.emplace_back(i, se);
    }
    for (int i = a2.size() - 1; i >= 0; --i) {
        int fi = i - a2[i] + 1;
        if (v2.empty() || fi < v2.back().first)
            v2.emplace_back(fi, i);
    }
    int p = 0, q = v2.size() - 1;
    for (;;) {
        if (q < 0) break;
        while (p < v1.size() &&
            (v1[p].second < v2[q].second || v1[p].first < v2[q].first)) ++p;
        if (p >= v1.size()) break;
        int t = v2[q].second - v1[p].first + 1;
        if (t > ans) ans = t;
        --q;
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<vector<int>> a1(n, vector<int>(n));
        vector<int> b0(n), b1(n);
        for (int i = 0; i < n; ++i) {
            int e = 0;
            for (int j = 0; j < n; ++j) {
                char c;
                scanf(" %c", &c);
                if (c == 'X') a1[i][j] = min(++e, b0[j] = b1[j] + 1);
                else e = b0[j] = 0;
            }
            swap(b0, b1);
        }
        vector<vector<int>> a2(n, vector<int>(n));
        for (int i = 0; i < n; ++i) b1[i] = 0;
        for (int i = n - 1; i >= 0; --i) {
            int e = 0;
            for (int j = n - 1; j >= 0; --j)
                if (a1[i][j]) a2[i][j] = min(++e, b0[j] = b1[j] + 1);
                else e = b0[j] = 0;
            swap(b0, b1);
        }
        ans = 0;
        for (int i = -(n - 1); i <= n - 1; ++i) {
            int jS = max(0, i);
            int jT = min(n - 1, n - 1 + i);
            b0.resize(jT - jS + 1);
            b1.resize(jT - jS + 1);
            for (int j = jS; j <= jT; ++j) {
                b0[j - jS] = a2[j][j - i];
                b1[j - jS] = a1[j][j - i];
            }
            sub(b0, b1);
        }
        printf("%d\n", ans);
    }
    return 0;
}
