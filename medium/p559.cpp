#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

bool head;

bool valid(const int* s, const int* t, int lb, int ub) {
    if (s == t) return true;
    if (s[0] < lb || ub < s[0]) return false;
    const int* m = upper_bound(s + 1, t, s[0]);
    return valid(s + 1, m, lb, s[0]) && valid(m, t, s[0], ub);
}

void put(const int* s, const int* t) {
    if (s == t) return;
    const int* m = upper_bound(s + 1, t, s[0]);
    put(s + 1, m);
    put(m, t);
    if (!head) putchar(' ');
    head = false;
    printf("%d", s[0]);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        if (valid(&a[0], &a[n], 0x80000000, 0x7fffffff)) {
            head = true;
            put(&a[0], &a[n]);
            printf("\n");
        } else printf("NO\n");
    }
    return 0;
}
