#include <cstdio>
#include <vector>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n + 1);
        for (int i = 1; i <= n; ++i) scanf("%d", &a[i]);
        for (int i = 1; i <= n; ++i) {
            int b;
            scanf("%d", &b);
            a[i] -= b;
            a[i] += a[i - 1];
        }
        unordered_map<int, int> b;
        int ans = 0;
        for (int i = 0; i <= n; ++i) {
            auto t = b.emplace(a[i], i);
            if (!t.second) {
                int k = i - t.first->second;
                if (k > ans) ans = k;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
