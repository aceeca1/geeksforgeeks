#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        for (int i = 0; i < n - 1; i += 2)
            swap(a[i], a[i + 1]);
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
