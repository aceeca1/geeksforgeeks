#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, d = 0, ans = 0;
        scanf("%d", &n);
        while (~n & 1) { n >>= 1; ++d; }
        int i = 1;
        for (; i * i < n; ++i) ans += !(n % i);
        if (d) ans <<= 1;
        ans += d && i * i == n;
        ans += ~d & 1 && i * i == n;
        printf("%d\n", ans);
    }
    return 0;
}
