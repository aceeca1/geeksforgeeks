#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110000];
        scanf("%s", s);
        int state = 1;
        for (int i = 0; s[i]; ++i) switch (s[i]) {
            case 'R': if (state) state = 2; break;
            case 'Y': state = state == 2 ? 3 : state == 3 ? 1 : 0; break;
            default: state = 0;
        }
        printf("%s\n", state ? "YES" : "NO");
    }
    return 0;
}
