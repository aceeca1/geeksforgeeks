#include <cstdio>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        string s;
        cin >> s;
        int n;
        scanf("%d", &n);
        if (n == 1) {
            printf("%s\n", s.c_str());
            continue;
        }
        vector<string> a(n);
        int j = 0, d = 1;
        for (int i = 0; i < s.size(); ++i) {
            a[j] += s[i];
            if (j == n - 1) d = -1;
            else if (j == 0) d = 1;
            j += d;
        }
        for (int i = 0; i < a.size(); ++i)
            printf("%s", a[i].c_str());
        printf("\n");
    }
    return 0;
}
