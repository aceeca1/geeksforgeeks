#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, k;
        scanf("%d%d%d", &n, &m, &k);
        vector<vector<int>> a(n, vector<int>(m));
        while (k--) {
            int r, c;
            scanf("%d%d", &r, &c);
            a[r - 1][c - 1] = true;
        }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (!i && !j) a[i][j] = !a[i][j];
                else if (a[i][j]) a[i][j] = 0;
                else {
                    int t = 0;
                    if (i) t += a[i - 1][j];
                    if (j) t += a[i][j - 1];
                    a[i][j] = t % 1000000007;
                }
        printf("%d\n", a[n - 1][m - 1]);
    }
    return 0;
}
