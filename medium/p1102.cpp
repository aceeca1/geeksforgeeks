#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> l(t), r(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d%d", &l[ti], &r[ti]);
        if (r[ti] > m) m = r[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    p[0] = 0;
    for (int i = 1; i <= m; ++i) p[i] = p[i - 1] + (p[i] > i);
    for (int ti = 0; ti < t; ++ti) {
        int ans = r[ti] - l[ti] + 1 - ((p[r[ti]] - p[l[ti] - 1]) << 1);
        printf("%d\n", ans - (l[ti] == 1));
    }
    return 0;
}
