// The same as p393
#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        vector<int> a(n), b(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            if (i) b[i] = b[i - 1];
            if (a[i] > b[i]) b[i] = a[i];
            ans -= a[i];
        }
        int p = 0;
        for (int i = n - 1; i >= 0; --i) {
            if (a[i] > p) p = a[i];
            if (p < b[i]) b[i] = p;
            ans += b[i];
        }
        printf("%d\n", ans);
    }
    return 0;
}
