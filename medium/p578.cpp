#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), b(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            b[i] = i && a[i] > a[i - 1] ? b[i - 1] + 1 : 1;
        }
        int ans = 0, c = 0;
        for (int i = n - 1; i >= 0; --i) {
            c = i < n - 1 && a[i] > a[i + 1] ? c + 1 : 1;
            int t = b[i] + c - 1;
            if (t > ans) ans = t;
        }
        printf("%d\n", ans);
    }
    return 0;
}
