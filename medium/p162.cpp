#include <cstdio>
using namespace std;

double maxLevel(int m, int n) {
    double ans = 0, a = 1;
    for (int i = 0; i <= n; ++i) {
        ans += a;
        a = a * (m - i) / (i + 1);
    }
    return ans;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k, l = 1, r = 1;
        scanf("%d%d", &n, &k);
        while (maxLevel(r, n) <= k) r <<= 1;
        while (l < r) {
            int mid = (l + r) >> 1;
            if (maxLevel(mid, n) <= k) l = mid + 1;
            else r = mid;
        }
        printf("%d\n", l);
    }
    return 0;
}
