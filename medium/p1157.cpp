#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, sum = 0, a0, aK;
        scanf("%d", &n);
        for (int i = 0; i < n; ++i) {
            int a;
            scanf("%d", &a);
            sum += a;
            if (i == 0) a0 = a;
            else if (i == n - 1) aK = a;
        }
        sum /= n - 2;
        printf("%d %d\n", sum - a0 + 1, n - (sum - a0));
    }
    return 0;
}
