#include <cstdio>
#include <vector>
using namespace std;

constexpr int M = 1000000007;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        if (m > n * 9) { printf("-1\n"); continue; }
        vector<int> a0(m + 1), a1(m + 1);
        a0[0] = a1[0] = 1;
        while (n--) {
            swap(a0, a1);
            for (int j = 1; j <= m; ++j) {
                a0[j] = (a0[j - 1] + a1[j]) % M;
                if (j >= 10) a0[j] = (a0[j] + (M - a1[j - 10])) % M;
            }
        }
        printf("%d\n", (a0[m] + (M - a1[m])) % M);
    }
    return 0;
}
