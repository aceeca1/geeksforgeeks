#include <cstdio>
#include <vector>
#include <algorithm>
#include <unordered_set>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n + n);
        for (int i = n; i < n + n; ++i) scanf("%d", &a[i]);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        unordered_set<int> b;
        int z = 0;
        for (int i = 0; i < n + n; ++i)
            if (b.emplace(a[i]).second) a[z++] = a[i];
        a.resize(z);
        vector<int> c = a;
        nth_element(c.begin(), c.end() - n, c.end());
        int th = *(c.end() - n);
        bool head = true;
        for (int i = 0; i < z; ++i) if (a[i] >= th) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
