#include <cstdio>
#include <stack>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        stack<int> s;
        s.emplace(-1);
        bool head = true;
        while (n--) {
            int a;
            scanf("%d", &a);
            while (s.top() >= a) s.pop();
            if (!head) putchar(' ');
            head = false;
            printf("%d", s.top());
            s.emplace(a);
        }
        printf("\n");
    }
    return 0;
}
