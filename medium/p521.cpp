#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        double a0, a1 = 1, a2 = 0, a3 = 0, a4 = 0;
        scanf("%d", &n);
        while (n--) {
            a0 = a1 + a4;
            a4 = a3;
            a3 = a2;
            a2 = a1;
            a1 = a0;
        }
        printf("%.0f\n", a1);
    }
    return 0;
}
