#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a0(n), a1(n);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                scanf("%d", &a0[j]);
                int k = a1[j];
                if (j && a1[j - 1] > k) k = a1[j - 1];
                if (j < n - 1 && a1[j + 1] > k) k = a1[j + 1];
                a0[j] += k;
            }
            swap(a0, a1);
        }
        printf("%d\n", *max_element(a1.begin(), a1.end()));
    }
    return 0;
}
