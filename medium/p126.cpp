#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<pair<int, int>> a(n);
        for (int i = 0; i < n; ++i)
            scanf("%d%d", &a[i].first, &a[i].second);
        sort(a.begin(), a.end());
        int z = 0;
        for (int i = 1; i < n; ++i)
            if (a[i].first > a[z].second) a[++z] = a[i];
            else if (a[i].second > a[z].second) a[z].second = a[i].second;
        for (int i = 0; i <= z; ++i) printf("%d %d ", a[i].first, a[i].second);
        printf("\n");
    }
    return 0;
}
