#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            if (b >= a.size()) a.resize(b + 1);
            ++a[b];
        }
        vector<int> b(a.size() + 1);
        for (int i = a.size() - 1; i; --i) b[i] = a[i] + b[i + 1];
        for (int i = 1; i < a.size(); ++i) b[i] += b[i - 1];
        for (int i = 1; i < a.size(); ++i) a[i] = a[i] * i + a[i - 1];
        int ans = 0x7fffffff;
        if (k > a.size() - 2) k = a.size() - 2;
        for (int i = 0; i <= a.size() - 2 - k; ++i) {
            int t = a[i] + b[a.size() - 1] - b[i + k + 1];
            if (t < ans) ans = t;
        }
        printf("%d\n", ans);
    }
    return 0;
}
