#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

struct Node {
    int u, p, pEnd, w, v;
    bool operator<(const Node& that) const { return v > that.v; }
    Node& calcV(const vector<int>& a) {
        v = u + w * a[p];
        return *this;
    }
    Node(int u_, int p_, int pEnd_, int w_, const vector<int>& a):
        u(u_), p(p_), pEnd(pEnd_), w(w_) { calcV(a); }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2, n;
        scanf("%d%d%d", &n1, &n2, &n);
        vector<int> a(n);
        a[0] = 1;
        priority_queue<Node> pq;
        pq.emplace(n1 * a[0], 0, 0, -n2, a);
        pq.emplace(-n2 * a[0], 0, 0, n1, a);
        putchar('1');
        int z = 1;
        for (int i = 2; z < n; ++i) {
            while (!pq.empty() && pq.top().v < i) {
                auto pqH = pq.top();
                pq.pop();
                if (pqH.p == pqH.pEnd) continue;
                if (pqH.w > 0) ++pqH.p; else --pqH.p;
                pq.emplace(pqH.calcV(a));
            }
            if (pq.top().v == i) continue;
            a[z++] = i;
            pq.emplace(n1 * a[z - 1], z - 1, 0, -n2, a);
            pq.emplace(-n2 * a[z - 1], 0, z - 1, n1, a);
            printf(" %d", a[z - 1]);
        }
        printf("\n");
    }
    return 0;
}
