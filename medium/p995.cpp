#include <cstdio>
#include <cstring>
using namespace std;

int e[26][26], v[26];

void visit(int no) {
    v[no] = 0;
    for (int i = 0; i < 26; ++i)
        if (v[i] && (e[no][i] || e[i][no])) visit(i);
}

bool is() {
    memset(v, 0, sizeof(v));
    for (int i = 0; i < 26; ++i) {
        int b = 0;
        for (int j = 0; j < 26; ++j) {
            v[i] += e[i][j] + e[j][i];
            b += e[i][j] - e[j][i];
        }
        if (b) return false;
    }
    for (int i = 0; i < 26; ++i) if (v[i]) {
        visit(i);
        break;
    }
    for (int i = 0; i < 26; ++i)
        if (v[i]) return false;
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        memset(e, 0, sizeof(e));
        int n;
        scanf("%d", &n);
        while (n--) {
            char s[11];
            scanf("%s", s);
            ++e[s[0] - 'a'][s[strlen(s) - 1] - 'a'];
        }
        printf("%d\n", is());
    }
    return 0;
}
