#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n + 1);
        for (int i = 1; i <= n; ++i) {
            scanf("%d", &a[i]);
            a[i] += a[i - 1];
        }
        vector<vector<int>> b(n + 1);
        for (int i = n; i; --i) {
            b[i].resize(n + 2 - i);
            for (int j = 1; j <= n + 1 - i; ++j) {
                int t1 = b[i][j - 1];
                if (i < n) {
                    int t2 = b[i + 1][j - 1];
                    if (t2 < t1) t1 = t2;
                }
                b[i][j] = a[i + j - 1] - a[i - 1] - t1;
            }
        }
        printf("%d\n", b[1][n]);
    }
    return 0;
}
