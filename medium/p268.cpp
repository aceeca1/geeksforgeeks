#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        int p = -1, ans = n * (n + 1) >> 1;
        for (int i = 0; i <= n; ++i) {
            int a;
            if (i < n) scanf("%d", &a);
            if (i < n && a <= k) {
                if (p == -1) p = i;
            } else if (p != -1) {
                p = i - p;
                ans -= p * (p + 1) >> 1;
                p = -1;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
