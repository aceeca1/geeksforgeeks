#include <cstdio>
#include <utility>
using namespace std;

double a[3][3];
bool b[3];

int getRank() {
    for (int i = 0; i < 3; ++i) {
        double max = -1.0 / 0.0;
        int j_max, k_max;
        for (int j = i; j < 3; ++j)
            for (int k = 0; k < 3; ++k) if (!b[k]) {
                double t = a[j][k];
                if (t < 0) t = -t;
                if (t > max) {
                    max = t;
                    j_max = j;
                    k_max = k;
                }
            }
        if (max < 1e-12) return i;
        for (int j = 0; j < 3; ++j) swap(a[i][j], a[j_max][j]);
        double c = a[i][k_max];
        for (int j = 0; j < 3; ++j) a[i][j] /= c;
        for (int j = 0; j < 3; ++j) if (j != i) {
            c = a[j][k_max];
            for (int k = 0; k < 3; ++k) a[j][k] -= c * a[i][k];
        }
        b[k_max] = true;
    }
    return 3;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                scanf("%lf", &a[i][j]);
        for (int i = 0; i < 3; ++i) b[i] = false;
        printf("%d\n", getRank());
    }
    return 0;
}
