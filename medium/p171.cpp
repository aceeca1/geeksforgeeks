#include <cstdio>
#include <vector>
#include <stack>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        vector<int> a(n + 1);
        stack<int> s;
        for (int i = 0; i <= n; ++i) {
            if (i < n) scanf("%d", &a[i]);
            while (!s.empty() && a[s.top()] > a[i]) {
                int k = s.top();
                s.pop();
                int k1 = s.empty() ? -1 : s.top();
                int v = a[k] * (i - k1 - 1);
                if (v > ans) ans = v;
            }
            s.emplace(i);
        }
        printf("%d\n", ans);
    }
    return 0;
}
