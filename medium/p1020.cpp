#include <cstdio>
#include <deque>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        char s[33];
        scanf("%d%s", &n, s);
        int p = 0, q = 0;
        deque<int> d;
        for (; s[q]; ++q) {
            while (!d.empty() && s[d.back()] > s[q]) d.pop_back();
            d.emplace_back(q);
            if (q >= n) {
                int dH = d.front();
                d.pop_front();
                putchar(s[dH]);
            }
        }
        printf("\n");
    }
    return 0;
}
