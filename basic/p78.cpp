#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        printf("%d", a[n - 1]);
        for (int i = n - 2; i >= 0; --i) printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
