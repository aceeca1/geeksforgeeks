#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[55], s2[55];
        scanf("%s%s", s1, s2);
        bool inS2[256]{};
        for (int i = 0; s2[i]; ++i) inS2[s2[i]] = true;
        for (int i = 0; s1[i]; ++i) if (!inS2[s1[i]]) putchar(s1[i]);
        printf("\n");
    }
    return 0;
}
