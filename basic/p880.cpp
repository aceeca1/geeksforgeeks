#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    p[1] = 1;
    for (int i = 2; i <= m; ++i) {
        int v = p[i] < i ? p[i] : i;
        int a = i / v;
        p[i] = p[a] * (v - bool(a % v));
    }
    for (int ni: n) {
        int i = 1, ans = 0;
        for (; i * i < ni; ++i)
            if (!(ni % i)) ans += p[i] + p[ni / i];
        if (i * i == ni) ans += p[i];
        printf("%d\n", ans);
    }
    return 0;
}
