#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n & (n - 1)) {
            printf("-1\n");
            continue;
        }
        printf("%d\n", ilogb(n) + 1);
    }
    return 0;
}
