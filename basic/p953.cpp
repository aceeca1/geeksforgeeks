#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 1;
        scanf("%d", &n);
        while (n--) {
            int a, b, c;
            scanf("%d", &a);
            b = ans;
            c = a;
            while (c) {
                b %= c;
                swap(b, c);
            }
            ans *= a / b;
        }
        printf("%d\n", ans);
    }
    return 0;
}
