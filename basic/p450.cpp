#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", ((n >> 4) + ((n & 0xf) << 4)));
    }
    return 0;
}
