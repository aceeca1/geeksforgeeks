#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, n;
        scanf("%d%d%d", &a, &b, &n);
        printf("%d\n", a + (b - a) * (n - 1));
    }
    return 0;
}
