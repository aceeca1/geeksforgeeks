#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110], ans;
        scanf("%s", s);
        int a[256]{}, m = 0;
        for (int i = 0; s[i]; ++i) ++a[s[i]];
        for (int i = 0; i < 256; ++i)
            if (a[i] > m) { m = a[i]; ans = i; }
        printf("%c\n", ans);
    }
    return 0;
}
