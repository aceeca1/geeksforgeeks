#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k = 4;
        scanf("%d", &n);
        vector<int> a0(n + 1), a1(n + 1);
        a1[0] = 1;
        for (int i = 1; i <= k; ++i) {
            a0[0] = 0;
            for (int j = 1; j <= n; ++j) {
                a0[j] = a1[j - 1];
                if (j >= i) a0[j] += a0[j - i];
            }
            swap(a0, a1);
        }
        printf("%d\n", a1[n]);
    }
    return 0;
}
