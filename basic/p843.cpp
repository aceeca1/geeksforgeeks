#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        int ans = 0;
        for (int i = 0; s[i]; ++i)
            ans = (ans * 10 + (s[i] - '0')) % 7;
        printf("%d\n", ans);
    }
    return 0;
}
