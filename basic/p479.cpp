#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        ++n;
        int a = 1, b = 0, c = 0, e = 0, ans = 0;
        for (int i = n; i; i /= 10) c += !(i % 10);
        while (n) {
            int d = n % 10;
            c -= !d;
            if (c) ans += a * d;
            else if (d) {
                if (n == d) ans += e;
                else ans += a;
                ans += b * (d - 1);
            }
            n /= 10;
            e += b * 9;
            b = b * 9 + a;
            a *= 10;
        }
        printf("%d\n", ans);
    }
    return 0;
}
