#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        unordered_set<int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            a.emplace(b);
        }
        int ans = 0;
        for (auto i: a) ans += i;
        printf("%d\n", ans);
    }
    return 0;
}
