#include <cstdio>
#include <vector>
#include <algorithm>
#include <iterator>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        vector<int> a1(n1), a2(n2);
        for (int i = 0; i < n1; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n2; ++i) scanf("%d", &a2[i]);
        sort(a1.begin(), a1.end());
        sort(a2.begin(), a2.end());
        auto e1 = unique(a1.begin(), a1.end());
        auto e2 = unique(a2.begin(), a2.end());
        vector<int> a;
        set_union(a1.begin(), e1, a2.begin(), e2, back_inserter(a));
        printf("%d", a[0]);
        for (int i = 1; i < a.size(); ++i) printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
