#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cstring>
#include <vector>
#include <string>
using namespace std;

struct BigInt {
    static constexpr uint64_t M = 1000000000000000000;
    vector<uint64_t> v;

    // Deprecated: sscanf is slow
    BigInt(const char* s, int z) {
        for (int i = z; i > 0; i -= 18) {
            int p = i - 18;
            if (p < 0) p = 0;
            v.emplace_back();
            auto format = '%' + to_string(i - p) + SCNu64;
            sscanf(s + p, format.c_str(), &v.back());
        }
    }

    operator string() {
        string ans = to_string(v.back());
        char s[20];
        for (int i = v.size() - 2; i >= 0; --i) {
            sprintf(s, "%018" PRIu64, v[i]);
            ans += s;
        }
        return ans;
    }

    BigInt& operator+=(const BigInt& that) {
        if (that.v.size() > v.size()) v.resize(that.v.size());
        uint64_t carry = 0;
        for (int i = 0; i < v.size(); ++i) {
            carry += v[i];
            if (i < that.v.size()) carry += that.v[i];
            v[i] = carry % M;
            carry /= M;
        }
        if (carry) v.emplace_back(carry);
        return *this;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf("%s", s);
        BigInt a1(s, strlen(s));
        scanf("%s", s);
        BigInt a2(s, strlen(s));
        printf("%s\n", string(a1 += a2).c_str());
    }
    return 0;
}
