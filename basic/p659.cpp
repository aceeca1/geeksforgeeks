#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        n %= 6;
        printf("%s\n", !n || n == 1 || n == 3 ? "yes" : "no");
    }
    return 0;
}
