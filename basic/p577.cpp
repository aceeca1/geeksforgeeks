#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x, ans = 0;
        scanf("%d%d", &n, &x);
        while (n--) {
            int c;
            scanf("%d", &c);
            ans += c == x;
        }
        printf("%d\n", ans ? ans : -1);
    }
    return 0;
}
