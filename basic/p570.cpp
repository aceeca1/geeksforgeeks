#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        bool head = true;
        for (int i = ceil(cbrt(a)); i <= cbrt(b); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", i * i * i);
        }
        if (head) printf("No\n");
        else printf("\n");
    }
    return 0;
}
