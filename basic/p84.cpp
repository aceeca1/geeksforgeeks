#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n + 1);
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            ++a[b];
        }
        printf("%d", a[1]);
        for (int i = 2; i <= n; ++i)
            printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
