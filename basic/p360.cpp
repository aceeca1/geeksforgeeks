// Almost the same as p924
#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, d;
        scanf("%d%d", &n, &d);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        bool head = true;
        for (int i = d; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        for (int i = 0; i < d; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
