#include <cstdio>
#include <algorithm>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        printf("%s\n", equal(s.begin(), s.end(), s.rbegin()) ? "Yes" : "No");
    }
    return 0;
}
