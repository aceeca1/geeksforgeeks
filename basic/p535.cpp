#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> c(n);
        for (int i = 0; i < n; ++i) scanf("%d", &c[i]);
        swap(c[k - 1], c[n - k]);
        printf("%d", c[0]);
        for (int i = 1; i < n; ++i) printf(" %d", c[i]);
        printf("\n");
    }
    return 0;
}
