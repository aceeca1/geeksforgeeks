#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int r, c;
        scanf("%d%d", &r, &c);
        vector<int> a(r), b(c);
        for (int i = 0; i < r; ++i)
            for (int j = 0; j < c; ++j) {
                int d;
                scanf("%d", &d);
                a[i] |= d;
                b[j] |= d;
            }
        bool head = true;
        for (int i = 0; i < r; ++i)
            for (int j = 0; j < c; ++j) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", a[i] | b[j]);
            }
        printf("\n");
    }
    return 0;
}
