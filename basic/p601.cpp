#include <cstdio>
#include <queue>
#include <functional>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        priority_queue<int, vector<int>, greater<int>> pq;
        while (n--) {
            int a;
            scanf("%d", &a);
            if (pq.size() < k) pq.emplace(a);
            else if (a > pq.top()) {
                pq.pop();
                pq.emplace(a);
            }
        }
        vector<int> a;
        while (!pq.empty()) {
            a.emplace_back(pq.top());
            pq.pop();
        }
        printf("%d", a.back());
        for (int i = a.size() - 2; i >= 0; --i)
            printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
