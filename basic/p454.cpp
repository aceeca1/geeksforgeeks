#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf(" %[^\n]", s);
        *remove(s, s + strlen(s), ' ') = 0;
        printf("%s\n", s);
    }
    return 0;
}
