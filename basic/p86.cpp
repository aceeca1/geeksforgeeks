#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char n[22];
        scanf("%s", n);
        int ans = 0;
        for (int i = 0; n[i]; ++i) ans += n[i] - '0';
        printf("%d\n", ans);
    }
    return 0;
}
