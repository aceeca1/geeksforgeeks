#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int n, a = 0;
        sscanf(s, "%d", &n);
        for (int i = 0; s[i]; ++i) a += s[i] - '0';
        printf("%s\n", n % a ? "No" : "Yes");
    }
    return 0;
}
