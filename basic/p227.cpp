#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        int p = a, q = b;
        while (q) {
            p %= q;
            swap(p, q);
        }
        printf("%d %d\n", a / p * b, p);
    }
    return 0;
}
