#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a[3]{};
        scanf("%d", &n);
        while (n--) {
            int b;
            scanf("%d", &b);
            ++a[b % 3];
        }
        int ans0 = (a[0] * (a[0] - 1) >> 1) * (a[0] - 2) / 3;
        int ans1 = (a[1] * (a[1] - 1) >> 1) * (a[1] - 2) / 3;
        int ans2 = (a[2] * (a[2] - 1) >> 1) * (a[2] - 2) / 3;
        int ans3 = a[0] * a[1] * a[2];
        int ans4 = a[0] * (a[0] - 1) >> 1;
        int ans5 = a[1] * a[2];
        printf("%d\n", ans0 + ans1 + ans2 + ans3 + ans4 + ans5);
    }
    return 0;
}
