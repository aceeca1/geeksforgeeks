#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            int k = i + 2;
            for (int j = i + 1; j < n; ++j) {
                while (k < n && a[k] < a[i] + a[j]) ++k;
                ans += k - j - 1;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
