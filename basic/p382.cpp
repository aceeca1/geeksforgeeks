#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int z = strlen(s);
        sort(s, s + z);
        bool head = true;
        for (;;) {
            if (!head) putchar(' ');
            head = false;
            printf("%s", s);
            if (!next_permutation(s, s + z)) break;
        }
        printf("\n");
    }
    return 0;
}
