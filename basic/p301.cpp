#include <cstdio>
#include <vector>
using namespace std;

bool is(int n) {
    vector<int> a;
    int i = 1;
    for (; i * i < n; ++i) if (!(n % i)) {
        a.emplace_back(i);
        if (i != 1) a.emplace_back(n / i);
    }
    if (i * i == n) a.emplace_back(i);
    int s = 0;
    for (int ai: a) s += ai;
    if (s <= n) return false;
    vector<int> c(n + 1);
    c[0] = 1;
    for (int ai: a)
        for (int i = n; i >= ai; --i)
            c[i] |= c[i - ai];
    return !c[n];
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (!is(n)) printf("Not ");
        printf("Weird\n");
    }
    return 0;
}
