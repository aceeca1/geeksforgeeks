#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        double m;
        scanf("%d%lf", &n, &m);
        int w = 1 << (ilogb(n + 2) + 1), s = 0;
        vector<int> a(w + w);
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            for (b += w; b > 1; b >>= 1) {
                if (~b & 1) s += a[b + 1];
                ++a[b];
            }
        }
        s *= -2;
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            s += b;
        }
        printf("%d\n", s >= m * n);
    }
    return 0;
}
