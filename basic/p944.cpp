#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int64_t ans = 0;
        for (int i = 0; i < 32; ++i) {
            int k = 0;
            for (int j = 0; j < n; ++j) k += (a[j] >> i) & 1;
            ans += k * int64_t(k - 1) >> 1 << i;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
