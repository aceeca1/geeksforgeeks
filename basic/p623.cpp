#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int z = n - 1, max = -1;
        for (int i = n - 1; i >= 0; --i)
            if (a[i] > max) a[z--] = max = a[i];
        printf("%d", a[++z]);
        for (++z; z < n; ++z) printf(" %d", a[z]);
        printf("\n");
    }
    return 0;
}
