#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf(" x +%d =%d", &a, &b);
        printf("%d\n", b - a);
    }
    return 0;
}
