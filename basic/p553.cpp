#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> a(m);
    queue<int> a2, a3, a5;
    a2.emplace(2);
    a3.emplace(3);
    a5.emplace(5);
    a[0] = 1;
    for (int i = 1; i < m; ++i) {
        int k1 = a2.front();
        int k2 = a3.front();
        int k3 = a5.front();
        int k = k1 < k2 ? k1 < k3 ? k1 : k3 : k2 < k3 ? k2 : k3;
        if (k == k1) a2.pop();
        if (k == k2) a3.pop();
        if (k == k3) a5.pop();
        a[i] = k;
        a5.emplace(k * 5);
        if (k % 5) {
            a3.emplace(k * 3);
            if (k % 3) a2.emplace(k * 2);
        }
    }
    for (int ni: n) printf("%d\n", a[ni - 1]);
    return 0;
}
