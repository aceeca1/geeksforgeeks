#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0, len = 0, a1, a0;
        scanf("%d", &n);
        while (n--) {
            scanf("%d", &a0);
            len = a0 > a1 ? len + 1 : 1;
            ans += len - 1;
            a1 = a0;
        }
        printf("%d\n", ans);
    }
    return 0;
}
