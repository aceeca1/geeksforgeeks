#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int64_t f(int n, int k) {
    n = (n - 1) / k;
    return (int64_t(1 + n) * n >> 1) * k;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%" PRId64 "\n", f(n, 3) + f(n, 7) - f(n, 21));
    }
    return 0;
}
