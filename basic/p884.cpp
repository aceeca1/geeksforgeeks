#include <cstdio>
using namespace std;

char s[33];
bool head;

void put(int k) {
    while (s[k] && s[k] != '?') ++k;
    if (!s[k]) {
        if (!head) putchar(' ');
        head = false;
        printf("%s", s);
        return;
    }
    s[k] = '0';
    put(k + 1);
    s[k] = '1';
    put(k + 1);
    s[k] = '?';
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        head = true;
        put(0);
        printf("\n");
    }
    return 0;
}
