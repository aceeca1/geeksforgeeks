#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        bool head = true;
        for (int i = 1; i <= n; ++i) {
            int a;
            scanf("%d", &a);
            if (a != i) continue;
            if (!head) putchar(' ');
            head = false;
            printf("%d", a);
        }
        if (head) printf("Not Found\n");
        else printf("\n");
    }
    return 0;
}
