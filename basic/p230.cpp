#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n = 3;
        vector<int> three(3);
        vector<vector<int>> a(3, three), b(3, three), c(3, three);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                scanf("%d", &a[i][j]);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                scanf("%d", &b[i][j]);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                for (int k = 0; k < n; ++k)
                    c[i][k] += a[i][j] * b[j][k];
        for (int i = 0; i < n; ++i)
            printf("%d %d %d\n", c[i][0], c[i][1], c[i][2]);
    }
    return 0;
}
