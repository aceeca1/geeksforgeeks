#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        int64_t ans = 1;
        --a, --b, a += b;
        for (int i = 1; i <= b; ++i) ans = ans * (a + 1 - i) / i;
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
