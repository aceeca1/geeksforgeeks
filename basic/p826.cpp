#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int s, d;
        scanf("%d%d", &s, &d);
        string a(d, 0);
        --s;
        for (int i = d - 1; i >= 0; --i) {
            int k = s < 9 ? s : 9;
            a[i] = k + '0';
            s -= k;
        }
        if (a[0] == '9') printf("-1\n");
        else {
            ++a[0];
            printf("%s\n", a.c_str());
        }
    }
    return 0;
}
