#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a = 1, c = 0, d = 0, ans = 0;
        scanf("%d", &n);
        ++n;
        for (int i = n; i; i /= 10) d += !(i % 10) || i % 10 > 5;
        while (n) {
            int b = n % 10;
            d -= !b || b > 5;
            if (!d) {
                if (n == b) ans += c;
                --b;
                if (b > 5) b = 5;
                if (b > 0) ans += a * b;
            }
            n /= 10;
            a *= 5;
            c += a;
        }
        printf("%d\n", ans);
    }
    return 0;
}
