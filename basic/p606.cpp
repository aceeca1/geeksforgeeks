#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        int ans = 0x7fffffff;
        for (int i = 0; i < n - 1; ++i) {
            int k = a[i + 1] - a[i];
            if (k < ans) ans = k;
        }
        printf("%d\n", ans);
    }
    return 0;
}
