#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        for (int i = 1; i <= n; ++i) ans ^= i;
        while (--n) {
            int a;
            scanf("%d", &a);
            ans ^= a;
        }
        printf("%d\n", ans);
    }
    return 0;
}
