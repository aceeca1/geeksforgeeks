#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        int from = 1, via = 2, to = 3;
        while (n > 1) {
            int p = 1 << (n - 1);
            if (k == p) break;
            if (k < p) { --n; swap(via, to); }
            else { --n; k -= p; swap(from, via); }
        }
        printf("%d %d\n", from, to);
    }
    return 0;
}
