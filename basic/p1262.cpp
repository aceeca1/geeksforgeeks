#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        n %= 5;
        if (n) printf("%d\n", n);
        else printf("-1\n");
    }
    return 0;
}
