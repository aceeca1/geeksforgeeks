#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf("%s", s);
        int e = 0, n = 0;
        for (int i = 0; s[i]; ++i) switch (s[i]) {
            case 'E': ++e; break;
            case 'N': ++n; break;
            case 'S': --n; break;
            case 'W': --e;
        }
        while (e > 0) { putchar('E'); --e; }
        while (n > 0) { putchar('N'); --n; }
        while (n < 0) { putchar('S'); ++n; }
        while (e < 0) { putchar('W'); ++e; }
        printf("\n");
    }
    return 0;
}
