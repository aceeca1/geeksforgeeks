#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> b(n);
        for (int i = 0; i < n; ++i) scanf("%d", &b[i]);
        vector<int> a = b;
        sort(a.begin(), a.end());
        a.resize(unique(a.begin(), a.end()) - a.begin());
        for (int i = 0; i < n; ++i)
            b[i] = lower_bound(a.begin(), a.end(), b[i]) - a.begin();
        int m = 1 << (ilogb(a.size() + 2) + 1);
        a.resize(m + m);
        for (int i = 0; i < a.size(); ++i) a[i] = 0;
        for (int i = n - 1; i >= 0; --i) {
            int ans = 0;
            for (int k = b[i] + m; k > 1; k >>= 1) {
                if (~k & 1) ans += a[k + 1];
                ++a[k];
            }
            b[i] = ans;
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", b[i]);
        }
        printf("\n");
    }
    return 0;
}
