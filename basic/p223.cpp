#include <cstdio>
using namespace std;

int cubic(int n) { return n * n * n; }

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[22];
        scanf("%s", s);
        int n;
        sscanf(s, "%d", &n);
        for (int i = 0; s[i]; ++i) n -= cubic(s[i] - '0');
        printf("%s\n", n ? "No" : "Yes");
    }
    return 0;
}
