#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        printf("%d\n", equal(s.begin(), s.end(), s.rbegin()) ? 1 : 2);
    }
    return 0;
}
