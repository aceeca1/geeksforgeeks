#include <cstdio>
using namespace std;

double cross(double ax, double ay, double bx, double by) {
    return ax * by - ay * bx;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        double ax, ay, bx, by, cx, cy, dx, dy;
        scanf("%lf%lf%lf%lf%lf%lf%lf%lf",
            &ax, &ay, &bx, &by, &cx, &cy, &dx, &dy);
        double c1 = cross(ax - dx, ay - dy, bx - dx, by - dy);
        double c2 = cross(bx - dx, by - dy, cx - dx, cy - dy);
        double c3 = cross(cx - dx, cy - dy, ax - dx, ay - dy);
        printf("%s\n", !c1 || !c2 || !c3 ||
            c1 > 0 && c2 > 0 && c3 > 0 ||
            c1 < 0 && c2 < 0 && c3 < 0 ? "Yes" : "No");
    }
    return 0;
}
