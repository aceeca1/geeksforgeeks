#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        printf("%d", a[0]);
        for (int i = 1; i < n; ++i) printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
