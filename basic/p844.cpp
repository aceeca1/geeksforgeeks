#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        double h, m;
        scanf("%lf%lf", &h, &m);
        if (m == 60.0) m = 0.0;
        double hA = h * 30.0 + m * 0.5;
        double mA = m * 6.0;
        double a = abs(hA - mA);
        if (a > 180.0) a = 360.0 - a;
        printf("%d\n", int(a));
    }
    return 0;
}
