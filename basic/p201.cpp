#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        int i = 1;
        for (; i * i < n; ++i) ans += !(n % i);
        ans <<= 1;
        ans += i * i == n;
        printf("%d\n", ans);
    }
    return 0;
}
