#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int ans = 0;
        for (int i = 0; s[i]; ++i) switch (s[i]) {
            case '2': case '3': case '5': case '7': ans += s[i] - '0';
        }
        printf("%d\n", ans);
    }
    return 0;
}
