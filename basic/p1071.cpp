#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, g, s;
        scanf("%d%d%d%d", &n, &m, &g, &s);
        int l = 1, r = n, k = 0;
        while (l < r) {
            int mid = (l + r) >> 1;
            if (mid == m) break;
            ++k;
            if (mid < m) l = mid + 1;
            else r = mid - 1;
        }
        int a1 = m * g, a2 = k * s;
        printf("%d\n", a1 < a2 ? 1 : a1 == a2 ? 0 : 2);
    }
    return 0;
}
