#include <cstdio>
#include <cstdint>
#include <vector>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    vector<int> n(t);
    int m = 0;
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    string s;
    int64_t a0 = 1, a1 = 0, a2;
    for (int i = 0; s.size() < m; ++i) {
        s += to_string(a0);
        a2 = a1;
        a1 = a0;
        a0 = a1 + a2;
    }
    for (int ni: n) printf("%c\n", s[ni - 1]);
    return 0;
}
