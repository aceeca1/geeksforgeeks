#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        vector<int> a1(n1), a2(n2);
        for (int i = 0; i < n1; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n2; ++i) scanf("%d", &a2[i]);
        sort(a1.begin(), a1.end());
        sort(a2.begin(), a2.end());
        auto e1 = unique(a1.begin(), a1.end());
        auto e2 = unique(a2.begin(), a2.end());
        auto e = set_intersection(a1.begin(), e1, a2.begin(), e2, a1.begin());
        bool head = true;
        for (auto i = a1.begin(); i != e; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", *i);
        }
        if (head) printf("Zero\n");
        printf("\n");
    }
    return 0;
}
