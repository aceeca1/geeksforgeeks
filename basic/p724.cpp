#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    double s5 = sqrt(5), a = 0.5 * (s5 + 1);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%.0f\n", pow(a, n + 1) / s5);
    }
    return 0;
}
