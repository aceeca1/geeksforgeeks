#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, n;
        scanf("%d%d%d", &a, &b, &n);
        printf("%.0f\n", floor(a * pow(double(b) / a, n - 1)));
    }
    return 0;
}
