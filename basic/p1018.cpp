#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n + 2);
        a[0] = -1;
        for (int i = 1; i <= n; ++i) scanf("%d", &a[i]);
        a[n + 1] = 1000;
        sort(a.begin(), a.end());
        printf("[");
        for (int i = 1; i <= n + 1; ++i) {
            int s = a[i - 1] + 1;
            int t = a[i] - 1;
            if (s > t) continue;
            if (s == t) printf(" %d", s);
            else printf(" %d-%d", s, t);
        }
        printf(" ]\n");
    }
    return 0;
}
