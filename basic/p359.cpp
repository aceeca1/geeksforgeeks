#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int m, n, x;
        scanf("%d%d%d", &m, &n, &x);
        vector<int64_t> a0(x + 1), a1(x + 1);
        a1[0] = 1;
        for (int i = 1; i <= n; ++i) {
            a0[0] = 0;
            for (int j = 1; j <= x; ++j) {
                a0[j] = a0[j - 1] + a1[j - 1];
                if (j > m) a0[j] -= a1[j - m - 1];
            }
            swap(a0, a1);
        }
        printf("%" PRId64 "\n", a1[x]);
    }
    return 0;
}
