#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, minA;
        scanf("%d%d", &n, &minA);
        int maxA = minA;
        while (--n) {
            int a;
            scanf("%d", &a);
            if (a < minA) minA = a;
            if (a > maxA) maxA = a;
        }
        printf("%d %d\n", maxA, minA);
    }
    return 0;
}
