#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, k;
        int64_t c = 1;
        scanf("%d%d%d", &a, &b, &k);
        while (b--) c *= a;
        while (--k) c /= 10;
        printf("%d\n", int(c % 10));
    }
    return 0;
}
