#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%s\n", n & 3 || !(n % 100) && n % 400 ? "No" : "Yes");
    }
    return 0;
}
