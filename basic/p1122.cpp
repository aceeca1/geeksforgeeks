#include <cstdio>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a;
        scanf("%d", &a);
        char s[110];
        int z = 0;
        for (; a; a >>= 1) s[z++] = '0' + (a & 1);
        s[z] = 0;
        reverse(s, s + z);
        printf("%s", s);
        scanf("%s", s);
        printf(" %d", int(strtol(s, nullptr, 2)));
        scanf("%d", &a);
        printf(" %X", a);
        scanf("%x", &a);
        printf(" %d\n", a);
    }
    return 0;
}
