#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1 = 0, a2 = 0;
        scanf("%d", &n);
        while (n--) {
            scanf("%d", &a0);
            a0 += a2;
            if (a1 > a0) a0 = a1;
            a2 = a1;
            a1 = a0;
        }
        printf("%d\n", a1);
    }
    return 0;
}
