#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = -2;
        scanf("%d", &n);
        while (n) {
            ans += n % 3 + 1;
            n /= 3;
        }
        printf("%d\n", ans);
    }
    return 0;
}
