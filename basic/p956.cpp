#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[220];
        scanf("%s", s);
        const char *a = "LIE";
        int aLen = strlen(a), z = 0;
        for (int i = 0; ; ) {
            auto p = strstr(s + i, a);
            if (z && s[z - 1] != ' ') s[z++] = ' ';
            if (p) {
                int k = p - s - i;
                strncpy(s + z, s + i, k);
                z += k;
                i += k + aLen;
            } else {
                strcpy(s + z, s + i);
                break;
            }
        }
        int i = strlen(s) - 1;
        while (s[i] == ' ') s[i--] = 0;
        printf("%s\n", s);
    }
    return 0;
}
