#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[22];
        scanf("%s", s);
        int n;
        sscanf(s, "%d", &n);
        string s1(s);
        auto s2 = to_string(n * n);
        if (!equal(s1.rbegin(), s1.rend(), s2.rbegin())) printf("Not ");
        printf("Automorphic\n");
    }
    return 0;
}
