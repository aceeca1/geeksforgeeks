#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int a1 = s[0] == 'R' ? 0 : s[0] == 'P' ? 1 : 2;
        int a2 = s[1] == 'R' ? 0 : s[1] == 'P' ? 1 : 2;
        int a = (a1 + 3 - a2) % 3;
        printf("%s\n", a == 2 ? "B" : a == 1 ? "A" : "DRAW");
    }
    return 0;
}
