#include <cstdio>
#include <cstring>
using namespace std;

bool is(char* s) {
    int a = 0;
    for (int i = 0; s[i]; ++i) {
        if (s[i] == '0') continue;
        int k = 1 << (s[i] - '0');
        if (a & k) return false;
        a += k;
    }
    return a == 0x3fe;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[33];
        scanf("%s", s);
        int n;
        sscanf(s, "%d", &n);
        if (n < 100) {
            printf("Number should be atleast three digits\n");
            continue;
        }
        int i = strlen(s);
        i += sprintf(s + i, "%d", n + n);
        sprintf(s + i, "%d", n + n + n);
        if (!is(s)) printf("Not ");
        printf("Fascinating\n");
    }
    return 0;
}
