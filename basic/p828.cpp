#include <cstdio>
using namespace std;

bool is(int n) {
    for (int i = 2;; ++i) {
        if (n == 1) return true;
        if (n % i) return false;
        n /= i;
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", is(n));
    }
    return 0;
}
