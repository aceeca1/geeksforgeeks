#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int64_t> a(m + 1);
    a[0] = 1;
    for (int i = 1; i <= m; ++i) a[i] = a[i - 1] * i;
    for (int ni: n) printf("%" PRId64 "\n", a[ni]);
    return 0;
}
