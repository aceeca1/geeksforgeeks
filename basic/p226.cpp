#include <cstdio>
#include <cstdint>
#include <random>
using namespace std;

bool isWitness(int n, int d, int r, int64_t a) {
    int64_t x = 1;
    while (d) {
        if (d & 1) x = x * a % n;
        a = a * a % n;
        d >>= 1;
    }
    if (x == 1 || x == n - 1) return false;
    for (int i = 1; i < r; ++i) {
        x = x * x % n;
        if (x == 1) return true;
        if (x == n - 1) return false;
    }
    return true;
}

bool isPrime(int n) {
    static default_random_engine rand;
    if (n <= 1) return false;
    if (n == 2 || n == 3) return true;
    if (!(n & 1)) return false;
    int d = n - 1, r = 0;
    while (!(d & 1)) { d >>= 1; ++r; }
    uniform_int_distribution<int> u(2, n - 2);
    for (int i = 0; i < 10; ++i)
        if (isWitness(n, d, r, u(rand))) return false;
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%s\n", isPrime(n) ? "Yes" : "No");
    }
    return 0;
}
