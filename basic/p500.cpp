#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        ++n;
        int a = 0, c = -1, ans = 0;
        for (int i = n; i; i >>= 1) c += i & 1;
        for (; n; n >>= 1) {
            if (n & 1) {
                ans += c-- << a;
                if (a) ans += a << (a - 1);
            }
            ++a;
        }
        printf("%d\n", ans);
    }
    return 0;
}
