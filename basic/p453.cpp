// Almost the same as p538
#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a1 = 0, a2 = 0;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a > a1) { a2 = a1; a1 = a; }
            else if (a > a2) a2 = a;
        }
        printf("%d\n", a1 * a2);
    }
    return 0;
}
