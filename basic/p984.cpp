#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        int64_t ans = 0, b = 1;
        for (int i = n - 1; i >= 0; i -= 2) {
            ans += a[i] * b;
            if (i == 0) break;
            ans += a[i - 1] * b;
            b *= 10;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
