#include <cstdio>
#include <vector>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        vector<int> s1(10), s2(10), a0(10), a1(10);
        for (int i = 0; i < 10; ++i) scanf("%d", &s1[i]);
        for (int i = 0; i < 10; ++i) scanf("%d", &s2[i]);
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                if (s1[i] == s2[j]) a0[j] = (j ? a1[j - 1] : 0) + 1;
                else a0[j] = max(j ? a0[j - 1] : 0, a1[j]);
            }
            swap(a0, a1);
        }
        printf("%d\n", a1[9]);
    }
    return 0;
}
