#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        int64_t n = a ^ b;
        n *= 0x200040008001ULL;
        n &= 0x111111111111111ULL;
        n %= 0xf;
        printf("%d\n", int(n));
    }
    return 0;
}
