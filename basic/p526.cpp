#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        int64_t a = 1, b = 0, ans = 0;
        scanf("%d", &n);
        while (n--) {
            ans += 9 * b;
            b = 9 * b + a;
            a *= 10;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
