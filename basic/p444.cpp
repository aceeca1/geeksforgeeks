#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int i1 = 0, i2 = 0;
        bool head = true;
        for (;;) {
            while (i1 < n && a[i1] < 0) ++i1;
            if (i1 == n) break;
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i1++]);
            while (i2 < n && a[i2] >= 0) ++i2;
            if (i2 == n) break;
            printf(" %d", a[i2++]);
        }
        printf("\n");
    }
    return 0;
}
