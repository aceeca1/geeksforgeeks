#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        n >>= 1;
        int64_t ans = 1;
        for (int i = 1; i <= n; ++i) {
            if (i != n) ans *= n + n + 1 - i;
            ans /= i;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
