#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n;
        scanf("%" SCNd64, &n);
        printf("%s\n", !n || n & (n - 1) ? "NO" : "YES");
    }
    return 0;
}
