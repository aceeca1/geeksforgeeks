// Almost the same as p77
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x;
        scanf("%d%d", &n, &x);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int p = find(a.begin(), a.end(), x) - a.begin();
        printf("%d\n", p == a.size() ? -1 : p + 1);
    }
    return 0;
}
