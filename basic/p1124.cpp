#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        int ans = 0;
        while (n--) {
            int a;
            scanf("%d", &a);
            ans += (a + k - 1) / k;
        }
        printf("%d\n", ans);
    }
    return 0;
}
