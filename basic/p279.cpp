#include <cstdio>
#include <cstdlib>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        printf("%d\n", int(strtol(s, nullptr, 2)));
    }
    return 0;
}
