#include <queue>
using namespace std;

void convert(node* head, TreeNode*& root) {
    if (!root) root = new TreeNode;
    root->data = head->data;
    head = head->next;
    root->left = root->right = nullptr;
    queue<TreeNode*> q;
    q.emplace(root);
    while (!q.empty()) {
        TreeNode *qH = q.front();
        q.pop();
        if (!head) return;
        qH->left = new TreeNode;
        qH->left->left = qH->left->right = nullptr;
        qH->left->data = head->data;
        head = head->next;
        q.emplace(qH->left);
        if (!head) return;
        qH->right = new TreeNode;
        qH->right->left = qH->right->right = nullptr;
        qH->right->data = head->data;
        head = head->next;
        q.emplace(qH->right);
    }
}
