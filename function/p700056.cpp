
#include <string>
#include <algorithm>
using namespace std;

bool Compute(Node* root) {
    string s;
    for (; root; root = root->next)
        s += root->data;
    return equal(s.begin(), s.end(), s.rbegin());
}
