int getCount(node* head) {
    int ans = 0;
    for (; head; head = head->next) ++ans;
    return ans;
}
