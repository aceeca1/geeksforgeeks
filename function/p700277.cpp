void printPat(int n) {
    for (int i = n; i; --i) {
        for (int j = n; j; --j)
            for (int k = i; k; --k)
                printf("%d ", j);
        putchar('$');
    }
}
