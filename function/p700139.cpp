int getCountOfNode(node *root, int l, int h) {
    if (!root) return 0;
    int ans = 0;
    if (l <= root->data) ans += getCountOfNode(root->left, l, h);
    if (l <= root->data && root->data <= h) ++ans;
    if (root->data <= h) ans += getCountOfNode(root->right, l, h);
    return ans;
}
