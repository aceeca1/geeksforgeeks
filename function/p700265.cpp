node* merge(node* n1, node* n2) {
    node h, *p = &h;
    while (n1 || n2)
        if (n1 && (!n2 || n1->data < n2->data)) {
            p = p->next = n1;
            n1 = n1->next;
        } else {
            p = p->next = n2;
            n2 = n2->next;
        }
    p->next = nullptr;
    return h.next;
}

node* mergeKList(node* arr[], int N) {
    ++N;
    while (N > 1) {
        int z = 0, i = 0;
        for (; i + 1 < N; i += 2)
            arr[z++] = merge(arr[i], arr[i + 1]);
        if (i + 1 == N) arr[z++] = arr[i];
        N = z;
    }
    return arr[0];
}
