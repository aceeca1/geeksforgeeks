void display(Node *head) {
    bool h = true;
    for (; head; head = head->next) {
        if (!h) putchar(' ');
        h = false;
        printf("%d", head->data);
    }
}
