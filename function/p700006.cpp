void rearrange(node* head) {
    node h0, *p0 = &h0, *p1 = nullptr;
    while (head) {
        p0 = p0->next = head;
        head = head->next;
        if (!head) break;
        auto n = head->next;
        head->next = p1;
        p1 = head;
        head = n;
    }
    p0->next = p1;
}
