int tour(petrolPump p[], int n) {
    int sum = 0, minPSum = 0, i_min = -1;
    for (int i = 0; i < n; ++i) {
        sum += p[i].petrol - p[i].distance;
        if (sum < minPSum) { minPSum = sum; i_min = i; }
    }
    return sum < 0 ? -1 : (i_min + 1) % n;
}

