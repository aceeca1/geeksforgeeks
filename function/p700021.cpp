void linkdelete(node* head, int M, int N) {
    for (;;) {
        for (int i = 1; i < M; ++i) {
            if (!head) return;
            head = head->next;
        }
        if (!head) return;
        auto p = head->next;
        for (int i = 0; i < N; ++i) {
            if (!p) {
                head->next = nullptr;
                return;
            }
            auto n = p->next;
            delete p;
            p = n;
        }
        head = head->next = p;
    }
}
