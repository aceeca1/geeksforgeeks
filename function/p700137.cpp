
#include <utility>
using namespace std;

void reverse(node** head_ref) {
    for (auto i = *head_ref; i; i = i->prev) {
        swap(i->prev, i->next);
        if (!i->prev) *head_ref = i;
    }
}
