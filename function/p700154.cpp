bool head = true;

void put(Node* root, int k) {
    if (!root) return;
    if (!k) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", root->data);
        return;
    }
    put(root->left, k - 1);
    put(root->right, k - 1);
}

void printKdistance(Node *root, int k) {
    head = true;
    put(root, k);
}
