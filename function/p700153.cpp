vector<int> v;

void put(Node* p, int d) {
    if (!p) return;
    if (d == v.size()) v.emplace_back();
    v[d] = p->data;
    put(p->left, d + 1);
    put(p->right, d + 1);
}

void rightView(Node *root) {
    put(root, 0);
    bool head = true;
    for (int i = 0; i < v.size(); ++i) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", v[i]);
    }
    v.clear();
}
