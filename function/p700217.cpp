
#include <queue>
using namespace std;

void bfs(int s, vector<int> g[], bool vis[]) {
    bool head = true;
    queue<int> q;
    q.emplace(s);
    vis[s] = true;
    while (!q.empty()) {
        auto qH = q.front();
        q.pop();
        if (!head) putchar(' ');
        head = false;
        printf("%d", qH);
        for (int i: g[qH]) if (!vis[i]) {
            q.emplace(i);
            vis[i] = true;
        }
    }
}
