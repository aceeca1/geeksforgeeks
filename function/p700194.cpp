
#include <algorithm>
using namespace std;

int transitionPoint(int arr[], int n) {
    return lower_bound(arr, arr + n, 1) - arr;
}
