void sortList(Node** head) {
    Node *p0 = nullptr, *t0 = nullptr, h1, *p1 = &h1;
    for (auto i = *head; i;)
        if (i->data < 0) {
            auto n = i->next;
            i->next = p0;
            p0 = i;
            if (!t0) t0 = i;
            i = n;
        } else {
            p1 = p1->next = i;
            i = i->next;
        }
    p1->next = nullptr;
    if (t0) {
        t0->next = h1.next;
        *head = p0;
    } else *head = h1.next;
}
