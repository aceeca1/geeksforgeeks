node* deleteNode(node* root, int x) {
    node pRoot;
    pRoot.left = root;
    pRoot.right = nullptr;
    node *p = root, *pp = &pRoot;
    int d = 0;
    while (p->data != x)
        if (x < p->data) {
            pp = p;
            p = p->left;
            d = 0;
        } else {
            pp = p;
            p = p->right;
            d = 1;
        }
    if (!p->left) {
        auto q = p->right;
        delete p;
        if (d) pp->right = q;
        else pp->left = q;
    } else {
        auto q = p->left, qq = p;
        d = 0;
        while (q->right) {
            qq = q;
            q = q->right;
            d = 1;
        }
        p->data = q->data;
        p = q->left;
        delete q;
        if (d) qq->right = p;
        else qq->left = p;
    }
    return pRoot.left;
}
