Node* removeDuplicates(Node* root) {
    if (!root) return nullptr;
    Node *h = root, *p = h;
    for (auto i = root; i->next;)
        if (i->data != i->next->data) {
            p = p->next = i->next;
            i = i->next;
        } else {
            auto q = i->next;
            i->next = q->next;
            delete q;
        }
    p->next = nullptr;
    return h;
}
