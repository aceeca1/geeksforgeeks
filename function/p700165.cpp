
#include <algorithm>
using namespace std;

int ans;

int put(Node* node) {
    if (!node) return 0;
    int d1 = put(node->left);
    int d2 = put(node->right);
    int d = d1 + d2 + 1;
    if (d > ans) ans = d;
    return max(d1, d2) + 1;
}

int diameter(Node* node) {
    ans = 0;
    put(node);
    return ans;
}
