node* addTwoLists(node* first, node* second) { // caller uses delete
    node h, *p = &h;
    int carry = 0;
    while (first || second) {
        if (first) {
            carry += first->data;
            first = first->next;
        }
        if (second) {
            carry += second->data;
            second = second->next;
        }
        p = p->next = new node;
        p->data = carry % 10;
        carry /= 10;
    }
    if (carry) {
        p = p->next = new node;
        p->data = carry;
    }
    p->next = nullptr;
    return h.next;
}
