int getSize(Node* node) {
    if (!node) return 0;
    return getSize(node->left) + getSize(node->right) + 1;
}
