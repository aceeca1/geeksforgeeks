
#include <unordered_map>
using namespace std;

Node* copyList(Node* head) { // caller use delete
    unordered_map<Node*, Node*> a;
    for (auto i = head; i; i = i->next) {
        a[i] = new Node;
        a[i]->data = i->data;
    }
    for (auto i = head; i; i = i->next) {
        a[i]->next = a[i->next];
        a[i]->arb = a[i->arb];
    }
    return a[head];
}
