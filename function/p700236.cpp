vector<node*> lookup(node* root, int n) {
    vector<node*> ans;
    for (;;) {
        ans.emplace_back(root);
        if (root->data == n) return ans;
        if (n < root->data) root = root->left;
        else root = root->right;
    }
}

node* LCA(node *root, int n1, int n2) {
    auto pA = lookup(root, n1);
    auto qA = lookup(root, n2);
    while (pA.size() > qA.size()) pA.pop_back();
    while (pA.size() < qA.size()) qA.pop_back();
    while (pA.back() != qA.back()) {
        pA.pop_back();
        qA.pop_back();
    }
    return pA.back();
}
