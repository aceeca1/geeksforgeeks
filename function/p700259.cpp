#include <cstdio>
using namespace std;

void addFraction(int num1, int den1, int num2, int den2) {
    int x1 = den1, x2 = den2;
    while (x2) swap(x1 %= x2, x2);
    int den = den1 / x1 * den2;
    int num = num1 * (den / den1) + num2 * (den / den2);
    x1 = num, x2 = den;
    while (x2) swap(x1 %= x2, x2);
    printf("%d/%d\n", num / x1, den / x1);
}
