Node* mergeResult(Node* node1, Node* node2) {
    Node *h = nullptr;
    while (node1 || node2)
        if (node1 && (!node2 || node1->data < node2->data)) {
            auto n = node1->next;
            node1->next = h;
            h = node1;
            node1 = n;
        } else {
            auto n = node2->next;
            node2->next = h;
            h = node2;
            node2 = n;
        }
    return h;
}
