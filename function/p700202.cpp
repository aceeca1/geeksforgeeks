vector<vector<int>> a;

void put(node* p, int n) {
    if (!p) return;
    if (n == a.size()) a.emplace_back();
    a[n].emplace_back(p->data);
    put(p->left, n + 1);
    put(p->right, n + 1);
}

void reversePrint(node* root) {
    put(root, 0);
    bool head = true;
    for (int i = a.size() - 1; i >= 0; --i)
        for (int j = 0; j < a[i].size(); ++j) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i][j]);
        }
    a.clear();
}
