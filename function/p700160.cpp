
#include <vector>
#include <queue>
#include <cstdio>
using namespace std;

vector<int> aP, aN;

void bottomView(Node *root) {
    if (!root) return;
    queue<pair<Node*, int>> q;
    q.emplace(root, 0);
    aN.emplace_back();
    while (!q.empty()) {
        auto qH = q.front();
        q.pop();
        if (qH.second < 0) {
            int t = -qH.second;
            if (t == aN.size()) aN.emplace_back();
            aN[t] = qH.first->data;
        } else {
            int t = qH.second;
            if (t == aP.size()) aP.emplace_back();
            aP[t] = qH.first->data;
        }
        if (qH.first->left) q.emplace(qH.first->left, qH.second - 1);
        if (qH.first->right) q.emplace(qH.first->right, qH.second + 1);
    }
    bool head = true;
    for (int i = aN.size() - 1; i; --i) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", aN[i]);
    }
    aN.clear();
    for (int i = 0; i < aP.size(); ++i) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", aP[i]);
    }
    aP.clear();
}
