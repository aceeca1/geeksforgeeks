bool head;

void put(int* in, int* pre, int n) {
    if (!n) return;
    int k = 0;
    while (in[k] != pre[0]) ++k;
    put(in, pre + 1, k);
    put(in + k + 1, pre + k + 1, n - k - 1);
    if (!head) putchar(' ');
    head = false;
    printf("%d", pre[0]);
}

void printPostOrder(int in[], int pre[], int n) {
    head = true;
    put(in, pre, n);
}
