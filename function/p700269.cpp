
#include <cstdio>
using namespace std;

int convertFive(int n) {
    char s[11];
    sprintf(s, "%d", n);
    for (int i = 0; s[i]; ++i) if (s[i] == '0') s[i] = '5';
    sscanf(s, "%d", &n);
    return n;
}
