void Stack::push(int x) {
    arr[++top] = x;
}

int Stack::pop() {
    if (top < 0) return -1;
    return arr[top--];
}
