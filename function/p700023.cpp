void rotate(node** head_ref, int k) {
    auto p = *head_ref;
    while (--k) p = p->next;
    auto q = p;
    while (q->next) q = q->next;
    q->next = *head_ref;
    *head_ref = p->next;
    p->next = nullptr;
}
