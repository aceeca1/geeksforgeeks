bool head;

void put(int s, vector<int> g[], bool vis[]) {
    if (!head) putchar(' ');
    head = false;
    printf("%d", s);
    vis[s] = true;
    for (int i: g[s]) if (!vis[i]) put(i, g, vis);
}

void dfs(int s, vector<int> g[], bool vis[]) {
    head = true;
    put(s, g, vis);
}
