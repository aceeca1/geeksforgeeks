int GetNth(node* head, int index) {
    while (index--) head = head->next;
    return head->data;
}
