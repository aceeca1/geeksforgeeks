int put(Node* p) {
    if (!p) return 0;
    int s1 = put(p->left);
    int s2 = put(p->right);
    s1 += s2;
    s2 = s1 + p->data;
    p->data = s1;
    return s2;
}

void toSumTree(Node* node) {
    put(node);
}

