vector<vector<int>> a;

void put(node* p, int n) {
    if (!p) return;
    if (n == a.size()) a.emplace_back();
    a[n].emplace_back(p->data);
    put(p->left, n + 1);
    put(p->right, n + 1);
}

void printSpiral(node *root) {
    put(root, 0);
    bool head = true;
    for (int i = 0; i < a.size(); ++i)
        if (i & 1) for (int j = 0; j < a[i].size(); ++j) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i][j]);
        } else for (int j = a[i].size() - 1; j >= 0; --j) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i][j]);
        }
    a.clear();
}
