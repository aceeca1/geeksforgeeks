int getId(int M[MAX][MAX], int n) {
    int ans = 0;
    for (int i = 1; i < n; ++i)
        if (M[ans][i]) ans = i;
    for (int i = 0; i < n; ++i) if (i != ans)
        if (M[ans][i] || !M[i][ans]) return -1;
    return ans;
}
