Node* SortedMerge(Node* head1, Node* head2) {
    Node h, *p = &h;
    while (head1 || head2)
        if (head1 && (!head2 || head1->data < head2->data)) {
            p = p->next = head1;
            head1 = head1->next;
        } else {
            p = p->next = head2;
            head2 = head2->next;
        }
    return h.next;
}
