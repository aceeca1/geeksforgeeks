int compare(Node* list1, Node* list2) {
    for (;;) {
        if (!list1) return list2 ? -1 : 0;
        if (!list2) return 1;
        if (list1->c < list2->c) return -1;
        if (list1->c > list2->c) return 1;
        list1 = list1->next;
        list2 = list2->next;
    }
}
