
#include <unordered_set>
using namespace std;

void removeDuplicate(vector<int> &vect) {
    unordered_set<int> a;
    int z = 0;
    for (int i = 0; i < vect.size(); ++i)
        if (a.emplace(vect[i]).second) vect[z++] = vect[i];
    vect.resize(z);
}
