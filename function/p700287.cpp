#include <regex>
using namespace std;

int wildCard(string pattern, string str) {
    string reg;
    reg.reserve(pattern.size() << 1);
    for (char i: pattern) switch (i) {
        case '?': reg += '.'; break;
        case '*': reg += ".*"; break;
        default: reg += i;
    }
    return regex_match(str, regex(reg));
}
