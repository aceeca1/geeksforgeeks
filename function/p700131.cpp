void deleteNode(node** head_ref, node* del) {
    if (!del->prev) {
        del->next->prev = nullptr;
        *head_ref = del->next;
        delete del;
    } else if (!del->next) {
        del->prev->next = nullptr;
        delete del;
    } else {
        del->prev->next = del->next;
        del->next->prev = del->prev;
        delete del;
    }
}
