
#include <unordered_set>
using namespace std;

void printMat(int M[MAX][MAX], int row, int col) {
    unordered_set<long long> u;
    for (int i = 0; i < row; ++i) {
        long long s = 0;
        for (int j = 0; j < col; ++j) s += M[i][j] << j;
        if (!u.emplace(s).second) continue;
        for (int j = 0; j < col; ++j) printf("%d ", M[i][j]);
        printf("$");
    }
}
