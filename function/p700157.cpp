Node* buildTree(int in[], int post[], int n) { // caller use delete
    if (!n) return nullptr;
    int k = 0;
    while (in[k] != post[n - 1]) ++k;
    auto ans = new Node;
    ans->data = post[n - 1];
    ans->left = buildTree(in, post, k);
    ans->right = buildTree(in + k + 1, post + k, n - k - 1);
    return ans;
}
