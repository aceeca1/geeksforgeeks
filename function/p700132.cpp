
#include <utility>
using namespace std;

node* partition(node* l, node* h) {
    node *p0 = l, *p1 = l;
    for (auto i = l; i != h->next; i = i->next)
        if (i->data < h->data) {
            swap(i->data, p1->data);
            swap(p1->data, p0->data);
            p1 = p1->next;
            p0 = p0->next;
        } else if (i->data = h->data) {
            swap(i->data, p1->data);
            p1 = p1->next;
        }
    return p0;
}
