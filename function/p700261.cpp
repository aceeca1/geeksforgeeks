int getLevelDiff(Node *root) {
    if (!root) return 0;
    return root->data - getLevelDiff(root->left) - getLevelDiff(root->right);
}

