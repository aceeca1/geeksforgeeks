
#include <queue>
using namespace std;

bool Graph::isCyclic() {
    vector<int> visit(V);
    for (int i = 0; i < V; ++i) if (!visit[i]) {
        queue<int> q;
        visit[i] = true;
        q.emplace(i);
        while (!q.empty()) {
            int qH = q.front();
            q.pop();
            for (int j: adj[qH]) if (j != -1) {
                if (visit[j]) return true;
                visit[j] = true;
                q.emplace(j);
                for (int& k: adj[j]) if (k == qH) {
                    k = -1;
                    break;
                }
            }
        }
    }
    return false;
}
