int MinHeap::extractMin() {
    if (!heap_size) return -1;
    int ans = harr[1];
    deleteKey(1);
    return ans;
}

void MinHeap::deleteKey(int x) {
    harr[x] = harr[heap_size--];
    while (x + x <= heap_size) {
        if (x + x + 1 <= heap_size && harr[x + x + 1] < harr[x + x]) {
            if (harr[x + x + 1] < harr[x]) {
                swap(harr[x + x + 1], harr[x]);
                x = x + x + 1;
            } else break;
        } else {
            if (harr[x + x] < harr[x]) {
                swap(harr[x + x], harr[x]);
                x = x + x;
            } else break;
        }
    }
}

void MinHeap::insertKey(int x) {
    harr[++heap_size] = x;
    x = heap_size;
    while (x > 1)
        if (harr[x] < harr[x >> 1]) {
            swap(harr[x], harr[x >> 1]);
            x >>= 1;
        } else break;
}
