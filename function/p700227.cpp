int ans;

int put(Node* p) {
    if (!p) return 0x7fffffff;
    int m1 = put(p->left);
    int m2 = put(p->right);
    if (m2 < m1) m1 = m2;
    int t = p->data - m1;
    if (t > ans) ans = t;
    return p->data < m1 ? p->data : m1;
}

int maxDiff(Node* root) {
    ans = 0x80000000;
    put(root);
    return ans;
}
