int intersectPoint(Node* head1, Node* head2) {
    int z1 = 0, z2 = 0;
    for (auto i = head1; i; i = i->next) ++z1;
    for (auto i = head2; i; i = i->next) ++z2;
    while (z1 > z2) { head1 = head1->next; --z1; }
    while (z2 > z1) { head2 = head2->next; --z2; }
    while (head1 != head2) {
        head1 = head1->next;
        head2 = head2->next;
    }
    return head1 ? head1->data : -1;
}
