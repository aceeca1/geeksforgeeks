int detectloop(node* list) {
    if (!list) return 0;
    int n1 = 0, n2 = 1;
    auto x1 = list, x2 = list->next;
    while (x2 && x1 != x2) {
        if (n1 + n1 < n2) { n1 = n2; x1 = x2; }
        ++n2;
        x2 = x2->next;
    }
    return bool(x2);
}
