
#include <unordered_set>
using namespace std;

Node* removeDuplicates(Node* root) {
    Node h, *p = &h;
    unordered_set<int> a;
    while (root)
        if (a.emplace(root->data).second) {
            p = p->next = root;
            root = root->next;
        } else {
            auto n = root->next;
            delete root;
            root = n;
        }
    p->next = nullptr;
    return h.next;
}
