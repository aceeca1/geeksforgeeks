
#include <cmath>
#include <algorithm>
using namespace std;

int *constructST(int arr[], int n) { // caller use delete[]
    int m = 1 << (ilogb(n + 2) + 1);
    int *ans = new int[m + m];
    for (int i = 0; i < n; ++i) ans[m + 1 + i] = arr[i];
    for (int i = m - 1; i > 1; --i)
        ans[i] = min(ans[i + i], ans[i + i + 1]);
    return ans;
}

int RMQ(int st[], int n, int a, int b) {
    int m = 1 << (ilogb(n + 2) + 1);
    a += m;
    b += m + 2;
    int ans = 0x7fffffff;
    while ((a ^ b) != 1) {
        if ( b & 1 && st[b - 1] < ans) ans = st[b - 1];
        if (~a & 1 && st[a + 1] < ans) ans = st[a + 1];
        a >>= 1;
        b >>= 1;
    }
    return ans;
}
