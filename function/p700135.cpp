bool head;

void put(Node* p) {
    if (!p) return;
    put(p->left);
    if (!head) putchar(' ');
    head = false;
    printf("%d", p->data);
    put(p->right);
}

void inOrder(Node* root) {
    head = true;
    put(root);
}
