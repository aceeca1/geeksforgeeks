int count(node* head, int search_for) {
    int ans = 0;
    for (; head; head = head->next)
        ans += head->data == search_for;
    return ans;
}
