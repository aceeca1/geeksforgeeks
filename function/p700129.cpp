int isPowerOfFour(unsigned int n) {
    return n && (n & -n) == n && n & 0x55555555;
}
