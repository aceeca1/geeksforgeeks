void heapify(int arr[], int n, int p) {
    --arr;
    ++p;
    while (p + p <= n)
        if (p + p + 1 <= n && arr[p + p + 1] > arr[p + p]) {
            if (arr[p + p + 1] > arr[p]) {
                swap(arr[p + p + 1], arr[p]);
                p = p + p + 1;
            } else break;
        } else {
            if (arr[p + p] > arr[p]) {
                swap(arr[p + p], arr[p]);
                p = p + p;
            } else break;
        }
}

void buildHeap(int arr[], int n)  {
    for (int i = (n >> 1) - 1; i >= 0; --i) heapify(arr, n, i);
}
