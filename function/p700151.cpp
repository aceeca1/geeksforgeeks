
#include <utility>
using namespace std;

int partition(int arr[], int low, int high) {
    int p0 = low, p1 = low;
    for (int i = low; i <= high; ++i)
        if (arr[i] < arr[high]) {
            swap(arr[i], arr[p1]);
            swap(arr[p1++], arr[p0++]);
        } else if (arr[i] == arr[high])
            swap(arr[i], arr[p1++]);
    return p1 - 1;
}
