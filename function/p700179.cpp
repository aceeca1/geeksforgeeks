int put(Node* p) {
    int s = 0;
    if (p->left) s += put(p->left);
    if (p->right) s += put(p->right);
    if (p->left || p->right && p->data != s) throw false;
    return s + p->data;
}

bool isSumTree(Node* root) {
    if (!root) return true;
    try { put(root); }
    catch (bool e) { return false; }
    return true;
}
