bool head;

void put(node* p, int l, int h) {
    if (!p) return;
    if (l <= p->data) put(p->left, l, h);
    if (l <= p->data && p->data <= h) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", p->data);
    }
    if (p->data <= h) put(p->right, l, h);
}

void printNearNodes(node* root, int l, int h) {
    head = true;
    put(root, l, h);
}
