int bin_search(int A[], int left, int right, int k) {
    while (left < right) {
        int mid = (left + right) >> 1;
        if (A[mid] == k) return mid;
        else if (A[mid] < k) left = mid + 1;
        else right = mid - 1;
    }
    return A[left] == k ? left : -1;
}
