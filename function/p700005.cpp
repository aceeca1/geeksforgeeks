Node* reverse(Node* head) {
    Node *h = nullptr;
    while (head) {
        auto n = head->next;
        head->next = h;
        h = head;
        head = n;
    }
    return h;
}
