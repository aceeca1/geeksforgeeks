vector<Node*> a;

void put(Node* p, int n) {
    if (!p) return;
    if (n == a.size()) a.emplace_back();
    p->nextRight = a[n];
    a[n] = p;
    put(p->right, n + 1);
    put(p->left, n + 1);
}

void connect(Node* p) {
    put(p, 0);
    a.clear();
}

