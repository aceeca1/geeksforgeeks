// The same as p700103
node* reverse(node* head) {
    node *h = nullptr;
    while (head) {
        auto n = head->next;
        head->next = h;
        h = head;
        head = n;
    }
    return h;
}

void reorderList(node* head) {
    int z = 0;
    for (auto i = head; i; i = i->next) ++z;
    z = (z - 1) >> 1;
    auto p = head;
    while (z--) p = p->next;
    auto h1 = reverse(p->next);
    p->next = nullptr;
    node h, *q = &h;
    while (head) {
        q = q->next = head;
        head = head->next;
        if (!h1) break;
        q = q->next = h1;
        h1 = h1->next;
    }
    q->next = nullptr;
}
