bool head;

void put(Node* p) {
    if (p->left) {
        if (!p->right) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", p->left->data);
        }
        put(p->left);
    }
    if (p->right) {
        if (!p->left) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", p->right->data);
        }
        put(p->right);
    }
}

void printSibling(Node* node) {
    head = true;
    put(node);
}
