
#include <queue>
using namespace std;

int findIslands(int A[MAX][MAX], int N, int M) {
    int ans = 0;
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < M; ++j) if (A[i][j]) {
            ++ans;
            queue<pair<int, int>> q;
            q.emplace(i, j);
            A[i][j] = 0;
            while (!q.empty()) {
                auto qH = q.front();
                q.pop();
                for (int i = qH.first - 1; i <= qH.first + 1; ++i) {
                    if (i < 0 || N <= i) continue;
                    for (int j = qH.second - 1; j <= qH.second + 1; ++j) {
                        if (j < 0 || M <= j || !A[i][j]) continue;
                        q.emplace(i, j);
                        A[i][j] = 0;
                    }
                }
            }
        }
    return ans;
}
