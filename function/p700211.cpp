void Queue::push(int x) {
    arr[rear++] = x;
}

int Queue::pop() {
    if (front == rear) return -1;
    return arr[front++];
}
