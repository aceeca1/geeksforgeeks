int pu, qu;
vector<Node*> pr, pA, qA;

void preOrder(Node* u) {
    if (!u) return;
    pr.emplace_back(u);
    if (u->data == pu) pA = pr;
    if (u->data == qu) qA = pr;
    preOrder(u->left);
    preOrder(u->right);
    pr.pop_back();
}

int findDist(Node* root, int a, int b) {
    pu = a;
    qu = b;
    preOrder(root);
    int ans = 0;
    while (pA.size() > qA.size()) { pA.pop_back(); ++ans; }
    while (pA.size() < qA.size()) { qA.pop_back(); ++ans; }
    while (pA.back() != qA.back()) {
        pA.pop_back(); ++ans;
        qA.pop_back(); ++ans;
    }
    pA.clear();
    qA.clear();
    return ans;
}
