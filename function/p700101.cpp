Node* reverse(Node* head) {
    Node *h = nullptr;
    while (head) {
        auto n = head->next;
        head->next = h;
        h = head;
        head = n;
    }
    return h;
}

Node* compute(Node* head) {
    Node *h = nullptr;
    for (auto i = head; i; ) {
        while (h && h->data < i->data) {
            auto hn = h->next;
            delete h;
            h = hn;
        }
        auto n = i->next;
        i->next = h;
        h = i;
        i = n;
    }
    return reverse(h);
}
