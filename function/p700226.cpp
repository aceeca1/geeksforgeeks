
#include <cmath>
using namespace std;

long long int floorSqrt(long long int x) {
    return floor(sqrt((long double)x));
}
