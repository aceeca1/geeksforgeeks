void splitList(node* head, node** head1_ref, node** head2_ref) {
    int z = 1;
    node *p, *q = head;
    for (p = head; p->next != head; p = p->next) ++z;
    z = (z - 1) >> 1;
    while (z--) q = q->next;
    p->next = q->next;
    *head2_ref = q->next;
    q->next = head;
    *head1_ref = head;
}
