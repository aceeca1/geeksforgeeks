int max_sum(int A[], int N) {
    int sum = 0, u = 0;
    for (int i = 0; i < N; ++i) {
        sum += A[i];
        u += A[i] * i;
    }
    int ans = 0;
    for (int i = N - 1; i >= 0; --i) {
        if (u > ans) ans = u;
        u += sum - N * A[i];
    }
    return ans;
}
