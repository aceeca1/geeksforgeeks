
#include <vector>
#include <stack>
#include <utility>
using namespace std;

bool Graph::isCyclic() {
    vector<int> visit(V);
    for (int i = 0; i < V; ++i) if (!visit[i]) {
        stack<pair<int, int>> q;
        q.emplace(i, 1);
        while (!q.empty()) {
            auto qH = q.top();
            q.pop();
            if (qH.second == 2) visit[qH.first] = 2;
            else {
                visit[qH.first] = 1;
                q.emplace(qH.first, 2);
                for (int j: adj[qH.first]) {
                    if (visit[j] == 1) return true;
                    if (visit[j] == 2) continue;
                    q.emplace(j, 1);
                }
            }
        }
    }
    return false;
}
