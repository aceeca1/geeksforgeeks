Node* pp;

void put(Node* p) {
    if (!p) return;
    put(p->left);
    pp->right = p;
    p->left = pp;
    pp = p;
    put(p->right);
}

void BToDLL(Node* root, Node** head_ref) {
    Node h;
    pp = &h;
    put(root);
    h.right->left = pp->right = nullptr;
    *head_ref = h.right;
}
