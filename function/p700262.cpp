void printKAlmostPrimes(int k, int n) {
    constexpr int m = 110000;
    static int p[m];
    if (!p[0]) {
        int u = 0;
        for (int i = 2; i <= m; ++i) {
            int v = p[i];
            if (!v) u = p[u] = v = i;
            for (int w = 2; i * w <= m; w = p[w]) {
                p[i * w] = w;
                if (w >= v) break;
            }
        }
        p[u] = m + 1;
        for (int i = 2; i <= m; ++i)
            if (p[i] > i) p[i] = 1;
            else p[i] = p[i / p[i]] + 1;
    }
    bool head = true;
    for (int i = 2; n; ++i) if (p[i] == k) {
        if (!head) putchar(' ');
        head = false;
        printf("%d", i);
        --n;
    }
}
