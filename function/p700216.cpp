void insert(node** root, int val) {
    if (!*root) {
        *root = new node;
        (*root)->data = val;
        (*root)->left = nullptr;
        (*root)->right = nullptr;
        return;
    }
    auto p = *root;
    for (;;)
        if (val < p->data) {
            if (!p->left) {
                p->left = new node;
                p->left->data = val;
                p->left->left = nullptr;
                p->left->right = nullptr;
                break;
            } else p = p->left;
        } else if (val > p->data) {
            if (!p->right) {
                p->right = new node;
                p->right->data = val;
                p->right->left = nullptr;
                p->right->right = nullptr;
                break;
            } else p = p->right;
        } else break;
}
