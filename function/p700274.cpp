int findK(int A[MAX][MAX], int n, int m, int k) {
    int r = 0, c = 0, d = 0;
    while (--k) switch (d) {
        case 0: if (c + 1 == m) { d = 1; ++r; } else ++c; break;
        case 1: if (r + 1 == n) { d = 2; --c; } else ++r; break;
        case 2: if (c == 0)     { d = 3; --r; } else --c; break;
        case 3: if (r == 0)     { d = 4; ++c; } else --r;
    }
    return A[r][c];
}

