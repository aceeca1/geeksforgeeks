vector<vector<int>> a;

void put(node* p, int n) {
    if (!p) return;
    if (n == a.size()) a.emplace_back();
    a[n].emplace_back(p->data);
    put(p->left, n + 1);
    put(p->right, n + 1);
}

void let(node* p, int n) {
    if (!p) return;
    p->data = a[n].back();
    a[n].pop_back();
    let(p->right, n + 1);
    let(p->left, n + 1);
}

void reverseAlternate(node* root) {
    put(root, 0);
    bool head = true;
    for (int i = 0; i < a.size(); ++i)
        if (i & 1) reverse(a[i].begin(), a[i].end());
    let(root, 0);
}
