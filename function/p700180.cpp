Node* createTree(int parent[], int n) { // caller use delete
    vector<Node*> a(n);
    for (int i = 0; i < n; ++i) {
        a[i] = new Node;
        a[i]->data = i;
        a[i]->left = a[i]->right = nullptr;
    }
    int root;
    for (int i = 0; i < n; ++i)
        if (parent[i] == -1) root = i;
        else if (a[parent[i]]->left == nullptr)
            a[parent[i]]->left = a[i];
        else a[parent[i]]->right = a[i];
    return a[root];
}
