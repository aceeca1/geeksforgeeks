
#include <vector>
#include <queue>
#include <cstdio>
using namespace std;

vector<vector<int>> a;

void diagonalPrint(Node *root) {
    if (!root) return;
    queue<pair<Node*, int>> q;
    q.emplace(root, 0);
    while (!q.empty()) {
        auto qH = q.front();
        q.pop();
        int t = qH.second;
        if (t == a.size()) a.emplace_back();
        a[t].emplace_back(qH.first->data);
        if (qH.first->left) q.emplace(qH.first->left, qH.second + 1);
        if (qH.first->right) q.emplace(qH.first->right, qH.second);
    }
    bool head = true;
    for (int i = 0; i < a.size(); ++i)
        for (int j: a[i]) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", j);
        }
    a.clear();
}
