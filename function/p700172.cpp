bool isCircular(Node* head) {
    if (!head) return true;
    auto i = head->next;
    while (i && i != head) i = i->next;
    return i == head;
}
