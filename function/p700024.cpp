void pairWiseSwap(node* head) {
    for (; head && head->next; head = head->next->next)
        swap(head->data, head->next->data);
}
