int areMirror(Node* a, Node* b) {
    if (!a) return !b;
    if (!b) return false;
    if (a->data != b->data) return false;
    return areMirror(a->left, b->right) && areMirror(a->right, b->left);
}
