
#include <algorithm>
using namespace std;

int put(Node* p) {
    if (!p) return 0;
    int d1 = put(p->left);
    int d2 = put(p->right);
    if (d1 > d2 + 1 || d2 > d1 + 1) throw false;
    return max(d1, d2) + 1;
}

bool isBalanced(Node* root) {
    try { put(root); }
    catch (bool e) { return false; }
    return true;
}
