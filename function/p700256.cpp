
#include <vector>
#include <queue>
#include <cstdio>
using namespace std;

vector<vector<int>> aP, aN;

void verticalOrder(Node *root) {
    if (!root) return;
    queue<pair<Node*, int>> q;
    q.emplace(root, 0);
    aN.emplace_back();
    while (!q.empty()) {
        auto qH = q.front();
        q.pop();
        if (qH.second < 0) {
            int t = -qH.second;
            if (t == aN.size()) aN.emplace_back();
            aN[t].emplace_back(qH.first->data);
        } else {
            int t = qH.second;
            if (t == aP.size()) aP.emplace_back();
            aP[t].emplace_back(qH.first->data);
        }
        if (qH.first->left) q.emplace(qH.first->left, qH.second - 1);
        if (qH.first->right) q.emplace(qH.first->right, qH.second + 1);
    }
    for (int i = aN.size() - 1; i; --i) {
        for (int j: aN[i]) printf("%d ", j);
        putchar('$');
    }
    aN.clear();
    for (int i = 0; i < aP.size(); ++i) {
        for (int j: aP[i]) printf("%d ", j);
        putchar('$');
    }
    aP.clear();
}
