// Need initializer !!

void Queue::push(int x) {
    if (!rear) {
        front = rear = new QueueNode;
        rear->data = x;
        rear->next = nullptr;
    } else {
        auto n = new QueueNode;
        n->data = x;
        n->next = nullptr;
        rear = rear->next = n;
    }
}

int Queue::pop() {
    if (!front) return -1;
    auto n = front;
    front = front->next;
    if (!front) rear = nullptr;
    auto data = n->data;
    delete n;
    return data;
}
