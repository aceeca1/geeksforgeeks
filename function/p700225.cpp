bool hasPathSum(Node *node, int sum) {
    if (!node->left && !node->right) return node->data == sum;
    int nSum = sum - node->data;
    return node->left && hasPathSum(node->left, nSum) ||
        node->right && hasPathSum(node->right, nSum);
}
