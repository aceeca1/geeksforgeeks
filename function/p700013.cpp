node* reverse(node* head) {
    node *h = nullptr;
    while (head) {
        auto n = head->next;
        head->next = h;
        h = head;
        head = n;
    }
    return h;
}

node* reverse(node* head, int k) {
    node h, *p = &h;
    while (head) {
        auto q = head;
        for (int i = 1; i < k; ++i) {
            if (!q->next) break;
            q = q->next;
        }
        auto n = q->next;
        q->next = nullptr;
        auto nHead = reverse(head);
        p->next = nHead;
        p = head;
        head = n;
    }
    return h.next;
}
