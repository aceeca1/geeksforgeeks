Node *addOne(Node *head) {
    Node *p = nullptr;
    for (auto i = head; i; i = i->next)
        if (i->data != 9) p = i;
    if (!p) {
        p = new Node;
        p->data = 1;
        p->next = head;
        head = p;
    } else ++p->data;
    for (auto i = p->next; i; i = i->next)
        i->data = 0;
    return head;
}
