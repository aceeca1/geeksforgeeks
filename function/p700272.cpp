int countZeroes(int A[MAX][MAX], int N) {
    int k = N - 1, ans = 0;
    for (int i = 0; i < N; ++i) {
        while (k >= 0 && A[i][k] == 1) --k;
        ans += k + 1;
    }
    return ans;
}
