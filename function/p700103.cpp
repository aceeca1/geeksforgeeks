Node* reverse(Node* head) {
    Node *h = nullptr;
    while (head) {
        auto n = head->next;
        head->next = h;
        h = head;
        head = n;
    }
    return h;
}

Node* inPlace(Node *root) {
    int z = 0;
    for (auto i = root; i; i = i->next) ++z;
    z = (z - 1) >> 1;
    auto p = root;
    while (z--) p = p->next;
    auto h1 = reverse(p->next);
    p->next = nullptr;
    Node h, *q = &h;
    while (root) {
        q = q->next = root;
        root = root->next;
        if (!h1) break;
        q = q->next = h1;
        h1 = h1->next;
    }
    q->next = nullptr;
    return h.next;
}
