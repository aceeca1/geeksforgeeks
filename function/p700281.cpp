
#include <vector>
#include <queue>
using namespace std;

void serialize(Node *root, vector<int>& A) {
    if (!root) return;
    queue<Node*> q;
    q.emplace(root);
    A.emplace_back(root->data);
    while (!q.empty()) {
        auto qH = q.front();
        q.pop();
        if (qH->left) {
            A.emplace_back(qH->left->data);
            q.emplace(qH->left);
        } else A.emplace_back(-1);
        if (qH->right) {
            A.emplace_back(qH->right->data);
            q.emplace(qH->right);
        } else A.emplace_back(-1);
    }
    while (A.back() == -1) A.pop_back();
}

Node* deSerialize(vector<int>& A) { // caller use delete
    if (A.empty()) return nullptr;
    int p = 0;
    auto root = new Node;
    root->data = A[p++];
    root->left = root->right = nullptr;
    queue<Node*> q;
    q.emplace(root);
    while (!q.empty()) {
        auto qH = q.front();
        q.pop();
        if (p == A.size()) break;
        if (A[p] != -1) {
            qH->left = new Node;
            qH->left->data = A[p++];
            qH->left->left = qH->left->right = nullptr;
            q.emplace(qH->left);
        } else ++p;
        if (p == A.size()) break;
        if (A[p] != -1) {
            qH->right = new Node;
            qH->right->data = A[p++];
            qH->right->left = qH->right->right = nullptr;
            q.emplace(qH->right);
        } else ++p;
    }
    return root;
}
