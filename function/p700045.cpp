Node* rearrangeEvenOdd(Node* head) {
    Node h0, h1, *p0 = &h0, *p1 = &h1;
    while (head) {
        p0 = p0->next = head;
        head = head->next;
        if (!head) break;
        p1 = p1->next = head;
        head = head->next;
    }
    p1->next = nullptr;
    p0->next = h1.next;
    return h0.next;
}
