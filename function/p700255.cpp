int* topoSort(vector<int> graph[], int N) { // caller use delete[]
    vector<int> in(N);
    for (int i = 0; i < N; ++i)
        for (int j: graph[i]) ++in[j];
    int* ans = new int[N], z0 = 0, z1 = 0;
    for (int i = 0; i < N; ++i)
        if (!in[i]) ans[z1++] = i;
    while (z0 < z1) {
        int no = ans[z0++];
        for (int i: graph[no])
            if (!--in[i]) ans[z1++] = i;
    }
    return ans;
}
