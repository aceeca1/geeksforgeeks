
#include <vector>
using namespace std;

void merge(int arr[], int l, int m, int r) {
    vector<int> b(r - l + 1);
    int p = l, q = m + 1, z = 0;
    while (p <= m || q <= r)
        if (p <= m && (q > r || arr[p] < arr[q])) b[z++] = arr[p++];
        else b[z++] = arr[q++];
    for (int i = 0; i < b.size(); ++i) arr[l + i] = b[i];
}
