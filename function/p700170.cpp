int getNthFromLast(Node* head, int n) {
    auto i = head;
    while (n--) {
        if (!i) return -1;
        i = i->next;
    }
    while (i) {
        head = head->next;
        i = i->next;
    }
    return head->data;
}
