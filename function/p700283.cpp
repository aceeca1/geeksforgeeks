long long findMaxProduct(int A[], int n, int k) {
    long long ans = 0, a = 1;
    for (int i = 0; i < k; ++i) a *= A[i];
    for (int i = k; ; ++i) {
        if (a > ans) ans = a;
        if (i >= n) break;
        a = a / A[i - k] * A[i];
    }
    return ans;
}
