void intersection(node** head1, node** head2, node** head3) {
    node *h1 = *head1, *h2 = *head2, h, *p = &h;
    while (h1 && h2)
        if (h1->val == h2->val) {
            p = p->next = new node;
            p->val = h1->val;
            h1 = h1->next;
            h2 = h2->next;
        } else if (h1->val < h2->val) h1 = h1->next;
        else h2 = h2->next;
    p->next = nullptr;
    *head3 = h.next;
}
