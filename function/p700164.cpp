
#include <algorithm>
using namespace std;

int height(Node* node) {
    if (!node) return 0;
    return max(height(node->left), height(node->right)) + 1;
}
