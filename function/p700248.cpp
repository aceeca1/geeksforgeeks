
#include <queue>
#include <cstdio>
using namespace std;

void levelOrder(Node* node) {
    if (!node) return;
    queue<Node*> q0, q1;
    q0.emplace(node);
    while (!q0.empty()) {
        while (!q0.empty()) {
            auto qH = q0.front();
            q0.pop();
            printf("%d ", qH->data);
            if (qH->left) q1.emplace(qH->left);
            if (qH->right) q1.emplace(qH->right);
        }
        printf("$ ");
        swap(q0, q1);
    }
}


