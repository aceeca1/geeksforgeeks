
#include <algorithm>
using namespace std;

vector<int> v;

void put(Node* p, int d) {
    if (!p) return;
    if (d == v.size()) v.emplace_back();
    ++v[d];
    put(p->left, d + 1);
    put(p->right, d + 1);
}

int getMaxWidth(Node* root) {
    if (!root) return 0;
    put(root, 0);
    int ans = *max_element(v.begin(), v.end());
    v.clear();
    return ans;
}
