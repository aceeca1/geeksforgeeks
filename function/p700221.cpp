int pu, qu;
vector<Node*> pr, pA, qA;

void preOrder(Node* u) {
    if (!u) return;
    pr.emplace_back(u);
    if (u->data == pu) pA = pr;
    if (u->data == qu) qA = pr;
    preOrder(u->left);
    preOrder(u->right);
    pr.pop_back();
}

Node* LCA(Node* root, int n1, int n2) {
    pu = n1;
    qu = n2;
    preOrder(root);
    while (pA.size() > qA.size()) pA.pop_back();
    while (pA.size() < qA.size()) qA.pop_back();
    while (pA.back() != qA.back()) {
        pA.pop_back();
        qA.pop_back();
    }
    auto ans = pA.back();
    pA.clear();
    qA.clear();
    return ans;
}
