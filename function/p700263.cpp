
#include <algorithm>
using namespace std;

void toVec(Node* p, vector<int>& v) {
    if (!p) return;
    toVec(p->left, v);
    v.emplace_back(p->data);
    toVec(p->right, v);
}

void fromVec(Node* p, vector<int>& v) {
    if (!p) return;
    fromVec(p->right, v);
    p->data = v.back();
    v.pop_back();
    fromVec(p->left, v);
}

Node* binaryTreeToBST(Node* root) {
    vector<int> v;
    toVec(root, v);
    sort(v.begin(), v.end());
    fromVec(root, v);
    return root;
}
