
#include <utility>
using namespace std;

void sortList(node* head) {
    node h0, h1, h2, *p0 = &h0, *p1 = &h1, *p2 = &h2;
    for (auto i = head; i; i = i->next) switch (i->data) {
        case 0: p0 = p0->next = i; break;
        case 1: p1 = p1->next = i; break;
        case 2: p2 = p2->next = i;
    }
    p2->next = nullptr;
    p1->next = h2.next;
    p0->next = h1.next;
    switch (head->data) {
        case 1:
            p0->next = h0.next;
            swap(*head, *h0.next);
            break;
        case 2:
            p1->next = h0.next;
            swap(*head, *h0.next);
    }
}
