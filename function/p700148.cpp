void insert(int arr[], int i) {
    int x = arr[i], k = i;
    while (k && arr[k - 1] > x) {
        arr[k] = arr[k - 1];
        --k;
    }
    arr[k] = x;
}
