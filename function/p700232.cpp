void addNode(node **head_ref, int pos, int data) { // caller use delete
    if (pos < 0) return;
    node *p = *head_ref;
    for (int i = 0; i < pos; ++i) p = p->next;
    node *q = new node;
    q->data = data;
    q->prev = p;
    q->next = p->next;
    if (p->next) p->next->prev = q;
    p->next = q;
}
