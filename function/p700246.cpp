int remove_duplicate(int A[], int N) {
    int z = 0;
    for (int i = 0; i < N; ++i)
        if (A[i] != A[i - 1]) A[z++] = A[i];
    return z;
}
