
#include <string>
#include <vector>
using namespace std;

bool is_k_pallin(string s, int k) {
    vector<int> a0(s.size() + 1), a1(s.size() + 1);
    for (int i = s.size() - 1; i >= 0; --i) {
        for (int j = 2; j <= s.size() - i; ++j) {
            if (s[i] == s[i + j - 1]) a0[j] = a1[j - 2];
            else a0[j] = min(a1[j - 1], a0[j - 1]) + 1;
        }
        swap(a0, a1);
    }
    return a1[s.size()] <= k;
}
