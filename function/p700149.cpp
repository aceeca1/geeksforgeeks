int pp;

void put(Node* p) {
    if (!p) return;
    put(p->left);
    if (p->data < pp) throw false;
    pp = p->data;
    put(p->right);
}

bool isBST(Node* root) {
    pp = 0x80000000;
    try { put(root); }
    catch (bool e) { return false; }
    return true;
}
