void deleteNode(Node *node) {
    node->data = node->next->data;
    Node *a = node->next;
    node->next = a->next;
    delete a;
}
