node* merge(node* n1, node* n2) {
    node h, *p = &h;
    while (n1 || n2)
        if (n1 && (!n2 || n1->data < n2->data)) {
            p = p->bottom = n1;
            n1->next = nullptr;
            n1 = n1->bottom;
        } else {
            p = p->bottom = n2;
            n2->next = nullptr;
            n2 = n2->bottom;
        }
    p->bottom = nullptr;
    return h.next;
}

node* flatten(node *root) {
    while (root->next) {
        node h, *p = &h;
        for (auto i = root; i; ) {
            if (!i->next) {
                p = p->next = i;
                break;
            }
            auto n = i->next->next;
            p = p->next = merge(i, i->next);
            i = n;
        }
        root = h.next;
    }
    return root;
}
