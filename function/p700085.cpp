Node* zigZack(Node* head) {
    if (!head) return nullptr;
    int d = 0;
    for (auto i = head; i->next; i = i->next) {
        if (d ^ (i->data > i->next->data)) swap(i->data, i->next->data);
        d = !d;
    }
    return head;
}
