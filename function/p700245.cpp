
#include <unordered_map>
using namespace std;

int maxLen(int A[],int n) {
    if (!A[0]) A[0] = -1;
    for (int i = 1; i < n; ++i)
        A[i] = A[i - 1] + (A[i] ? 1 : -1);
    unordered_map<int, int> c;
    c[0] = -1;
    int ans = 0;
    for (int i = 0; i < n; ++i) {
        auto u = c.emplace(A[i], i);
        if (!u.second) {
            int t = i - u.first->second;
            if (t > ans) ans = t;
        }
    }
    return ans;
}
