void sortedInsert(node** head_ref, int x) {
    if (!*head_ref) {
        *head_ref = new node;
        (*head_ref)->data = x;
        (*head_ref)->next = *head_ref;
        return;
    }
    node *p = *head_ref;
    while (p->next != *head_ref) p = p->next;
    *head_ref = p;
    while (p->next->data < x) {
        p = p->next;
        if (p == *head_ref) break;
    }
    bool isLast = p->next->data < x;
    auto q = new node;
    q->data = x;
    q->next = p->next;
    p->next = q;
    if (isLast) *head_ref = (*head_ref)->next;
    *head_ref = (*head_ref)->next;
}
