// Need Initializer !!

void Stack::push(int x) {
    if (!top) {
        top = new StackNode;
        top->data = x;
        top->next = nullptr;
    } else {
        auto h = new StackNode;
        h->data = x;
        h->next = top;
        top = h;
    }
}

int Stack::pop() {
    if (!top) return -1;
    auto h = top;
    top = top->next;
    auto data = h->data;
    delete h;
    return data;
}
