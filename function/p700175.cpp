Node* deleteMid(Node* head) {
    if (!head) return nullptr;
    if (!head->next) {
        delete head;
        return nullptr;
    }
    int z = 0;
    for (auto i = head; i; i = i->next) ++z;
    z = (z - 2) >> 1;
    auto p = head;
    while (z--) p = p->next;
    auto n = p->next;
    p->next = n->next;
    delete n;
    return head;
}
