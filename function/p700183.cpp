long long unsigned int decimalValue(Node* head) {
    int ans = 0;
    for (auto i = head; i; i = i->next)
        ans = ((ans << 1) + i->data) % 1000000007;
    return ans;
}
