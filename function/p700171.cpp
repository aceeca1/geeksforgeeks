int getMiddle(Node* head) {
    int z = 0;
    for (auto i = head; i; i = i->next) ++z;
    for (z >>= 1; z; --z) head = head->next;
    return head->data;
}
