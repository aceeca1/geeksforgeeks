int _stack::getMin() {
    return s.empty() ? -1 : minEle;
}

int _stack::pop() {
    if (s.empty()) return -1;
    if (s.top() > minEle) {
        int ans = s.top();
        s.pop();
        return ans;
    }
    int ans = minEle;
    minEle += minEle - s.top();
    s.pop();
    return ans;
}

void _stack::push(int x) {
    if (s.empty()) minEle = 0x7fffffff;
    if (x > minEle) {
        s.emplace(x);
        return;
    }
    s.emplace(x - (minEle - x));
    minEle = x;
}
