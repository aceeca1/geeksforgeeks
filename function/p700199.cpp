bool head;

void put(Node* p) {
    if (!p) return;
    put(p->left);
    put(p->right);
    if (!head) putchar(' ');
    head = false;
    printf("%d", p->data);
}

void postOrder(Node* root) {
    head = true;
    put(root);
}
