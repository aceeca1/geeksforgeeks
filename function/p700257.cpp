int minDepth(Node* node) {
    if (!node) return 0;
    if (!node->left && !node->right) return 1;
    int ans = 0x7fffffff;
    if (node->left) {
        int t = minDepth(node->left);
        if (t < ans) ans = t;
    }
    if (node->right) {
        int t = minDepth(node->right);
        if (t < ans) ans = t;
    }
    return ans + 1;
}
