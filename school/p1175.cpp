#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int ans;
        auto mid = a.begin() + (n >> 1);
        if (n & 1) {
            nth_element(a.begin(), mid, a.end());
            ans = a[n >> 1];
        } else {
            nth_element(a.begin(), mid, a.end());
            ans = (a[n >> 1] + *max_element(a.begin(), mid)) >> 1;
        }
        printf("%d\n", ans);
    }
    return 0;
}
