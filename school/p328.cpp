#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, r;
        int64_t ans = 1;
        scanf("%d%d", &n, &r);
        while (r--) ans *= n--;
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
