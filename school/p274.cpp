#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        int64_t ans = 1;
        scanf("%d", &n);
        for (int i = 2; i <= n; ++i) ans *= i;
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
