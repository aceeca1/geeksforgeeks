#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        int64_t ans = 1;
        while (b) {
            if (b & 1) ans *= a;
            b >>= 1;
            a *= a;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
