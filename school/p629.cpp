#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int len = strlen(s);
        for (int i = 0; i < len; ++i) {
            for (int j = len - 1 - i; j; --j)
                putchar('.');
            char k = s[i + 1];
            s[i + 1] = 0;
            printf("%s\n", s);
            s[i + 1] = k;
        }
    }
    return 0;
}
