#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        n >>= 1;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < i; ++j) putchar(' ');
            putchar('*');
            for (int j = 0; j < n + n - 1 - i - i; ++j) putchar(' ');
            putchar('*');
            for (int j = 0; j < i; ++j) putchar(' ');
        }
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) putchar(' ');
            putchar('*');
            for (int j = 0; j < n; ++j) putchar(' ');
        }
        printf("\n");
    }
    return 0;
}
