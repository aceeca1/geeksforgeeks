#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a, r;
        scanf("%d%d%d", &n, &a, &r);
        printf("%.6f\n", r == 1 ? a * n : a * (1.0 - pow(r, n)) / (1.0 - r));
    }
    return 0;
}
