#include <cstdio>
#include <cstdlib>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int base;
        char s[44];
        scanf("%d%s", &base, s);
        printf("%ld\n", strtol(s, nullptr, base));
    }
    return 0;
}
