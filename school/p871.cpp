#include <cstdio>
using namespace std;

int main() {
    char dial[256];
    for (int i = 0; i < 26; ++i)
        dial['A' + i] = dial['a' + i] = "22233344455566677778889999"[i];
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        for (int i = 0; s[i]; ++i) putchar(dial[s[i]]);
        printf("\n");
    }
    return 0;
}
