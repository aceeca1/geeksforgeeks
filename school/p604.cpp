#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    vector<int> n(t);
    int m = 0;
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    for (int ni: n) {
        bool head = true;
        for (int i = 2; i <= ni >> 1; i = p[i])
            for (int j = 2; j * i <= ni; j = p[j]) {
                if (!head) putchar(' ');
                head = false;
                printf("%d %d", i, j);
            }
        printf("\n");
    }
    return 0;
}
