#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s("1");
        for (int i = 1; i <= n; ++i) {
            if (i > 1) putchar(' ');
            printf("%s", s.c_str());
            int k = s.size() - 1;
            while (k >= 0 && s[k] == '1') --k;
            if (k < 0) {
                for (k = 1; k < s.size(); ++k) s[k] = '0';
                s += '0';
            } else {
                s[k] = '1';
                for (++k; k < s.size(); ++k) s[k] = '0';
            }
        }
        printf("\n");
    }
    return 0;
}
