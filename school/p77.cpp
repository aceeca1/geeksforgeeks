#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        scanf("%d", &x);
        int p = find(a.begin(), a.end(), x) - a.begin();
        printf("%d\n", p == a.size() ? -1 : p);
    }
    return 0;
}
