#include <cstdio>
#include <cctype>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        for (int i = 0; s[i]; ++i) if (!isalpha(s[i])) putchar(s[i]);
        printf("\n");
    }
    return 0;
}
