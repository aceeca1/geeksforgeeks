#include <cstdio>
using namespace std;

void put(int s, int t) {
    if (s > t) return;
    if (s > 1) putchar(' ');
    printf("%d", s);
    put(s + 1, t);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        put(1, n);
        printf("\n");
    }
    return 0;
}
