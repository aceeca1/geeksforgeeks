#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("1");
        int k = 2;
        for (; k * k <= n; ++k)
            if (!(n % k)) printf(" %d", k);
        while (k * k >= n) --k;
        for (; k >= 1; --k)
            if (!(n % k)) printf(" %d", n / k);
        printf("\n");
    }
    return 0;
}
