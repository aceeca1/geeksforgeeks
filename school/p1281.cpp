#include <cstdio>
using namespace std;

constexpr int fac[10] = {
    1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int n;
        sscanf(s, "%d", &n);
        for (int i = 0; s[i]; ++i) n -= fac[s[i] - '0'];
        printf("%s\n", n ? "Not Perfect" : "Perfect");
    }
    return 0;
}
