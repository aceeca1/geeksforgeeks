#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        for (int i = strlen(s) - 1; i >= 0; --i) {
            printf("%s\n", s);
            s[i] = 0;
        }
    }
    return 0;
}
