#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a1 = 0, a2 = 0;
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a > a1) { a2 = a1; a1 = a; }
            else if (a > a2) a2 = a;
        }
        printf("%d\n", a2);
    }
    return 0;
}
