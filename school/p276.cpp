#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%*d");
        char s[440];
        scanf("%[^\n]", s);
        printf("%s\n", s);
    }
    return 0;
}
