#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x, ans1 = 0, ans2 = 0;
        scanf("%d%d", &n, &x);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a <= x) ++ans1;
            if (a >= x) ++ans2;
        }
        printf("%d %d\n", ans1, ans2);
    }
    return 0;
}
