#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    double s5 = sqrt(5.0), a = 0.5 * (1 + s5);
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        long m = lround(pow(a, round(log(n * s5) / log(a))) / s5);
        printf("%s\n", n == m ? "Yes" : "No");
    }
    return 0;
}
