#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int p, tu, r;
        scanf("%d%d%d", &p, &tu, &r);
        printf("%d\n", p * tu * r / 100);
    }
    return 0;
}
