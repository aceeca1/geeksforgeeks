#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, c;
        scanf("%d%d%d", &a, &b, &c);
        printf("%d\n", a > b ?
            a > c ? (b > c ? b : c) : a :
            a > c ? a : (b > c ? c : b));
    }
    return 0;
}
