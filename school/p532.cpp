#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0 = 0, a1 = 0;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a) ++a1; else ++a0;
        }
        bool head = true;
        for (int i = 0; i < a0; ++i) {
            if (!head) putchar(' ');
            head = false;
            putchar('0');
        }
        for (int i = 0; i < a1; ++i) {
            if (!head) putchar(' ');
            head = false;
            putchar('1');
        }
        printf("\n");
    }
    return 0;
}
