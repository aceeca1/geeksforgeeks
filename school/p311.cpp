#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a, d;
        scanf("%d%d%d", &n, &a, &d);
        printf("%d\n", ((a << 1) + (n - 1) * d) * n >> 1);
    }
    return 0;
}
