#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, c;
        scanf("%d%d%d", &a, &b, &c);
        int delta = b * b - 4 * a * c;
        if (delta < 0) {
            printf("Imaginary\n");
            continue;
        }
        double sD = sqrt(delta);
        int x1 = floor((-b + sD) / (a + a));
        int x2 = floor((-b - sD) / (a + a));
        printf("%d %d\n", x1, x2);
    }
    return 0;
}
