#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int p, t, n, r;
        scanf("%d%d%d%d", &p, &t, &n, &r);
        printf("%d\n", int(p * pow(1 + r * 0.01 / n, n * t)));
    }
    return 0;
}
