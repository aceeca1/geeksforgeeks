#include <cstdio>
#include <ctime>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        tm a{};
        scanf("%d%d%d", &a.tm_mday, &a.tm_mon, &a.tm_year);
        a.tm_year -= 1900;
        --a.tm_mon;
        time_t aT = mktime(&a);
        scanf("%d%d%d", &a.tm_mday, &a.tm_mon, &a.tm_year);
        a.tm_year -= 1900;
        --a.tm_mon;
        time_t bT = mktime(&a);
        printf("%.0f\n", abs(difftime(bT, aT) / 86400.0));
    }
    return 0;
}
