#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[22];
        scanf("%s", s);
        string s1(s);
        reverse(s1.begin(), s1.end());
        printf("%s\n", s == s1 ? "Yes" : "No");
    }
    return 0;
}
