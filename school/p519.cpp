#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int r = lround(sqrt(n));
        printf("%d\n", r * r == n);
    }
    return 0;
}
