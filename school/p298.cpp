#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int l, b, h;
        scanf("%d%d%d", &l, &b, &h);
        printf("%d %d\n", (l * b + b * h + h * l) << 1, l * b * h);
    }
    return 0;
}
