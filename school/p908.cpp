#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), b(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        for (int i = 0; i < n; ++i) scanf("%d", &b[i]);
        sort(b.begin(), b.end());
        int ans = 0;
        for (int i = 0; i < n; ++i) ans += a[i] * b[i];
        printf("%d\n", ans);
    }
    return 0;
}
