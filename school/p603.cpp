#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, maxS = -1;
        scanf("%d", &n);
        string smaxS;
        while (n--) {
            char s[22];
            int a0, a1, a2;
            scanf("%s%d%d%d", s, &a0, &a1, &a2);
            a0 += a1 + a2;
            if (a0 > maxS) {
                maxS = a0;
                smaxS = s;
            }
        }
        printf("%s %d\n", smaxS.c_str(), maxS / 3);
    }
    return 0;
}
