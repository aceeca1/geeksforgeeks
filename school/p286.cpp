#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d", n);
        for (int i = 2; i <= 10; ++i)
            printf(" %d", n * i);
        printf("\n");
    }
    return 0;
}
