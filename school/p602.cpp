#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        for (int i = 13; i >= 0; --i)
            printf("%d", (n >> i) & 1);
        printf("\n");
    }
    return 0;
}
