#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, p;
        scanf("%d%d", &n, &p);
        unordered_set<int> u;
        int i = 0;
        for (; i < n; ++i) {
            int a;
            scanf("%d", &a);
            int b = p / a;
            if (b * a == p && u.count(b)) break;
            u.emplace(a);
        }
        printf("%s\n", i < n ? "Yes" : "No");
        for (++i; i < n; ++i) scanf("%*d");
    }
    return 0;
}
