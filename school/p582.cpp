#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, sum = 0;
        scanf("%d", &n);
        for (int i = 0; i < n; ++i) {
            int a;
            scanf("%d", &a);
            sum += a;
            if (i) putchar(' ');
            printf("%d", sum / (i + 1));
        }
        printf("\n");
    }
    return 0;
}
