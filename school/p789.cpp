#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int ax, ay, bx, by, cx, cy, dx, dy;
        scanf("%d%d%d%d%d%d%d%d", &ax, &ay, &bx, &by, &cx, &cy, &dx, &dy);
        if (bx <= ax || by >= ay || dx <= cx || dy >= cy) {
            printf("0\n");
            continue;
        }
        printf("%d\n", ax < dx && cx < bx && by < cy && dy < ay);
    }
    return 0;
}
