#include <cstdio>
using namespace std;

int x1, y1, z1, x2, y2, z2;

int f() {
    if (!(x1 * x2 + y1 * y2 + z1 * z2)) return 2;
    auto k = double(x1) / x2;
    return k == double(y1) / y2 && k == double(z1) / z2;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d%d%d%d%d", &x1, &y1, &z1, &x2, &y2, &z2);
        printf("%d\n", f());
    }
    return 0;
}
