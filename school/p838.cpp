#include <cstdio>
#include <cmath>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n;
        scanf("%" SCNd64, &n);
        printf("%d\n", int(sqrt(n)));
    }
    return 0;
}
