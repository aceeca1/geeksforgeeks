#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        putchar('*');
        for (int i = 2; i <= n; ++i) {
            putchar(' ');
            for (int j = 0; j < i; ++j) putchar('*');
        }
        printf("\n");
    }
    return 0;
}
