#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, y;
        scanf("%d%d", &x, &y);
        double d = log(y) / log(x);
        printf("%d\n", abs(d - round(d)) < 1e-9);
    }
    return 0;
}
