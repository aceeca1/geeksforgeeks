#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < i; ++j) putchar(' ');
            for (int j = 0; j < n + n - 1 - i - i; ++j) putchar('*');
            for (int j = 0; j < i; ++j) putchar(' ');
        }
        printf("\n");
    }
    return 0;
}
