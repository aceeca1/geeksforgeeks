#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, c, d, e;
        scanf("%d%d%d%d%d", &a, &b, &c, &d, &e);
        printf("%d %d %d\n", a * b, c * d >> 1, e * e * 314 / 100);
    }
    return 0;
}
