#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n % 3) printf("0\n");
        else {
            n /= 3;
            int ans = 0, i = 1;
            for (; i * i < n; ++i) ans += !(n % i);
            printf("%d\n", ans + ans + (i * i == n));
        }
    }
    return 0;
}
