#include <cstdio>
#include <cctype>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[55];
        scanf("%s", s);
        for (char& i: s) i = toupper(i);
        printf("%s\n", s);
    }
    return 0;
}
