#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        n ^= n >> 16;
        n ^= n >> 8;
        n ^= n >> 4;
        n &= 0xf;
        printf("%s\n", (0x6996 >> n) & 1 ? "odd" : "even");
    }
    return 0;
}
