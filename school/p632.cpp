#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        for (int i = 0; s[i]; ++i) {
            printf("%s\n", s);
            s[i] = '.';
        }
    }
    return 0;
}
