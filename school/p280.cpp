#include <cstdio>
using namespace std;

int main() {
    bool vowel[256]{};
    for (char i: "AEIOUaeiou") vowel[i] = true;
    int t;
    scanf("%d", &t);
    while (t--) {
        char c;
        scanf(" %c", &c);
        printf("%s\n", vowel[c] ? "YES" : "NO");
    }
    return 0;
}
