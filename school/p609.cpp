#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1;
        scanf("%d%d", &n, &a1);
        bool b = true;
        while (--n) {
            scanf("%d", &a0);
            if (a0 < a1) b = false;
            a1 = a0;
        }
        printf("%d\n", b);
    }
    return 0;
}
