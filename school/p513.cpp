#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int base, n;
        scanf("%d%d", &base, &n);
        string ans;
        if (!n) ans += '0';
        while (n) {
            int a = n % base;
            ans += a > 9 ? 'A' + (a - 10) : '0' + a;
            n /= base;
        }
        reverse(ans.begin(), ans.end());
        printf("%s\n", ans.c_str());
    }
    return 0;
}
