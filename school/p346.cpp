#include <cstdio>
#include <cctype>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        int ans = 0;
        for (int i = 0; s[i]; ++i) ans += bool(isalpha(s[i]));
        printf("%d\n", ans);
    }
    return 0;
}
