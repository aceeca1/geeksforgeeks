#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int c;
        scanf("%d", &c);
        printf("%d\n", c * 9 / 5 + 32);
    }
    return 0;
}
