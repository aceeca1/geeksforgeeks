#include <cstdio>
using namespace std;

int sqr(int x) {
    return x * x;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int ax, ay, bx, by, cx, cy, dx, dy;
        scanf("%d%d%d%d%d%d%d%d", &ax, &ay, &bx, &by, &cx, &cy, &dx, &dy);
        int ab2 = sqr(bx - ax) + sqr(by - ay);
        int bc2 = sqr(cx - bx) + sqr(cy - by);
        int ca2 = sqr(ax - cx) + sqr(ay - cy);
        if (ab2 == bc2 + ca2) {
            printf("%d\n", dx == ax + bx - cx && dy == ay + by - cy);
            continue;
        }
        if (bc2 == ca2 + ab2) {
            printf("%d\n", dx == bx + cx - ax && dy == by + cy - ay);
            continue;
        }
        if (ca2 == ab2 + bc2) {
            printf("%d\n", dx == cx + ax - bx && dy == cy + ay - by);
            continue;
        }
        printf("0\n");
    }
    return 0;
}
