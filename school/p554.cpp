#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[22];
        scanf("%s", s);
        reverse(s, s + strlen(s));
        int k = 0;
        while (s[k] == '0') ++k;
        if (!s[k]) --k;
        printf("%s\n", s + k);
    }
    return 0;
}
