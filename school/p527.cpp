#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        putchar('1');
        for (int i = 2; i <= n; ++i) {
            putchar(' ');
            for (int j = 1; j <= i; ++j) printf("%d", j);
            for (int j = i - 1; j >= 1; --j) printf("%d", j);
        }
        printf("\n");
    }
    return 0;
}
