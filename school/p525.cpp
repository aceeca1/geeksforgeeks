#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1;
        scanf("%d%d", &n, &a1);
        while (--n) {
            scanf("%d", &a0);
            printf("%d ", a0 < a1 ? a0 : -1);
            a1 = a0;
        }
        printf("-1\n");
    }
    return 0;
}
