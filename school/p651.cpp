#include <cstdio>
#include <vector>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int64_t> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    for (int i = 2; i <= m; ++i)
        p[i] = p[i] < i ? p[i - 1] : p[i - 1] + i;
    for (int ti = 0; ti < t; ++ti)
        printf("%" PRId64 "\n", p[n[ti]]);
    return 0;
}
