#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        sort(s.begin(), s.end());
        printf("%s\n", s.c_str());
    }
    return 0;
}
