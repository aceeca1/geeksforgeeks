#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, l, r, y;
        scanf("%d%d%d%d", &x, &l, &r, &y);
        int m = r - l == 31 ? -1 : ((1 << (r - l + 1)) - 1) << (l - 1);
        printf("%d\n", x | (y & m));
    }
    return 0;
}
