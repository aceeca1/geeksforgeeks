#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2, n3;
        scanf("%d%d%d", &n1, &n2, &n3);
        vector<int> a1(n1), a2(n2);
        for (int i = 0; i < n1; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n2; ++i) scanf("%d", &a2[i]);
        auto e1 = set_intersection(
            a1.begin(), a1.end(), a2.begin(), a2.end(), a1.begin());
        a2.resize(n3);
        for (int i = 0; i < n3; ++i) scanf("%d", &a2[i]);
        auto z = set_intersection(
            a1.begin(), e1, a2.begin(), a2.end(), a1.begin()) - a1.begin();
        bool head = true;
        for (int i = 0; i < z; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a1[i]);
        }
        printf("\n");
    }
    return 0;
}
