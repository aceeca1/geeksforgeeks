#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        int n = 0;
        sscanf(s, "%d%n", &n, &n);
        if (!s[n]) { printf("int\n"); continue; }
        double d;
        sscanf(s, "%lf%n", &d, &n);
        if (!s[n]) printf("float\n");
        else if (s[1] == 0) printf("char\n");
        else printf("string\n");
    }
    return 0;
}
