#include <cstdio>
#include <vector>
using namespace std;

void next(int& k) { k = k & 1 ? k * 3 + 1 : k >> 1; }

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> a(m + 1);
    a[1] = 1;
    for (int i = 2; i <= m; ++i) {
        if (a[i]) continue;
        int len = 1;
        for (int j = i; j != 1; next(j)) {
            if (j <= m && a[j]) {
                len += a[j] - 1;
                break;
            }
            ++len;
        }
        for (int j = i; j != 1; next(j), --len) {
            if (j > m) continue;
            if (a[j]) break;
            a[j] = len;
        }
    }
    for (int i = 1; i <= m; ++i)
        if (a[i - 1] > a[i]) a[i] = a[i - 1];
    for (int ni: n) printf("%d\n", a[ni]);
    return 0;
}
