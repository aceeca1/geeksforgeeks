#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, max1 = -1, ans;
        scanf("%d%d", &n, &m);
        for (int i = 0; i < n; ++i) {
            int num1 = 0;
            for (int j = 0; j < m; ++j) {
                int a;
                scanf("%d", &a);
                num1 += a;
            }
            if (num1 > max1) {
                max1 = num1;
                ans = i;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
