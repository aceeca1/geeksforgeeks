#include <cstdio>
#include <cmath>
using namespace std;

long double area(long double r1, long double r2, long double d) {
    long double a = (r1 * r1 + d * d - r2 * r2) / (r1 * d * 2.0L);
    if (a < -1.0L) a = -1.0L;
    if (a >  1.0L) a = 1.0L;
    a = acos(a) * 2.0L;
    return r1 * r1 * 0.5L * (a - sin(a));
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        long double x1, y1, r1, x2, y2, r2;
        scanf("%Lf%Lf%Lf%Lf%Lf%Lf", &x1, &y1, &r1, &x2, &y2, &r2);
        long double d = hypot(x2 - x1, y2 - y1);
        if (d > r1 + r2) { printf("0\n"); continue; }
        if (r1 > d + r2) {
            printf("%.0Lf\n", floor(r2 * r2 * M_PIl));
            continue;
        }
        if (r2 > d + r1) {
            printf("%.0Lf\n", floor(r1 * r1 * M_PIl));
            continue;
        }
        printf("%.0Lf\n", floor(area(r1, r2, d) + area(r2, r1, d)));
    }
    return 0;
}
