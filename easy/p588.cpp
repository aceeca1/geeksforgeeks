#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, p1, p2, n;
        scanf("%d%d%d%d", &x, &p1, &p2, &n);
        int s1 = (x >> p1) & ((1 << n) - 1);
        int s2 = (x >> p2) & ((1 << n) - 1);
        x = (x & ~(s1 << p1)) + (s2 << p1);
        x = (x & ~(s2 << p2)) + (s1 << p2);
        printf("%d\n", x);
    }
    return 0;
}
