#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        vector<int> a1(n1), a2(n2), a((n1 > n2 ? n1 : n2) + 1);
        for (int i = n1 - 1; i >= 0; --i) scanf("%d", &a1[i]);
        for (int i = n2 - 1; i >= 0; --i) scanf("%d", &a2[i]);
        int carry = 0;
        for (int i = 0; i < a.size(); ++i) {
            if (i < n1) carry += a1[i];
            if (i < n2) carry += a2[i];
            a[i] = carry % 10;
            carry /= 10;
        }
        if (a.back() == 0) a.pop_back();
        bool head = true;
        for (int i = a.size() - 1; i >= 0; --i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
