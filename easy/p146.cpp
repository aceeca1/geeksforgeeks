#include <cstdio>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        auto p = upper_bound(a.begin(), a.end(), a[0], greater<int>());
        scanf("%d", &x);
        auto p1 = lower_bound(a.begin(), p, x);
        auto p2 = a.end();
        if (p1 == a.begin()) p2 = lower_bound(p, a.end(), x);
        if (p1 != p && *p1 == x)
            printf("%d\n", int(p1 - a.begin()));
        else if (p2 != a.end() && *p2 == x)
            printf("%d\n", int(p2 - a.begin()));
        else
            printf("-1\n");
    }
    return 0;
}
