#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> c(n), a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &c[i]);
        for (int i = 1; i <= 2; ++i) {
            int p = 0x80000000, a0 = i == 1 ? 0 : 0xc0000000, a1;
            for (int j = 0; j < n; ++j) {
                a1 = a0;
                a0 = a[j];
                if (j) a[j] = a[j - 1];
                else a[j] = 0xc0000000;
                int k = p + c[j];
                if (k > a[j]) a[j] = k;
                k = a1 - c[j];
                if (k > p) p = k;
            }
        }
        printf("%d\n", a.back());
    }
    return 0;
}
