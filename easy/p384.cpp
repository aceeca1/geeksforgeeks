#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        auto n = next_permutation(s, s + strlen(s));
        printf("%s\n", n ? s : "not possible");
    }
    return 0;
}
