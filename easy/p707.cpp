#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 1;
        scanf("%d", &n);
        for (int i = 1; i <= n; ++i)
            ans = int64_t(ans) * i % 1000000007;
        printf("%d\n", ans);
    }
    return 0;
}
