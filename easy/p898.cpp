// The product of consecutive integers is never a power. (Erdös, 1975)
// See https://projecteuclid.org/download/pdf_1/euclid.ijm/1256050816
#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        long double l, r;
        scanf("%Lf%Lf", &l, &r);
        printf("%.0Lf\n", floor(sqrt(r)) - floor(sqrt(l)));
    }
    return 0;
}
