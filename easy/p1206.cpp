#include <cstdio>
using namespace std;

constexpr const char* ans[]{
    "2", "2", "4", "8", "16", "512", "256", "134217728",
    "65536", "2417851639229258349412352", "4294967296",
    "14134776518227074636666380005943348126619871175004951664972849610340958208"};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%s\n", ans[n - 1]);
    }
    return 0;
}
