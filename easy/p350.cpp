#include <cstdio>
#include <cctype>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        s[0] = tolower(s[0]);
        int p = 0, q = 0;
        bool head = true;
        for (;;) {
            while (s[q] && islower(s[q])) ++q;
            if (!s[q]) break;
            char c = tolower(s[q]);
            s[q] = 0;
            if (!head) putchar(' ');
            head = false;
            printf("%s", s + p);
            s[q] = c;
            p = q;
        }
        if (!head) putchar(' ');
        printf("%s\n", s + p);
    }
    return 0;
}
