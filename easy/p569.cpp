#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[220], s2[220];
        scanf("%s%s", s1, s2);
        int z1 = strlen(s1), z2 = strlen(s2);
        int z = (z1 > z2 ? z1 : z2) + 1;
        int carry = 0;
        for (int i = 0; i < z; ++i) {
            if (i < z1) carry += s1[z1 - 1 - i] - '0';
            if (i < z2) carry += s2[z2 - 1 - i] - '0';
            s1[z - 1 - i] = '0' + (carry & 1);
            carry >>= 1;
        }
        s1[z] = 0;
        int k = 0;
        if (s1[k] =='0') ++k;
        printf("%s\n", s1[k] ? s1 + k : "0");
    }
    return 0;
}
