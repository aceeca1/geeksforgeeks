#include <cstdio>
#include <vector>
#include <stack>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        stack<int> s;
        for (int i = n - 1; i >= 0; --i) {
            while (!s.empty() && s.top() < a[i]) s.pop();
            int ai = a[i];
            a[i] = s.empty() ? -1 : s.top();
            s.emplace(ai);
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
