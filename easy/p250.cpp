#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        for (int i = 0; s[i]; ++i) s[i] ^= 1;
        int i = 0;
        while (s[i]) ++i;
        --i;
        while (i >= 0 && s[i] == '1') s[i--] = '0';
        if (i >= 0) s[i] = '1';
        else putchar('1');
        printf("%s\n", s);
    }
    return 0;
}
