#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1 = 1, a2 = 1;
        scanf("%d", &n);
        while (--n) {
            a0 = (a1 + a2) % 10;
            a2 = a1;
            a1 = a0;
        }
        printf("%d\n", a2);
    }
    return 0;
}
