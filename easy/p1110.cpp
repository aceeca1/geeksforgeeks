#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, x;
        scanf("%d%d%d", &a, &b, &x);
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        x -= a + b;
        printf("%d\n", x >= 0 && (~x & 1));
    }
    return 0;
}
