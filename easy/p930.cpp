#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        for (;;) {
            int max = 0;
            for (int i = 0; i < n; ++i)
                if (a[i] > max) max = a[i];
            if (!max) break;
            for (int i = 0; i < n; ++i) {
                int k = a[i] ^ max;
                if (k < a[i]) a[i] = k;
            }
            int k = ans ^ max;
            if (k > ans) ans = k;
        }
        printf("%d\n", ans);
    }
    return 0;
}
