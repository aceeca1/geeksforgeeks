#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[110], s2[110];
        scanf("%s%s", s1, s2);
        if (!strstr(s1, s2)) printf("not ");
        printf("found\n");
    }
    return 0;
}
