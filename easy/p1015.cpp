#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    for (int ni: n) {
        int64_t ans = 1;
        for (int i = 2; i <= ni; i = p[i]) {
            int u = 0;
            for (int j = ni / i; j; j /= i) u += j;
            ans *= u + 1;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
