#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b, ans = 1;
        scanf("%d%d", &a, &b);
        for (; b; b >>= 1) {
            if (b & 1) ans = int64_t(ans) * a % 1000000007;
            a = int64_t(a) * a % 1000000007;
        }
        printf("%d\n", ans);
    }
    return 0;
}
