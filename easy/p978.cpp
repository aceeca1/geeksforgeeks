#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n + 1);
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            if (b > n) b = n;
            ++a[b];
        }
        int cu = a[n];
        while (cu < n) cu += a[--n];
        printf("%d\n", n);
    }
    return 0;
}
