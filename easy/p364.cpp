#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int v, n;
        scanf("%d%d", &v, &n);
        vector<int> a(v + 1, 0x7ffffffe);
        a[0] = 0;
        while (n--) {
            int b;
            scanf("%d", &b);
            for (int i = b; i <= v; ++i) {
                int t = a[i - b] + 1;
                if (t < a[i]) a[i] = t;
            }
        }
        printf("%d\n", a[v] == 0x7ffffffe ? -1 : a[v]);
    }
    return 0;
}
