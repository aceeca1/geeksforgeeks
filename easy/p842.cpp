#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> a(m + 1);
    a[1] = 1;
    for (int i = 2; i <= m; ++i)
        a[i] = (a[i - 1] + a[i - 2] + 1) % 1000000007;
    for (int ni: n) printf("%d\n", a[ni]);
    return 0;
}
