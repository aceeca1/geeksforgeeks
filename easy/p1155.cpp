#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        double k = floor(log(n) / log(m));
        double a1 = pow(m, k);
        double a2 = pow(m, k + 1);
        if (n - a1 < a2 - n) a2 = a1;
        printf("%.0f\n", a2);
    }
    return 0;
}
