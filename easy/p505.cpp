#include <cstdio>
#include <vector>
using namespace std;

bool head;

void put(const int* s, const int* t) {
    if (s == t) return;
    int m = (t - s - 1) >> 1;
    if (!head) putchar(' ');
    head = false;
    printf("%d", s[m]);
    put(s, s + m);
    put(s + m + 1, t);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        head = true;
        put(&a[0], &a[n]);
        printf("\n");
    }
    return 0;
}
