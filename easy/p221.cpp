#include <cstdio>
#include <initializer_list>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        bool head = true;
        for (int i: {1000, 500, 100, 50, 20, 10, 5, 2, 1}) {
            while (n >= i) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", i);
                n -= i;
            }
        }
        printf("\n");
    }
    return 0;
}
