#include <cstdio>
#include <vector>
using namespace std;

vector<int> a;
int n, s, p, q;

bool has() {
    p = q = 0;
    while (q < n) {
        while (q < n && s > 0) s -= a[q++];
        if (s == 0) return true;
        while (s < 0) s += a[p++];
        if (s == 0) return true;
    }
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d", &n, &s);
        a = vector<int>(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        if (has()) printf("%d %d\n", p + 1, q);
        else printf("-1\n");
    }
    return 0;
}
