#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        if (a % 3) {
            a %= 9;
            b %= 6;
            printf("%d\n", int(pow(a, b)) % 9);
        } else if (b >= 2) printf("9\n");
        else printf("%d\n", a % 9);
    }
    return 0;
}
