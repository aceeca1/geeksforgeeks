#include <cstdio>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        int a[256]{};
        for (int i = 0; s[i]; ++i) ++a[s[i]];
        scanf("%s", s);
        for (int i = 0; s[i]; ++i) --a[s[i]];
        bool b = all_of(a, a + 256, [&](int ai) { return !ai; });
        printf("%s\n", b ? "YES" : "NO");
    }
    return 0;
}
