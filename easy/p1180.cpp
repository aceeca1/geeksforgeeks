#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string sMin, sMax;
        while (n--) {
            char s[44];
            scanf("%s", s);
            if (sMin.empty() || s < sMin) sMin = s;
            if (sMax.empty() || s > sMax) sMax = s;
        }
        printf("%s %s\n", sMin.c_str(), sMax.c_str());
    }
    return 0;
}
