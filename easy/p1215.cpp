// See http://www.cut-the-knot.org/arithmetic/algebra/FibonacciGCD.shtml
#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, a0, a1 = 1, a2 = 1;
        scanf("%d%d", &n, &m);
        while (n) swap(m %= n, n);
        while (--m) {
            a0 = (a1 + a2) % 100;
            a2 = a1;
            a1 = a0;
        }
        printf("%d\n", a2);
    }
    return 0;
}
