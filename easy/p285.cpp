#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        int n = 0;
        for (int i = 0; s[i]; ++i) n += s[i] == '1';
        printf("%d\n", n * (n - 1) >> 1);
    }
    return 0;
}
