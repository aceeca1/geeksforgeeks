#include <cstdio>
#include <stack>
using namespace std;

char s[110];

bool bal() {
    stack<char> st;
    for (int i = 0; s[i]; ++i) switch (s[i]) {
        case ')':
            if (st.empty() || st.top() != '(') return false;
            st.pop(); break;
        case ']':
            if (st.empty() || st.top() != '[') return false;
            st.pop(); break;
        case '}':
            if (st.empty() || st.top() != '{') return false;
            st.pop(); break;
        default: st.emplace(s[i]);
    }
    return st.empty();
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        if (!bal()) printf("not ");
        printf("balanced\n");
    }
    return 0;
}
