#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        int p = a, q = b;
        while (q) swap(p %= q, q);
        printf("%" PRId64 " %d\n", int64_t(a / p) * (b / p), p);
    }
    return 0;
}
