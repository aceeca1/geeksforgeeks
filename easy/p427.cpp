// Almost the same as p907
#include <cstdio>
using namespace std;

int sqr(int x) { return x * x; }

int ax, ay, bx, by, cx, cy, dx, dy;

bool is() {
    int ab2 = sqr(bx - ax) + sqr(by - ay);
    int bc2 = sqr(cx - bx) + sqr(cy - by);
    int ca2 = sqr(ax - cx) + sqr(ay - cy);
    if (ab2 == bc2 + ca2) return dx == ax + bx - cx && dy == ay + by - cy;
    if (bc2 == ca2 + ab2) return dx == bx + cx - ax && dy == by + cy - ay;
    if (ca2 == ab2 + bc2) return dx == cx + ax - bx && dy == cy + ay - by;
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d%d%d%d%d%d%d", &ax, &ay, &bx, &by, &cx, &cy, &dx, &dy);
        printf("%s\n", is() ? "Yes" : "No");
    }
    return 0;
}
