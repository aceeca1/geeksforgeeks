#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        for (int i = 0; i < n; ++i) a[a[i] % k] += k;
        int max = 0, ans;
        for (int i = 0; i < n; ++i) {
            int b = a[i] / k;
            if (b > max) { max = b; ans = i; }
        }
        printf("%d\n", ans);
    }
    return 0;
}
