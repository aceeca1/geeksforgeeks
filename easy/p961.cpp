#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];  // workaround
        scanf("%s", s);
        if (strlen(s) & 1) {
            printf("-1\n");
            continue;
        }
        int b = 0, ans = 0;
        for (int i = 0; s[i]; ++i) if (s[i] == '{') ++b;
        else if (--b < 0) { b += 2; ++ans; }
        printf("%d\n", ans + (b >> 1));
    }
    return 0;
}
