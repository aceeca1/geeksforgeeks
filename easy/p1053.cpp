#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int m, s, n;
        scanf("%d%d%d", &m, &s, &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        vector<int> b = a;
        sort(b.begin(), b.end());
        b.resize(unique(b.begin(), b.end()) - b.begin());
        for (int i = 0; i < n; ++i)
            a[i] = lower_bound(b.begin(), b.end(), a[i]) - b.begin();
        int mu = 1 << (ilogb(b.size() + 2) + 1);
        b.clear();
        b.resize(mu + mu);
        int ans = 0;
        for (int i = n - 1; i >= 0; --i)
            for (int k = a[i] + mu; k > 1; k >>= 1) {
                if (k & 1) ans += b[k - 1];
                ++b[k];
            }
        printf("%d\n", ans * s <= m * 60);
    }
    return 0;
}
