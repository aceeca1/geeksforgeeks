#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a0, a1 = 0, p0, p1 = 0, p2 = 0;
        while (n--) {
            scanf("%d", &a0);
            p0 = a0 + p1;
            a1 += p2;
            if (a1 < p0) p0 = a1;
            p2 = p1;
            p1 = p0;
            a1 = a0;
        }
        printf("%d\n", p1);
    }
    return 0;
}
