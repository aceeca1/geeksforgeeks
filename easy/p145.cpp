#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1, i = 1;
        scanf("%d%d", &n, &a1);
        for (; i < n; ++i) {
            scanf("%d", &a0);
            if (a0 < a1) break;
            a1 = a0;
        }
        printf("%d\n", i % n);
        for (++i; i < n; ++i) scanf("%*d");
    }
    return 0;
}
