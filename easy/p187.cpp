#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        while (n) {
            n /= 5;
            ans += n;
        }
        printf("%d\n", ans);
    }
    return 0;
}
