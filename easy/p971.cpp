#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[1100000], s2[110000];
        scanf("%s%s", s1, s2);
        int z1 = strlen(s1), z2 = strlen(s2);
        reverse(s1, s1 + z1);
        reverse(s2, s2 + z2);
        auto p = strstr(s1, s2);
        printf("%d\n", p ? int(1 + z1 - 1 - (p - s1) - (z2 - 1)) : -1);
    }
    return 0;
}
