#include <cstdio>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        unordered_map<int, int> a;
        int ans = 0x7fffffff;
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            auto t = a.emplace(b, i);
            if (!t.second) {
                int k = t.first->second;
                if (k < ans) ans = k;
            }
        }
        printf("%d\n", ans == 0x7fffffff ? -1 : ans + 1);
    }
    return 0;
}
