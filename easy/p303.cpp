#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        int64_t b;
        scanf("%d%" SCNd64, &n, &b);
        while (n--) {
            int64_t a;
            scanf("%" SCNd64, &a);
            if (a == b) b += a;
        }
        printf("%" PRId64 "\n", b);
    }
    return 0;
}
