#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a5 = 0, ans = 1;
        while (n--) {
            int64_t b;
            scanf("%" SCNd64, &b);
            if (b) {
                while (!(b % 5)) { b /= 5; ++a5; }
                while (!(b & 1)) { b >>= 1; --a5; }
            }
            ans = ans * b % 10;
        }
        if (a5 > 0) while (a5--) ans = (ans * 5) % 10;
        else while (a5++) ans = (ans << 1) % 10;
        printf("%d\n", ans ? ans : -1);
    }
    return 0;
}
