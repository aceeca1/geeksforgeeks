#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

constexpr int m = 10000000;

bool palin(int n) {
    auto s = to_string(n);
    return equal(s.begin(), s.end(), s.rbegin());
}

int main() {
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    int t;
    scanf("%d", &t);
    while (t--) {
        int a;
        scanf("%d", &a);
        if (a >= 9989900) a = 100030001;
        else while (p[a] < a || !palin(a)) ++a;
        printf("%d\n", a);
    }
    return 0;
}
