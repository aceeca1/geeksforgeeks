// Almost the same as p443
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct Meeting {
    int s, t;
    bool operator<(const Meeting& that) const {
        return s < that.s || s == that.s && t > that.t;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<Meeting> v(n);
        for (int i = 0; i < n; ++i) scanf("%d", &v[i].s);
        for (int i = 0; i < n; ++i) scanf("%d", &v[i].t);
        sort(v.begin(), v.end());
        int z = 1;
        for (int i = 1; i < n; ++i) {
            while (z && v[z - 1].t >= v[i].t) --z;
            v[z++] = v[i];
        }
        v.resize(z);
        int i = 0, start = 0, ans = 0;
        for (;;) {
            while (i < n && v[i].s < start) ++i;
            if (i == n) break;
            ++ans;
            start = v[i].t;
        }
        printf("%d\n", ans);
    }
    return 0;
}
