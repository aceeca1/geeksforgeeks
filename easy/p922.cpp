#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf("%s", s);
        int ans = -1, a = 0, sum = 0;
        for (int i = 0; s[i]; ++i) {
            sum += s[i] == '1';
            int b = s[i] == '1' ? -1 : 1;
            if (a > 0) b += a;
            if (b > ans) ans = b;
            a = b;
        }
        printf("%d\n", sum + ans);
    }
    return 0;
}
