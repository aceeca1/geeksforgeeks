#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int k, n;
        scanf("%d%d", &k, &n);
        vector<vector<int>> a0(n, vector<int>(k + 1)), a1(a0);
        a1[0][0] = 1;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                int a;
                scanf("%d", &a);
                for (int l = 0; l <= k; ++l) {
                    if (l < a) { a0[j][l] = 0; continue; }
                    a0[j][l] = a1[j][l - a];
                    if (j) a0[j][l] += a0[j - 1][l - a];
                }
            }
            swap(a0, a1);
        }
        printf("%d\n", a1.back()[k]);
    }
    return 0;
}
