#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int r, s, q;
        scanf("%d%d%d", &r, &s, &q);
        printf("%d\n", (q - s - 1) / (r - s) + 1);
    }
    return 0;
}
