#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[220];
        scanf("%s", s);
        int z = 0;
        for (int i = 0; s[i]; ++i) {
            if (s[i] == 'b') continue;
            if (s[i] == 'a' && s[i + 1] == 'c') { ++i; continue; }
            s[z++] = s[i];
        }
        s[z] = 0;
        printf("%s\n", s);
    }
    return 0;
}
