#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        if (!prev_permutation(s, s + strlen(s))) printf("-1\n");
        else printf("%s\n", s);
    }
    return 0;
}
