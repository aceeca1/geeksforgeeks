#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    auto s5 = sqrt(5.0L), a = (s5 + 1.0) * 0.5;
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%.0Lf\n", pow(a, n + 1) / s5);
    }
    return 0;
}
