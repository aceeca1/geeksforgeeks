#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int i = n - 1;
        while (i >= 0 && a[i] == 9) a[i--] = 0;
        if (i == -1) a.emplace(a.begin(), 1);
        else ++a[i];
        printf("%d", a[0]);
        for (int i = 1; i < a.size(); ++i) printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
