#include <cstdio>
#include <cmath>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        char s[88];
        sprintf(s, "%.0f", pow(2, n));
        --s[strlen(s) - 1];
        printf("%s\n", s);
    }
    return 0;
}
