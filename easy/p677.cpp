#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a1, a0;
        scanf("%d%d", &n, &a1);
        while (--n) {
            scanf("%d", &a0);
            if (a0 <= a1) break;
            a1 = a0;
        }
        printf("%d\n", !n);
        while (--n > 0) scanf("%*d");
    }
    return 0;
}
