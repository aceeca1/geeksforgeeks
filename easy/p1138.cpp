#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n, x, y;
        scanf("%" SCNd64 "%" SCNd64 "%" SCNd64, &n, &x, &y);
        while (y) swap(x %= y, y);
        while (x--) printf("%" PRId64, n);
        printf("\n");
    }
    return 0;
}
