#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", ((n & 0x55555555) << 1) + ((n >> 1) & 0x55555555));
    }
    return 0;
}
