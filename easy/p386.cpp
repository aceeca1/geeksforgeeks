#include <cstdio>
#include <vector>
#include <string>
using namespace std;

const char *dial[10] = {
    "", "", "abc", "def", "ghi",
    "jkl", "mno", "pqrs", "tuv", "wxyz"};

vector<int> a;
string pr;
bool head;

void put() {
    if (pr.size() == a.size()) {
        if (!head) putchar(' ');
        head = false;
        printf("%s", pr.c_str());
    }
    const char *dap = dial[a[pr.size()]];
    for (int i = 0; dap[i]; ++i) {
        pr += dap[i];
        put();
        pr.pop_back();
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        a.resize(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        head = true;
        put();
        printf("\n");
    }
    return 0;
}
