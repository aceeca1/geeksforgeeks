#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a[2]{1, 0}, s = 0;
        scanf("%d", &n);
        while (n--) {
            int c;
            scanf("%d", &c);
            s ^= c & 1;
            ++a[s];
        }
        a[0] = a[0] * (a[0] - 1) >> 1;
        a[1] = a[1] * (a[1] - 1) >> 1;
        printf("%d\n",  a[0] + a[1]);
    }
    return 0;
}
