#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int n;

int64_t cut() {
    if (n == 1) return 0;
    if (n == 2) return 1;
    if (n == 3) return 2;
    int64_t ans = 1;
    for (; n > 4; n -= 3) ans *= 3;
    return ans * n;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d", &n);
        printf("%" PRId64 "\n", cut());
    }
    return 0;
}
