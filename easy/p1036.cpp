#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, d;
        scanf("%d%d", &n, &d);
        int a = 0, b = 1, c = 1, e = 1;
        for (;;) {
            int p = a + c, q = b + e;
            if (q > 10000) break;
            if (n * q <= p * d) { c = p; e = q; }
            else { a = p; b = q; }
        }
        printf("%d %d\n", a, b);
    }
    return 0;
}
