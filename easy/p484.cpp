// See http://www.cut-the-knot.org/blue/Sylvester2.shtml
#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, y;
        scanf("%d%d", &x, &y);
        int a = x, b = y;
        while (b) {
            a %= b;
            swap(a, b);
        }
        if (a == 1) printf("%d %d\n", x * y - x - y, (x - 1) * (y - 1) >> 1);
        else printf("NA\n");
    }
    return 0;
}
