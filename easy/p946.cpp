#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> l(t), r(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d%d", &l[ti], &r[ti]);
        if (r[ti] > m) m = r[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 3;
    for (int ti = 0; ti < t; ++ti) {
        int i = l[ti];
        while (i <= r[ti] && p[i] < i) ++i;
        while (i <= r[ti] - 2 && p[i] != i + 2) i = p[i];
        if (i > r[ti] - 2) printf("-1\n");
        else printf("%d %d\n", i, i + 2);
    }
    return 0;
}
