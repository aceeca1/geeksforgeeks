#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n & 1) printf("0\n");
        else printf("%d\n", ((n >> 1) - 1) >> 1);
    }
    return 0;
}
