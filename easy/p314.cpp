#include <cstdio>
#include <cstdint>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int64_t> a(n), b(n);
        int64_t s = 0, p = 1;
        for (int i = 0; i < n; ++i) {
            int c;
            scanf("%d", &c);
            a[i] = s += c;
            b[i] = p *= c;
        }
        int ans = 0;
        for (int i = 0; i < n; ++i)
            for (int j = i + 1; j <= n; ++j) {
                int64_t s = a[j - 1];
                if (i) s -= a[i - 1];
                int64_t p = b[j - 1];
                if (i) p /= b[i - 1];
                ans += s == p;
            }
        printf("%d\n", ans);
    }
    return 0;
}
