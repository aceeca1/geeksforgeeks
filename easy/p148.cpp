#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int64_t a = n * (n + 1) >> 1;
        printf("%" PRId64 "\n", a * a - a * (n + n + 1) / 3);
    }
    return 0;
}
