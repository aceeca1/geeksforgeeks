#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[220];
        scanf("%s", s);
        int x = 0, y = 0, d = 0;
        for (int i = 0; s[i]; ++i) switch (s[i]) {
            case 'G': switch (d) {
                case 0: ++x; break;
                case 1: ++y; break;
                case 2: --x; break;
                case 3: --y;
            }; break;
            case 'L': d = (d + 1) & 3; break;
            case 'R': d = (d + 3) & 3;
        }
        if (x || y) printf("Not ");
        printf("Circular\n");
    }
    return 0;
}
