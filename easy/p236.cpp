#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        bool a[26 * 26]{};
        for (int i = 0; s[i + 1]; ++i)
            a[(s[i] - 'A') * 26 + (s[i + 1] - 'A')] = true;
        int ans = 0;
        for (bool i: a) ans += i;
        printf("%d\n", ans);
    }
    return 0;
}
