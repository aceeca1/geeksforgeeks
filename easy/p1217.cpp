#include <cstdio>
#include <cmath>
using namespace std;

constexpr double S5 = sqrt(5), A = (S5 + 1.0) * 0.5;

int fib(int n) {
    int k = round(log(n * S5) / log(A));
    if (round(pow(A, k) / S5) > n) --k;
    return k - 1;
}

int rNFib(int n) {
    int r = 1;
    for (;;) {
        int k = (n + r) - fib(n + r);
        if (k >= n) break;
        r += r;
    }
    int l = n;
    r += n;
    while (l < r) {
        int m = (l + r) >> 1;
        int k = m - fib(m);
        if (k < n) l = m + 1;
        else r = m;
    }
    return l;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", rNFib(n));
    }
    return 0;
}
