#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, n, m, b = 1, c = 0;
        scanf("%d%d", &a, &n);
        m = n;
        while (m) {
            int k = a / m;
            swap(a -= k * m, m);
            swap(b -= k * c, c);
        }
        b = (b % n + n + n - 1) % n + 1;
        if (a == 1) printf("%d\n", b);
        else printf("-1\n");
    }
    return 0;
}
