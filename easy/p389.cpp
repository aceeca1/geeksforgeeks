#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int n;
vector<int> a;

void put(int p, int q) {
    if (p > n) return;
    a.emplace_back(p);
    int p1 = p * 10;
    if (q) put(q - 1 + p1, q - 1);
    if (q < 9) put(q + 1 + p1, q + 1);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d", &n);
        putchar('0');
        for (int i = 1; i < 10; ++i) {
            put(i, i);
            sort(a.begin(), a.end());
            for (int j: a) printf(" %d", j);
            a.clear();
        }
        printf("\n");
    }
    return 0;
}
