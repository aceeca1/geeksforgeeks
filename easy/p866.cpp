#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <queue>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        priority_queue<int> pq;
        int64_t ans = 0;
        while (n--) {
            int a;
            scanf("%d", &a);
            pq.emplace(a);
        }
        while (k--) {
            int pqH = pq.top();
            pq.pop();
            ans += pqH;
            pq.emplace(pqH >> 1);
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
