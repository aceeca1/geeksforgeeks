#include <cstdio>
#include <vector>
using namespace std;

struct Matrix {
    vector<vector<int>> v;
    Matrix(int n): v(n, vector<int>(n)) {}
    void operator*=(const Matrix& that) {
        int n = v.size();
        Matrix ans(n);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                for (int k = 0; k < n; ++k)
                    ans.v[i][k] += v[i][j] * that.v[j][k];
        v = move(ans.v);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, u, v, k;
        scanf("%d", &n);
        Matrix a(n), ans(n);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j) {
                scanf("%d", &a.v[i][j]);
                ans.v[i][j] = i == j;
            }
        scanf("%d%d%d", &u, &v, &k);
        while (k) {
            if (k & 1) ans *= a;
            a *= a;
            k >>= 1;
        }
        printf("%d\n", ans.v[u][v]);
    }
    return 0;
}
