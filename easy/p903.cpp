#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int64_t> a(n);
        for (int i = 0; i < n; ++i) scanf("%" SCNd64, &a[i]);
        int p = 0, q = n - 1, ans = 0;
        int64_t now = -1;
        while (p <= q) {
            int t1 = -a[p];
            int t2 = a[q];
            if (t1 < t2) {
                if (t2 != now) { now = t2; ++ans; }
                --q;
            } else {
                if (t1 != now) { now = t1; ++ans; }
                ++p;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
