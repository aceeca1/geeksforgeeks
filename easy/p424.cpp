#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[1100], s2[1100];
        scanf("%s%s", s1, s2);
        int z2 = strlen(s2);
        int z = 0, i = 0;
        bool head = true;
        for (;;) {
            auto p = strstr(s1 + i, s2);
            if (!p) break;
            int t = p - s1 - i;
            if (t) {
                memmove(s1 + z, s1 + i, t);
                z += t;
                s1[z++] = 'X';
            } else if (head) s1[z++] = 'X';
            head = false;
            i = p - s1 + z2;
        }
        memmove(s1 + z, s1 + i, strlen(s1 + i) + 1);
        printf("%s\n", s1);
    }
    return 0;
}
