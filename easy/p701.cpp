#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, d = 1, x = 0, y = 0;
        scanf("%d", &n);
        while (n--) {
            char c;
            int a;
            scanf(" %c%d", &c, &a);
            switch (c) {
                case 'L': d = (d + 1) & 3; break;
                case 'D': d = (d + 2) & 3; break;
                case 'R': d = (d + 3) & 3;
            }
            switch (d) {
                case 0: x += a; break;
                case 1: y += a; break;
                case 2: x -= a; break;
                case 3: y -= a;
            }
        }
        printf("%d ", int(hypot(x, y)));
        switch (d) {
            case 0: printf("E\n"); break;
            case 1: printf("N\n"); break;
            case 2: printf("W\n"); break;
            case 3: printf("S\n");
        }
    }
    return 0;
}
