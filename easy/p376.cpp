#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1 = 0x7fffffff, p = -1;
        scanf("%d%d", &n, &a1);
        bool head = true;
        for (int i = 1; i <= n; ++i) {
            if (i < n) scanf("%d", &a0);
            if (i < n && a0 > a1) {
                if (p < 0) p = i - 1;
            } else if (p >= 0) {
                if (!head) putchar(' ');
                head = false;
                printf("(%d %d)", p, i - 1);
                p = -1;
            }
            a1 = a0;
        }
        if (head) printf("No Profit\n");
        else printf("\n");
    }
    return 0;
}
