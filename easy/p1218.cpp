#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a = 1, b = 1;
        while (a * b <= n) {
            n -= a * b;
            b = a == 1 ? 1 : b + 1;
            a = a == 1 ? 9 : a * 10;
        }
        a = a / 9 + n / b;
        b = b - 1 - n % b;
        while (b--) a /= 10;
        printf("%d\n", a % 10);
    }
    return 0;
}
