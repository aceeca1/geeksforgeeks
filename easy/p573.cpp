#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        vector<int> b = a;
        sort(b.begin(), b.end());
        int p = 0;
        while (p < n && a[p] == b[p]) ++p;
        if (p == n) printf("0 0\n");
        else {
            int q = n - 1;
            while (a[q] == b[q]) --q;
            printf("%d %d\n", p, q);
        }
    }
    return 0;
}
