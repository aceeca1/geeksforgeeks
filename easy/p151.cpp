#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        uint64_t n, a;
        scanf("%" SCNd64, &n);
        a = ((n & 0x5555555555555555) << 1) + ((n >> 1) & 0x5555555555555555);
        a = ((a & 0x3333333333333333) << 2) + ((a >> 2) & 0x3333333333333333);
        a = ((a & 0x0f0f0f0f0f0f0f0f) << 4) + ((a >> 4) & 0x0f0f0f0f0f0f0f0f);
        a = ((a & 0x00ff00ff00ff00ff) << 8) + ((a >> 8) & 0x00ff00ff00ff00ff);
        a = ((a & 0x0000ffff0000ffff) <<16) + ((a >>16) & 0x0000ffff0000ffff);
        a = (a << 32) + (a >> 32);
        a /= a & -a;
        printf("%d\n", a == n);
    }
    return 0;
}
