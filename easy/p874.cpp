#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100000];
        int k;
        scanf("%s%d", s, &k);
        if (strlen(s) < 26) { printf("0\n"); continue; }
        bool a[256]{};
        for (int i = 0; s[i]; ++i) a[s[i]] = true;
        for (int i = 'a'; i <= 'z'; ++i) k += a[i];
        printf("%d\n", k >= 26);
    }
    return 0;
}
