#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        unsigned n;
        scanf("%u", &n);
        string s;
        while (n--) {
            s += 'A' + n % 26;
            n /= 26;
        }
        reverse(s.begin(), s.end());
        printf("%s\n", s.c_str());
    }
    return 0;
}
