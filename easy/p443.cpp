#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct Meeting {
    int s, t, no;
    bool operator<(const Meeting& that) const {
        return s < that.s || s == that.s && t > that.t;
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<Meeting> v(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &v[i].s);
            v[i].no = i + 1;
        }
        for (int i = 0; i < n; ++i) scanf("%d", &v[i].t);
        sort(v.begin(), v.end());
        int z = 1;
        for (int i = 1; i < n; ++i) {
            while (z && v[z - 1].t >= v[i].t) --z;
            v[z++] = v[i];
        }
        v.resize(z);
        int i = 0, start = 0;
        bool head = true;
        for (;;) {
            while (i < n && v[i].s < start) ++i;
            if (i == n) break;
            if (!head) putchar(' ');
            head = false;
            printf("%d", v[i].no);
            start = v[i].t + 1;
        }
        printf("\n");
    }
    return 0;
}
