#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a, b;
        while (n--) {
            int c;
            scanf("%d", &c);
            if (c & 1) b.emplace_back(c);
            else a.emplace_back(c);
        }
        sort(a.begin(), a.end());
        sort(b.begin(), b.end());
        bool head = true;
        for (int i = 0; i < a.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        for (int i = 0; i < b.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", b[i]);
        }
        printf("\n");
    }
    return 0;
}
