#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0, b = 0, c = 1;
        scanf("%d", &n);
        ++n;
        for (int i = n; i; i /= 10) b += i % 10 == 3;
        for (; n; n /= 10) {
            int a = n % 10;
            b -= a == 3;
            if (!b) {
                if (a > 3) --a;
                ans += a * c;
            }
            c *= 9;
        }
        printf("%d\n", ans - 1);
    }
    return 0;
}
