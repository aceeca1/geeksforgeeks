#include <cstdio>
using namespace std;

bool isPrime(int n) {
    for (int i = 2; i * i <= n; ++i)
        if (!(n % i)) return false;
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int a, b;
        scanf("%d%d", &a, &b);
        int n = a + b + 1;
        while (!isPrime(n)) ++n;
        printf("%d\n", n - a - b);
    }
    return 0;
}
