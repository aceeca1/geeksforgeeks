#include <cstdio>
#include <string>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        string s(n + 1, 0);
        scanf("%s", &s[0]);
        s.pop_back();
        int a[256];
        for (int i = 0; i < 256; ++i) a[i] = -1;
        for (int i = 0; i < s.size(); ++i)
            switch (a[s[i]]) {
                case -1: a[s[i]] = i; break;
                case -2: break;
                default: a[s[i]] = -2;
            }
        char ans;
        int min = 0x7fffffff;
        for (int i = 0; i < 256; ++i)
            if (a[i] >= 0 && a[i] < min) { min = a[i]; ans = i; }
        if (min == 0x7fffffff) printf("-1\n");
        else printf("%c\n", ans);
    }
    return 0;
}
