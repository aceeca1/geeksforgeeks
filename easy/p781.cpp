#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cmath>
#include <vector>
using namespace std;

vector<int> a;
int n;

int64_t next() {
    int i = 2, d = a[1] - a[0];
    for (; i < n; ++i)
        if (a[i] - a[i - 1] != d) break;
    if (i == n) return a[n - 1] + d;
    i = 2;
    double dd = double(a[1]) / a[0];
    for (; i < n; ++i)
        if (double(a[i]) / a[i - 1] != dd) break;
    if (i == n) return ceil(a[n - 1] * dd - 1e-12);
    i = 2;
    for (; i < n; ++i)
        if (a[i] != a[i - 1] + a[i - 2]) break;
    if (i == n) return a[n - 1] + a[n - 2];
    return -99999;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d", &n);
        a = vector<int>(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        printf("%" PRId64 "\n", next());
    }
    return 0;
}
