#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a = 0;
        scanf("%d", &n);
        while (n--) {
            int b;
            scanf("%d", &b);
            a ^= b;
        }
        printf("%d\n", a);
    }
    return 0;
}
