#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

constexpr int M = 1000000007;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf("%s", s);
        int z = strlen(s);
        vector<int> b(26);
        int u = 0;
        for (int i = 0; i < z; ++i) {
            int t = (u + 1) % M;
            u = (u + M - b[s[i] - 'a']) % M;
            b[s[i] - 'a'] = t;
            u = (u + t) % M;
        }
        printf("%d\n", (u + 1) % M);
    }
    return 0;
}
