#include <cstdio>
using namespace std;

int main() {
    int fact[6]{1, 1, 2, 6, 24, 120};
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int a[10]{}, b = 0;
        for (int i = 0; s[i]; ++i) {
            ++a[s[i] - '0'];
            ++b;
            s[i] = '1';
        }
        int p = fact[b], ans = 0;
        for (int i = 0; i < 10; ++i) p /= fact[a[i]];
        for (int i = 0; i < 10; ++i) ans += a[i] * p / b * i;
        sscanf(s, "%d", &b);
        printf("%d %d\n", p, ans * b);
    }
    return 0;
}
