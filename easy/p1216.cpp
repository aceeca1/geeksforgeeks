#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        for (int i = 2; i <= n; ++i) {
            long long j0, j1 = i, j2 = 1;
            while (j1 <= n) {
                ++ans;
                j0 = (j1 * j1 - 1) / j2;
                j2 = j1;
                j1 = j0;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
