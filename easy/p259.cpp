#include <cstdio>
#include <cstring>
#include <cstdint>
using namespace std;

uint64_t hash(int n) {
    const uint64_t kMul = 0x9ddfea08eb382d69ULL;
    uint64_t nL = n * kMul;
    nL ^= nL >> 47;
    nL *= kMul;
    return nL ^= nL >> 47;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[55], s2[55];
        scanf("%s%s", s1, s2);
        int h1 = 0, h2 = 0, z2 = strlen(s2), ans = 0;
        for (int i = 0; i < z2; ++i) h2 += hash(s2[i]);
        for (int i = 0; i < z2; ++i) h1 += hash(s1[i]);
        for (int i = z2; s1[i - 1]; ++i) {
            ans += h1 == h2;
            h1 += hash(s1[i]);
            h1 -= hash(s1[i - z2]);
        }
        printf("%d\n", ans);
    }
    return 0;
}
