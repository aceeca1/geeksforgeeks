#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int p = 0, q = 0, ans = 0;
        bool b[11]{};
        for (;;) {
            while (q < n && !b[a[q]]) {
                ans += q - p;
                b[a[q++]] = true;
            }
            if (q >= n) break;
            while (a[p] != a[q]) b[a[p++]] = false;
            ans += q++ - ++p;
        }
        printf("%d\n", ans);
    }
    return 0;
}
