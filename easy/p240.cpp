#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a1(n), a2(n), a(n + n);
        for (int i = 0; i < n; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n; ++i) scanf("%d", &a2[i]);
        merge(a1.begin(), a1.end(), a2.begin(), a2.end(), a.begin());
        printf("%d\n", a[n - 1] + a[n]);
    }
    return 0;
}
