#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a1(n), a2(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n; ++i) scanf("%d", &a2[i]);
        int v0 = 0, v1 = 0, v2 = 0;
        for (int i = 0; i < n; ++i) {
            int t = v2 + a1[i];
            if (t > v0) v0 = t;
            t = v2 + a2[i];
            if (t > v0) v0 = t;
            v2 = v1;
            v1 = v0;
        }
        printf("%d\n", v1);
    }
    return 0;
}
