#include <cstdio>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[22];
        scanf("%s", s);
        int a[256]{};
        for (int i = 0; s[i]; ++i) ++a[s[i]];
        char c = 0;
        int z = 0;
        for (int i = 0; i < 256; ++i) {
            if (a[i] & 1) {
                if (c) { c = -1; break; }
                c = i;
            }
            for (int j = a[i] >> 1; j; --j)
                s[z++] = i;
        }
        s[z] = 0;
        if (c == -1) printf("{ }\n");
        else {
            putchar('{');
            for (;;) {
                printf(" %s", s);
                if (c) putchar(c);
                reverse(s, s + z);
                printf("%s", s);
                reverse(s, s + z);
                if (!next_permutation(s, s + z)) break;
            }
            printf(" }\n");
        }
    }
    return 0;
}
