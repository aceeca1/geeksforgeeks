#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int m, n;
        scanf("%d%d", &m, &n);
        vector<double> a(m), b(n);
        for (int i = 0; i < m; ++i) {
            int k;
            scanf("%d", &k);
            a[i] = k / log(k);
        }
        for (int i = 0; i < n; ++i) {
            int k;
            scanf("%d", &k);
            b[i] = k / log(k);
        }
        sort(a.begin(), a.end());
        sort(b.begin(), b.end());
        int j = 0, ans = 0;
        for (int i = 0; i < m; ++i) {
            while (j < n && b[j] <= a[i]) ++j;
            ans += n - j;
        }
        printf("%d\n", ans);
    }
    return 0;
}
