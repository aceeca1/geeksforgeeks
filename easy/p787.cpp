#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> p(m + 1), q(m + 1), a(m + 1);
    int u = 0;
    a[1] = 1;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
        int c = i / v;
        int pc = p[c] > c ? c : p[c];
        q[i] = pc == v ? q[c] + 1 : 1;
        int t = pow(v, q[i] - 1) * ((v - 1) * q[i] + v);
        a[i] = a[i / pow(v, q[i])] * t;
    }
    for (int ni: n) printf("%d\n", a[ni]);
    return 0;
}
