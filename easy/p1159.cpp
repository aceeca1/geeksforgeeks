#include <cstdio>
using namespace std;

int a, b, c;

bool can() {
    if (!c) return a == b;
    b -= a;
    if (c < 0) { c = -c; b = -b; }
    return b >= 0 && !(b % c);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d%d", &a, &b, &c);
        printf("%d\n", can());
    }
    return 0;
}
