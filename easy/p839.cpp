#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int64_t ans = 0, a = 1;
        for (; n; n >>= 1) {
            if (n & 1) ans += a;
            a *= 7;
        }
        printf("%" PRId64 "\n", ans);
    }
    return 0;
}
