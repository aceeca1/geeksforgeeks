#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n % 1000) {
            printf("No\n");
            continue;
        }
        n /= 1000;
        vector<int> a(n + 1, 0x3fffffff);
        a[0] = 0;
        for (int i = 1; i <= 5; ++i) {
            for (int j = n; j >= i; --j) {
                int t = a[j - i] + 1;
                if (t < a[j]) a[j] = t;
            }
            for (int j = i + i; j <= n; ++j) {
                int t = a[j - i - i] + 3;
                if (t < a[j]) a[j] = t;
            }
        }
        printf("%d\n", a[n]);
    }
    return 0;
}
