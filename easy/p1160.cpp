#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        m %= n * (n + 1) >> 1;
        int k = (int(sqrt((m << 3) + 1)) - 1) >> 1;
        m -= k * (k + 1) >> 1;
        printf("%d\n", m);
    }
    return 0;
}
