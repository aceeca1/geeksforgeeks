#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, min = 0x7fffffff, ans = 0x80000000;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            int k = a - min;
            if (k > ans) ans = k;
            if (a < min) min = a;
        }
        printf("%d\n", ans);
    }
    return 0;
}
