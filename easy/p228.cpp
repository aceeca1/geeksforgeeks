#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;

char s[110];
int z;
vector<int> a1, a2;

void palin(vector<int>& ans) {
    vector<int> a(z + 1);
    a[0] = -1;
    for (int i = 1; i <= z; ++i) {
        int j = a[i - 1];
        while (j >= 0 && s[j] != s[i - 1]) j = a[j];
        a[i] = j + 1;
    }
    int j = 0;
    for (int i = z - 1; i >= 0; --i) {
        while (j >= 0 && s[j] != s[i]) j = a[j];
        ++j;
    }
    ans.clear();
    for (; j >= 0; j = a[j]) ans.emplace_back(j);
}

bool is() {
    int p1 = 0, p2 = a2.size() - 1;
    for (;;) {
        if (p1 >= a1.size()) break;
        while (p2 >= 0 && a1[p1] + a2[p2] < z) --p2;
        if (p2 < 0) break;
        if (a1[p1] + a2[p2] == z) {
            if (!(a1[p1] & a2[p2] & 1)) return true;
            ++p1, --p2;
        }
        if (p2 < 0) break;
        while (p1 < a1.size() && a1[p1] + a2[p2] > z) ++p1;
    }
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        z = strlen(s);
        palin(a1);
        reverse(s, s + z);
        palin(a2);
        printf("%d\n", is());
    }
    return 0;
}
