// Almost the same as p926
#include <cstdio>
#include <cctype>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110]; // Workaround
        scanf(" %[^\n]", s);
        bool a[256]{};
        for (int i = 0; s[i]; ++i)
            a[tolower(s[i])] = true;
        printf("%d\n", all_of(&a['a'], &a['z' + 1],
            [&](bool i){ return i; }));
    }
    return 0;
}
