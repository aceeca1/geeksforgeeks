#include <cstdio>
#include <cstdint>
#include <utility>
using namespace std;

int64_t cross(int x1, int y1, int x2, int y2) {
    return int64_t(x1) * y2 - int64_t(x2) * y1;
}

int crossTest(
    int x1, int y1, int x2, int y2,
    int x3, int y3, int x4, int y4) {
    auto a123 = cross(x2 - x1, y2 - y1, x3 - x1, y3 - y1);
    auto a124 = cross(x2 - x1, y2 - y1, x4 - x1, y4 - y1);
    if (!a123 && !a124) return -1;
    return !a123 || !a124 || (a123 ^ a124) < 0;
}

int x1, y1, x2, y2, x3, y3, x4, y4;

bool test1D() {
    if (x1 > x2 || x1 == x2 && y1 > y2) { swap(x1, x2); swap(y1, y2); }
    if (x3 > x4 || x3 == x4 && y3 > y4) { swap(x3, x4); swap(y3, y4); }
    if (x3 > x2 || x3 == x2 && y3 > y2) return false;
    if (x1 > x4 || x1 == x4 && y1 > y4) return false;
    return true;
}

bool intersect() {
    switch (crossTest(x1, y1, x2, y2, x3, y3, x4, y4)) {
        case -1: return test1D();
        case 0: return false;
        case 1: return crossTest(x3, y3, x4, y4, x1, y1, x2, y2);
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d%d%d%d%d%d%d", &x1, &y1, &x2, &y2, &x3, &y3, &x4, &y4);
        printf("%d\n", intersect());
    }
    return 0;
}
