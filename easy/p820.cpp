#include <cstdio>
#include <vector>
using namespace std;

vector<vector<int>> a;
int n, m, x;

bool has() {
    int p = n - 1, q = 0;
    while (p >= 0 && q < m) {
        if (a[p][q] < x) ++q;
        else if (a[p][q] > x) --p;
        else return true;
    }
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d", &n, &m);
        a = vector<vector<int>>(n, vector<int>(m));
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                scanf("%d", &a[i][j]);
        scanf("%d", &x);
        printf("%d\n", has());
    }
    return 0;
}
