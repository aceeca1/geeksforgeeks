#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];
        scanf("%s", s);
        int z = 0;
        bool a[256]{};
        for (int i = 0; s[i]; ++i) {
            if (!a[s[i]]) s[z++] = s[i];
            a[s[i]] = true;
        }
        s[z] = 0;
        printf("%s\n", s);
    }
    return 0;
}
