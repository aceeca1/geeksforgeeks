#include <cstdio>
#include <cstring>
using namespace std;

int comp(const char* s1, const char* s2) {
    while (*s1 == '0') ++s1;
    while (*s2 == '0') ++s2;
    int z1 = strlen(s1), z2 = strlen(s2);
    if (z1 < z2) return 1;
    if (z1 > z2) return 2;
    z1 = strcmp(s1, s2);
    if (z1 < 0) return 1;
    if (z1 > 0) return 2;
    return 3;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s1[220], s2[220];
        scanf("%s%s", s1, s2);
        printf("%d\n", comp(s1, s2));
    }
    return 0;
}
