#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

constexpr int M = 1000000007;

int powM(int a, int64_t b) {
    int ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = int64_t(ans) * a % M;
        a = int64_t(a) * a % M;
    }
    return ans;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n;
        scanf("%" SCNd64, &n);
        auto a = int64_t(powM(3, n) + 1) * powM(2, M - 2);
        printf("%d\n", int((a + M - powM(2, n)) % M));
    }
    return 0;
}
