// See https://en.wikipedia.org/wiki/Continued_fraction#Best_rational_within_an_interval
// and http://math.stackexchange.com/questions/1037286/finding-the-simplest-rational-in-a-closed-interval#answer-1037434
#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        double p, q;
        scanf("%lf%lf", &q, &p);
        p *= 0.01;
        q *= 0.01;
        int a = 0, b = 1, c = 1, d = 1, e, f;
        for (;;) {
            e = a + c;
            f = b + d;
            double m = double(e) / f;
            if (m < p) { a = e; b = f; }
            else if (m < q) break;
            else { c = e; d = f; }
        }
        printf("%d\n", f);
    }
    return 0;
}
