#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[220];
        scanf("%s", s);
        if (!s[1]) {
            printf("%d\n", s[0] == '0' || s[0] == '4' || s[0] == '8');
            continue;
        }
        int a[10]{};
        for (int i = 0; s[i]; ++i) ++a[s[i] - '0'];
        int odd = a[1] + a[3] + a[5] + a[7] + a[9];
        int even = a[0] + a[2] + a[4] + a[6] + a[8];
        bool ans = (odd && a[2] + a[6]) || (even >= 2 && a[0] + a[4] + a[8]);
        printf("%d\n", ans);
    }
    return 0;
}
