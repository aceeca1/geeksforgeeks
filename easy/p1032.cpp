#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &k, &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int maxS, i_max = 0, s = 0;
        for (int i = 0; i < k; ++i) s += a[i];
        maxS = s;
        for (int i = k; i < n; ++i) {
            s = s + a[i] - a[i - k];
            if (s > maxS) { maxS = s; i_max = i - k + 1; }
        }
        bool head = true;
        for (int i = i_max; i < i_max + k; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
