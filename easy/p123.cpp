#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        putchar('1');
        int64_t a = 1;
        for (int i = 0; i < n - 1; ++i) {
            a = a * (n - 1 - i) / (i + 1);
            printf(" %" PRId64, a);
        }
        printf("\n");
    }
    return 0;
}
