#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int n1 = n + 1;
        printf("%d\n", n * n1 * (n + n1) / 6);
    }
    return 0;
}
