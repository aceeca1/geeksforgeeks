#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <deque>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int64_t> a(n + 1);
        deque<int> b;
        b.emplace_back(0);
        for (int i = 1; i <= n; ++i) {
            scanf("%" SCNd64, &a[i]);
            if (b.front() < i - 4) b.pop_front();
            a[i] += a[b.front()];
            while (!b.empty() && a[i] <= a[b.back()]) b.pop_back();
            b.emplace_back(i);
        }
        if (!b.front() || b.front() < n - 3) b.pop_front();
        printf("%" PRId64 "\n", a[b.front()]);
    }
    return 0;
}
