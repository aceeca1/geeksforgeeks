#include <cstdio>
#include <cstring>
#include <string>
#include <utility>
using namespace std;

int k;
char s[33];
string ans;

void put(int p) {
    if (!s[p] || !k) {
        if (s > ans) ans = s;
        return;
    }
    if (strncmp(s, ans.c_str(), p) < 0) return;
    char max = s[p];
    for (int i = p + 1; s[i]; ++i)
        if (s[i] > max) max = s[i];
    if (max == s[p]) { put(p + 1); return; }
    for (int i = p + 1; s[i]; ++i)
        if (s[i] == max) {
            swap(s[p], s[i]);
            --k;
            put(p + 1);
            ++k;
            swap(s[p], s[i]);
        }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%s", &k, s);
        ans = s;
        put(0);
        printf("%s\n", ans.c_str());
    }
    return 0;
}
