#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a0, a1, d = 0;
        scanf("%d%d", &n, &a1);
        while (--n) {
            scanf("%d", &a0);
            if (d ^ (a0 < a1)) swap(a0, a1);
            d = !d;
            printf("%d ", a1);
            a1 = a0;
        }
        printf("%d\n", a1);
    }
    return 0;
}
