#include <cstdio>
#include <cstdint>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n == 1) { printf("0\n"); continue; }
        int a0, a1 = 1, a2 = 0;
        for (int i = 3; i <= n; ++i) {
            a0 = int64_t(a1 + a2) * (i - 1) % 1000000007;
            a2 = a1;
            a1 = a0;
        }
        printf("%d\n", a1);
    }
    return 0;
}
