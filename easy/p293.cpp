#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf("%s", s);
        int n = 0;
        for (int i = 0; s[i]; ++i) n += s[i] - '0';
        sprintf(s, "%d", n);
        string a(s);
        printf("%s\n", equal(a.begin(), a.end(), a.rbegin()) ? "YES" : "NO");
    }
    return 0;
}
