#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a1 = 0x80000000, a2 = 0x80000000, a3 = 0x80000000;
        int b1 = 0x7fffffff, b2 = 0x7fffffff;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a > a1) { a3 = a2; a2 = a1; a1 = a; }
            else if (a > a2) { a3 = a2; a2 = a; }
            else if (a > a3) a3 = a;
            if (a < b1) { b2 = b1; b1 = a; }
            else if (a < b2) b2 = a;
        }
        int ans1 = a1 * a2 * a3;
        int ans2 = a1 * b1 * b2;
        if (ans2 > ans1) ans1 = ans2;
        printf("%d\n", ans1);
    }
    return 0;
}
