#include <cstdio>
#include <cstdint>
#include <unordered_set>
using namespace std;

char s[22];

bool is() {
    unordered_set<int64_t> u;
    for (int i = 0; s[i]; ++i) {
        int64_t c = 1;
        for (int j = i; s[j]; ++j) {
            c *= s[j] - '0';
            if (!u.emplace(c).second) return false;
        }
    }
    return true;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s", s);
        printf("%d\n", is());
    }
    return 0;
}
