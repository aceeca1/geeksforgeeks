#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int k = sqrt(n + n);
        printf("%d\n", k * (k + 1) >> 1 == n);
    }
    return 0;
}
