#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t, m = 2;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int64_t> a(m + 1);
    a[0] = 1;
    a[1] = 1;
    a[2] = 2;
    for (int i = 3; i <= m; ++i) a[i] = a[i - 1] + a[i - 2] + a[i - 3];
    for (int ni: n) printf("%" PRId64 "\n", a[ni]);
    return 0;
}
