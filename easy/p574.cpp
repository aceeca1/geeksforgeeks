#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        vector<int> a1(n1), a2(n2), a(n1 + n2 - 1);
        for (int i = 0; i < n1; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < n2; ++i) scanf("%d", &a2[i]);
        for (int i = 0; i < n1; ++i)
            for (int j = 0; j < n2; ++j)
                a[i + j] += a1[i] * a2[j];
        printf("%d", a[0]);
        for (int i = 1; i < n1 + n2 - 1; ++i) printf(" %d", a[i]);
        printf("\n");
    }
    return 0;
}
