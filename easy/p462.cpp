#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        scanf(" %[^\n]", s);
        bool a[256]{};
        int z = 0;
        for (int i = 0; s[i]; ++i) {
            if (a[s[i]]) continue;
            a[s[i]] = true;
            s[z++] = s[i];
        }
        s[z] = 0;
        printf("%s\n", s);
    }
    return 0;
}
