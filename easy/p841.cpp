#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, q;
        scanf("%d%d", &n, &m);
        vector<vector<int>> a(n, vector<int>(m));
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j) {
                scanf("%d", &a[i][j]);
                if (i) a[i][j] += a[i - 1][j];
                if (j) a[i][j] += a[i][j - 1];
                if (i && j) a[i][j] -= a[i - 1][j - 1];
            }
        scanf("%d", &q);
        bool head = true;
        while (q--) {
            int u, v, ans = 0;
            scanf("%d%d", &u, &v);
            for (int i = -1; i < n - u; ++i)
                for (int j = -1; j < m - v; ++j) {
                    int s1 = a[i + u][j + v];
                    int s2 = i >= 0 ? a[i][j + v] : 0;
                    int s3 = j >= 0 ? a[i + u][j] : 0;
                    int s4 = i >= 0 && j >= 0 ? a[i][j] : 0;
                    int s = s1 - s2 - s3 + s4;
                    if (s > ans) ans = s;
                }
            if (!head) putchar(' ');
            head = false;
            printf("%d", ans);
        }
        printf("\n");
    }
    return 0;
}
