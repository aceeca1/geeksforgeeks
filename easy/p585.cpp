// Almost the same as p1053
#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        vector<int> b = a;
        sort(b.begin(), b.end());
        b.resize(unique(b.begin(), b.end()) - b.begin());
        for (int i = 0; i < n; ++i)
            a[i] = lower_bound(b.begin(), b.end(), a[i]) - b.begin();
        int m = 1 << (ilogb(b.size() + 2) + 1);
        b.clear();
        b.resize(m + m);
        for (int i = n - 1; i >= 0; --i) {
            int k = a[i] + m;
            a[i] = 0;
            for (; k > 1; k >>= 1) {
                if (k & 1) a[i] += b[k - 1];
                ++b[k];
            }
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
