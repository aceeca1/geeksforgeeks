#include <cstdio>
#include <vector>
#include <initializer_list>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x, y, z;
        scanf("%d%d%d%d", &n, &x, &y, &z);
        vector<int> a(n + 1, 0x80000000);
        a[0] = 0;
        for (int i: {x, y, z})
            for (int j = i; j <= n; ++j) {
                int t = a[j - i] + 1;
                if (t > a[j]) a[j] = t;
            }
        printf("%d\n", a[n]);
    }
    return 0;
}
