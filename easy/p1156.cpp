#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%" PRId64 "\n", int64_t(n - 1) * n + 2);
    }
    return 0;
}
