#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m = 0;
        scanf("%d", &n);
        int i = 1;
        for (; i * i < n; ++i) {
            if (n % i) continue;
            m += i;
            if (i != 1) m += n / i;
        }
        if (i * i == n) m += i;
        printf("%d\n", m == n);
    }
    return 0;
}
