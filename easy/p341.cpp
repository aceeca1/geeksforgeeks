#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a[3]{};
        scanf("%d", &n);
        while (n--) {
            int b;
            scanf("%d", &b);
            ++a[b];
        }
        bool head = true;
        for (int i = 0; i < 3; ++i)
            while (a[i]--) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", i);
            }
        printf("\n");
    }
    return 0;
}
