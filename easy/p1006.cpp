// Almost the same as p838
#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", int(sqrt(n)));
    }
    return 0;
}
