#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    double s5 = sqrt(5), a = (1 + s5) * 0.5;
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", (1 << n) - int(lround(pow(a, n + 2) / s5)));
    }
    return 0;
}
