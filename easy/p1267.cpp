#include <cstdio>
#include <string>
using namespace std;

string a;

void put(const char* s) {
    if (!*s) {
        printf("(%s)", a.c_str());
        return;
    }
    a += *s;
    put(s + 1);
    a.pop_back();
    if (!a.empty()) {
        a += ' ';
        a += *s;
        put(s + 1);
        a.pop_back();
        a.pop_back();
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[22];
        scanf("%s", s);
        put(s);
        printf("\n");
    }
    return 0;
}
