#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, zero = 0;
        scanf("%d", &n);
        bool head = true;
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", a);
            } else ++zero;
        }
        while (zero--) {
            if (!head) putchar(' ');
            head = false;
            putchar('0');
        }
        printf("\n");
    }
    return 0;
}
