#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        for (;;) {
            int a = n & (n >> 1);
            if (!a) break;
            a = 1 << ilogb(a);
            n = (n + a) & -a;
        }
        printf("%d\n", n);
    }
    return 0;
}
