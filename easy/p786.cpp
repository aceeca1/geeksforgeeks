#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t a, b, ans = 0;
        scanf("%" SCNd64 "%" SCNd64, &a, &b);
        while (b) {
            ans += a / b;
            swap(a %= b, b);
        }
        printf("%d\n", int(ans % 1000000007));
    }
    return 0;
}
