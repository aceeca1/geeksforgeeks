#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, p, ans = 0;
        scanf("%d%d", &n, &p);
        while (n) {
            n /= p;
            ans += n;
        }
        printf("%d\n", ans);
    }
    return 0;
}
