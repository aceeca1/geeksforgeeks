#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <string>
using namespace std;

struct BigInt {
    static constexpr auto M = 1000000000;
    vector<uint32_t> v;

    BigInt(uint32_t n = 0): v({n}) {}

    operator string() {
        string ans = to_string(v.back());
        char s[11];
        for (int i = v.size() - 2; i >= 0; --i) {
            sprintf(s, "%09" PRIu32, v[i]);
            ans += s;
        }
        return ans;
    }

    BigInt& operator*=(uint32_t that) {
        int64_t carry = 0;
        for (int i = 0; i < v.size(); ++i) {
            carry += int64_t(v[i]) * that;
            v[i] = carry % M;
            carry /= M;
        }
        if (carry) v.emplace_back(carry);
        return *this;
    }
};

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<BigInt> a{1};
    for (int i = 1; i <= m; ++i) {
        a.emplace_back(a.back());
        a.back() *= i;
    }
    for (int ni: n) printf("%s\n", string(a[ni]).c_str());
    return 0;
}
