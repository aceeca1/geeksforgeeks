#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n + 1);
        a[1] = 0;
        for (int i = 2; i <= n; ++i) a[i] = (a[i - 1] + k) % i;
        printf("%d\n", a[n] + 1);
    }
    return 0;
}
