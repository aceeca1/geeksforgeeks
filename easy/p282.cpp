#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[2200];
        scanf("%s", s);
        reverse(s, s + strlen(s));
        int p = 0, q = 0;
        for (;;) {
            while (s[q] && s[q] != '.') ++q;
            reverse(s + p, s + q);
            if (!s[q]) break;
            p = ++q;
        }
        printf("%s\n", s);
    }
    return 0;
}
