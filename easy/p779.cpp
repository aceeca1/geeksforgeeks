#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> a(m + 1);
    a[0] = 1;
    for (int i: {3, 5, 10})
        for (int j = i; j <= m; ++j)
            a[j] += a[j - i];
    for (int ni: n) printf("%d\n", a[ni]);
    return 0;
}
