#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

constexpr int a[16]{
    0, 1, 2, 3, 4, 5, 6, 9,
    12, 16, 20, 27, 36, 48, 64, 81
};

int64_t f(int n) {
    if (n < 16) return a[n];
    int k = (n - 11) / 5;
    return int64_t(a[n - 5 * k]) << (k + k);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        printf("%" PRId64 "\n", f(n));
    }
    return 0;
}
