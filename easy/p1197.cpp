#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a = (1 << (ilogb(n) + 1)) - 1;
        printf("%d %d\n", a - n, a);
    }
    return 0;
}
