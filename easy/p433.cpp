#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int ans = 1, minA = 1, maxA = 1;
        for (int i = 0; i < n; ++i) {
            int a;
            scanf("%d", &a);
            minA *= a;
            maxA *= a;
            if (a < 0) swap(minA, maxA);
            if (a < minA) minA = a;
            if (a > maxA) maxA = a;
            if (maxA > ans) ans = maxA;
        }
        printf("%d\n", ans);
    }
    return 0;
}
