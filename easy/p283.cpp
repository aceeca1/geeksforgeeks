#include <cstdio>
#include <unordered_set>
using namespace std;

unordered_set<int> a;

bool has() {
    for (auto i = a.begin(); i != a.end(); ++i) {
        auto j = i; ++j;
        for (; j != a.end(); ++j)
            if (a.count(*i + *j)) return true;
    }
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        a = unordered_set<int>();
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            a.emplace(b * b);
        }
        printf("%s\n", has() ? "Yes" : "No");
    }
    return 0;
}
