// Almost the same as p1032
#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int minS, i_min = 0, s = 0;
        for (int i = 0; i < k; ++i) s += a[i];
        minS = s;
        for (int i = k; i < n; ++i) {
            s = s + a[i] - a[i - k];
            if (s < minS) { minS = s; i_min = i - k + 1; }
        }
        printf("%d %d\n", i_min + 1, i_min + k);
    }
    return 0;
}
