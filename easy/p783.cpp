#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        n <<= 1;
        int n2 = n * n, ans = 0;
        for (int i = 1; i < n; ++i)
            ans += sqrt(n2 - i * i);
        printf("%d\n", ans);
    }
    return 0;
}
