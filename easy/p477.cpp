#include <cstdio>
#include <vector>
#include <unordered_set>
using namespace std;

vector<int> a;
int k, n;

bool dup() {
    unordered_set<int> b;
    for (int i = 0; i < k; ++i) {
        auto t = b.emplace(a[i]);
        if (!t.second) return true;
    }
    for (int i = k; i < n; ++i) {
        b.erase(a[i - k]);
        auto t = b.emplace(a[i]);
        if (!t.second) return true;
    }
    return false;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d", &k, &n);
        a = vector<int>(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        unordered_set<int> b;
        printf("%s\n", dup() ? "True" : "False");
    }
    return 0;
}
