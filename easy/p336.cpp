#include <cstdio>
#include <vector>
#include <initializer_list>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        vector<int> a(16);
        for (int i = 0; i < 16; ++i) scanf("%d", &a[i]);
        bool head = true;
        for (int i: {
            0, 1, 2, 3, 7, 11, 15, 14,
            13, 12, 8, 4, 5, 6, 10, 9}) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
