#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, y, ans = 0;
        scanf("%d%d", &x, &y);
        while (y) {
            ans += x / y;
            swap(x %= y, y);
        }
        printf("%d\n", ans);
    }
    return 0;
}
