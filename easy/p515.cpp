#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, max = 0x80000000, ans = 0;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a > max) {
                max = a;
                ++ans;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
