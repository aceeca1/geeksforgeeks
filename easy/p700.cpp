#include <cstdio>
#include <vector>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, b;
        scanf("%d%d", &n, &b);
        n >>= 1;
        int k = n * (b - 1);
        vector<int> a0(k + 1), a1(k + 1);
        a0[0] = a1[0] = 1;
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= k; ++j) {
                a0[j] = a0[j - 1] + a1[j];
                if (j >= b) a0[j] -= a1[j - b];
            }
            swap(a0, a1);
        }
        int ans = 0;
        for (int i = 0; i <= k; ++i)
            ans += a1[i] * a1[i];
        printf("%d\n", ans);
    }
    return 0;
}
