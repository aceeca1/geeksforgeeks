#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> p(t), q(t), r(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d%d%d", &p[ti], &q[ti], &r[ti]);
        if (p[ti] > m) m = p[ti];
        if (q[ti] > m) m = q[ti];
        if (r[ti] > m) m = r[ti];
    }
    vector<vector<vector<int>>> a(m + 1);
    for (int i = 0; i <= m; ++i) {
        a[i].resize(m + 1);
        for (int j = 0; j <= m; ++j) a[i][j].resize(j + 1);
    }
    for (int s = 0; s <= m + m + m; ++s) {
        int iT = m < s ? m : s;
        for (int i = 0; i <= iT; ++i) {
            int jS = (s - i + 1) >> 1, jT = s - i;
            if (m < jT) jT = m;
            for (int j = jS; j <= jT; ++j) {
                int k = s - i - j, i1 = i - 1;
                if (!i) a[i][j][k] = !j && !k;
                else if (i1 < k) a[i][j][k] = a[j][k][i1] + a[k][j][i1];
                else if (i1 < j) a[i][j][k] = a[j][i1][k] + a[k][j][i1];
                else a[i][j][k] = a[j][i1][k] + a[k][i1][j];
            }
        }
    }
    for (int ti = 0; ti < t; ++ti) {
        int pi = p[ti], qi = q[ti], ri = r[ti];
        int ans1 = qi > ri ? a[pi][qi][ri] : a[pi][ri][qi];
        int ans2 = ri > pi ? a[qi][ri][pi] : a[qi][pi][ri];
        int ans3 = pi > qi ? a[ri][pi][qi] : a[ri][qi][pi];
        printf("%d\n", (ans1 + ans2 + ans3) >> 1);
    }
    return 0;
}
