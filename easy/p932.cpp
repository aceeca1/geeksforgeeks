#include <cstdio>
#include <utility>
using namespace std;

int cross(int x1, int y1, int x2, int y2) {
    return x1 * y2 - x2 * y1;
}

int edge(int x, int y) {
    while (y) swap(x %= y, y);
    if (x < 0) x = -x;
    return x;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x1, y1, x2, y2, x3, y3;
        scanf("%d%d%d%d%d%d", &x1, &y1, &x2, &y2, &x3, &y3);
        int ans = cross(x2 - x1, y2 - y1, x3 - x1, y3 - y1);
        if (ans < 0) ans = -ans;
        ans += 2;
        ans -= edge(x2 - x1, y2 - y1);
        ans -= edge(x3 - x2, y3 - y2);
        ans -= edge(x1 - x3, y1 - y3);
        printf("%d\n", ans >> 1);
    }
    return 0;
}
