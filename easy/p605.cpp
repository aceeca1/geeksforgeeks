#include <cstdio>
#include <vector>
#include <stack>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        stack<int> s;
        bool head = true;
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            while (!s.empty() && a[s.top()] <= a[i]) s.pop();
            if (!head) putchar(' ');
            head = false;
            printf("%d", s.empty() ? i + 1 : i - s.top());
            s.emplace(i);
        }
        printf("\n");
    }
    return 0;
}
