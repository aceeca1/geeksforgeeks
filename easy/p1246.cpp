#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <utility>
using namespace std;

constexpr int M = 1000000007;

int powM(int a, int b) {
    int ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = int64_t(ans) * a % M;
        a = int64_t(a) * a % M;
    }
    return ans;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, hx = 1, fx = 0;
        scanf("%d", &n);
        for (int i = 0; i < n; ++i) {
            int a;
            scanf("%d", &a);
            hx = int64_t(hx) * a % M;
            while (a) swap(fx %= a, a);
        }
        printf("%d\n", powM(hx, fx));
    }
    return 0;
}
