#include <cstdio>
using namespace std;

int n, k;

void put0() {
    if (k <= 0) return;
    printf("%d ", k);
    k -= 5;
    put0();
}

void put1() {
    if (k == n) {
        printf("%d\n", k);
        return;
    }
    printf("%d ", k);
    k += 5;
    put1();
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d", &n);
        k = n;
        put0();
        put1();
    }
    return 0;
}
