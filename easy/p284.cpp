#include <cstdio>
#include <cstring>
using namespace std;

char s1[1100], s2[1100];

bool is(const char* s1, const char* s2) {
    char tr[256]{};
    for (int i = 0; s1[i]; ++i) if (tr[s1[i]]) {
        if (tr[s1[i]] != s2[i]) return false;
    } else tr[s1[i]] = s2[i];
    return true;
}

bool is() {
    if (strlen(s1) != strlen(s2)) return false;
    return is(s1, s2) && is(s2, s1);
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%s%s", s1, s2);
        printf("%d\n", is());
    }
    return 0;
}
