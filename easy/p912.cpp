#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int d, l, r;
vector<int> ans;
int n = 0, k = 10;

void put() {
    if (n > r) return;
    if (n && n >= l) ans.emplace_back(n);
    int k0 = k;
    for (int i = 0; i < k; ++i) if (i != d) {
        n = n * 10 + i;
        k -= i;
        if (i < k || n == i) k = i;
        put();
        k = k0;
        n /= 10;
    }
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        scanf("%d%d%d", &d, &l, &r);
        put();
        if (d && !l) ans.emplace_back(0);
        sort(ans.begin(), ans.end());
        bool head = true;
        for (int i = 0; i < ans.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", ans[i]);
        }
        if (head) printf("-1\n");
        else printf("\n");
        ans.clear();
    }
    return 0;
}
