#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), b(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            b[i] = i && a[i] > a[i - 1] ? b[i - 1] + 1 : 1;
        }
        int ans = b[n - 1];
        for (int i = n - 2; i >= 0; --i) {
            if (a[i] > a[i + 1]) {
                int t = b[i + 1] + 1;
                if (t > b[i]) b[i] = t;
            }
            ans += b[i];
        }
        printf("%d\n", ans);
    }
    return 0;
}
