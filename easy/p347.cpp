#include <cstdio>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        unordered_map<int, int> a;
        int ans = 0;
        while (n--) {
            int b;
            scanf("%d", &b);
            auto a1 = a.find(b + k);
            if (a1 != a.end()) ans += a1->second;
            auto a2 = a.find(b - k);
            if (a2 != a.end()) ans += a2->second;
            ++a[b];
        }
        printf("%d\n", ans);
    }
    return 0;
}
