#include <cstdio>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            while (a) {
                ans %= a;
                swap(ans, a);
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
