#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    int fac[10];
    fac[0] = 1;
    for (int i = 1; i < 10; ++i) fac[i] = fac[i - 1] * i;
    while (t--) {
        char s[22];
        scanf("%s", s);
        int n;
        sscanf(s, "%d", &n);
        for (int i = 0; s[i]; ++i) n -= fac[s[i] - '0'];
        if (n) printf("Not ");
        printf("Strong\n");
    }
    return 0;
}
