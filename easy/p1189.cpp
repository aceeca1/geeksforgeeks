#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, p = -1, a0, a1, ans = 0;
        scanf("%d", &n);
        for (int i = 0; i <= n; ++i) {
            if (i < n) scanf("%d", &a0);
            if (p == -1) p = i;
            else if (i == n || a0 < a1) {
                int t = i - p;
                if (t > ans) ans = t;
                p = i;
            }
            a1 = a0;
        }
        printf("%d\n", ans);
    }
    return 0;
}
