#include <cstdio>
#include <vector>
#include <deque>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        deque<int> q;
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (q.front() <= i - k) q.pop_front();
            while (!q.empty() && a[q.back()] <= a[i]) q.pop_back();
            q.emplace_back(i);
            if (i >= k - 1) {
                if (!head) putchar(' ');
                head = false;
                printf("%d", a[q.front()]);
            }
        }
        printf("\n");
    }
    return 0;
}
