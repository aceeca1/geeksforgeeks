#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int k = (a.back() - a[0]) / n;
        for (int i = 0; i < n; ++i) {
            int p = a[0] + i * k;
            if (p != a[i]) {
                printf("%d\n", p);
                break;
            }
        }
    }
    return 0;
}
