#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int k, n;
        scanf("%d%d", &k, &n);
        vector<int> a0{1}, a1;
        while (n--) {
            swap(a0, a1);
            int b;
            scanf("%d", &b);
            if (b < 0) b = -b;
            int z = a1.size();
            a0.resize(z + b);
            for (int i = 0; i < a0.size(); ++i) a0[i] = 0;
            for (int i = 0; i < z; ++i) {
                a0[i] += a1[i];
                a0[i + b] += a1[i];
                if (!i) continue;
                if (i >= b) a0[i - b] += a1[i];
                if (i <= b) a0[b - i] += a1[i];
            }
        }
        printf("%d\n", a0[k]);
    }
    return 0;
}
