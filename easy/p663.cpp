#include <cstdio>
#include <cmath>
using namespace std;

// Deprecated: sscanf is slow
int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[44];
        scanf("%s", s);
        double n;
        int k = 0;
        sscanf(s, "%lf%n", &n, &k);
        for (;;) {
            char op;
            int a, ki;
            if (sscanf(s + k, "%c%d%n", &op, &a, &ki) < 2) break;
            k += ki;
            switch (op) {
                case '+': n += a; break;
                case '-': n -= a; break;
                case '*': n *= a; break;
                case '/': n /= a;
            }
        }
        printf("%.0f\n", floor(n));
    }
    return 0;
}
