#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <cstring>
#include <cmath>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int p;
        char s[22];
        scanf("%d 0.%s", &p, s);
        int z = strlen(s);
        int64_t f, b;
        sscanf(s, "%" SCNd64, &f);
        if (!p) b = pow(10.0L, z);
        else {
            int64_t a = pow(10.0L, p), a1 = a - 1;
            b = a1 * pow(10.0L, z - p);
            f -= f / a;
        }
        int64_t c = f, d = b;
        while (d) swap(c %= d, d);
        printf("%" PRId64 "/%" PRId64 "\n", f / c, b / c);
    }
    return 0;
}
