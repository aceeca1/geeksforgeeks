#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, w;
        scanf("%d%d", &n, &w);
        vector<int> v(n), h(n);
        for (int i = 0; i < n; ++i) scanf("%d", &v[i]);
        for (int i = 0; i < n; ++i) scanf("%d", &h[i]);
        vector<int> a(w + 1);
        for (int i = 0; i < n; ++i)
            for (int j = w; j >= h[i]; --j) {
                int t = a[j - h[i]] + v[i];
                if (t > a[j]) a[j] = t;
            }
        printf("%d\n", a[w]);
    }
    return 0;
}
