#include <cstdio>
#include <cstring>
#include <cctype>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110];  // workaround
        scanf("%s", s);
        int p = 0, q = strlen(s) - 1;
        for (;;) {
            while (p < q && !isalnum(s[p])) ++p;
            while (p < q && !isalnum(s[q])) --q;
            if (p >= q) break;
            swap(s[p++], s[q--]);
        }
        printf("%s\n", s);
    }
    return 0;
}
