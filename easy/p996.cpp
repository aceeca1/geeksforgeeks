#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    int t, m = 0;
    scanf("%d", &t);
    vector<int> n(t);
    for (int ti = 0; ti < t; ++ti) {
        scanf("%d", &n[ti]);
        if (n[ti] > m) m = n[ti];
    }
    vector<int> p(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
    int sm = sqrt(m), ssm = sqrt(sm);
    for (int i = sm; i; --i)
        for (int j = ssm; j; --j) {
            int t = i * i + (j * j) * (j * j);
            if (t > m) continue;
            if (p[t] > t) p[t] = 0;
        }
    p[0] = p[1] = 0;
    for (int i = 2; i <= m; ++i)
        p[i] = p[i - 1] + !p[i];
    for (int ni: n) printf("%d\n", p[ni]);
    return 0;
}
