#include <cstdio>
#include <cstdint>
#include <cinttypes>
using namespace std;

int main() {
    int a[211]{};
    for (int i = 1; i <= 210; ++i)
        a[i] = a[i - 1] + ((i & 1) && (i % 3) && (i % 5) && (i % 7));
    int t;
    scanf("%d", &t);
    while (t--) {
        int64_t n;
        scanf("%" SCNd64, &n);
        auto k = n / 210;
        printf("%" PRId64 "\n", a[n - 210 * k] + a[210] * k);
    }
    return 0;
}
