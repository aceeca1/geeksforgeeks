#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x, i = 0;
        scanf("%d%d", &n, &x);
        for (; i < n; ++i) {
            int a;
            scanf("%d", &a);
            if (a == x) break;
        }
        if (i == n) printf("OOPS! NOT FOUND\n");
        else printf("%d\n", i);
        for (++i; i < n; ++i) scanf("%*d");
    }
    return 0;
}
