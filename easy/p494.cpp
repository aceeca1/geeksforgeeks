#include <cstdio>
#include <map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        map<int, int> m;
        m.emplace(0x80000000, 0);
        int ans = 0;
        for (int i = 0; i < n; ++i) {
            int a;
            scanf("%d", &a);
            auto ma = --m.lower_bound(a);
            int av = ma->second + a;
            if (av > ans) ans = av;
            auto p = m.emplace(a, av);
            auto pf = p.first;
            if (!p.second && av > pf->second) pf->second = av;
            auto q = pf; --q;
            if (q->second >= pf->second) { m.erase(pf); continue; }
            for (;;) {
                auto q = pf; ++q;
                if (q == m.end() || q->second > pf->second) break;
                m.erase(q);
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
