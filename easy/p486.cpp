#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m, x1, y1, x2, y2;
        scanf("%d%d", &n, &m);
        vector<vector<int>> a(n, vector<int>(m));
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                scanf("%d", &a[i][j]);
        scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
        int ans = 0;
        for (int i = x1 - 1; i <= x2 - 1; ++i)
            for (int j = y1 - 1; j <= y2 - 1; ++j)
                ans += a[i][j];
        printf("%d\n", ans);
    }
    return 0;
}
