#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, a = 1, ans = 0;
        scanf("%d", &n);
        while (n) {
            if (n & 1) ans += a;
            n >>= 1;
            a *= 5;
        }
        printf("%d\n", ans * 5);
    }
    return 0;
}
