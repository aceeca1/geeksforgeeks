#include <cstdio>
#include <cstring>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[55];
        scanf("%s", s);
        int z = strlen(s);
        for (int i = 0; i < z; ++i) {
            int i1 = i;
            int i2 = z - 1 - i1;
            if (i1 > i2) swap(i1, i2);
            for (int j = 0; j < i1; ++j) putchar(' ');
            putchar(s[i1]);
            for (int j = i1 + 1; j < i2; ++j) putchar(' ');
            if (i1 != i2) putchar(s[i2]);
            for (int j = i2 + 1; j < z; ++j) putchar(' ');
        }
        printf("\n");
    }
    return 0;
}
