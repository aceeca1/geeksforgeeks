#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x;
        scanf("%d%d", &n, &x);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        int p = 0, q = n - 1, ans = 0x7fffffff, ans_p, ans_q;
        while (p < q) {
            int t = a[p] + a[q] - x;
            if (t >= 0) {
                if (t < ans) { ans = t; ans_p = p; ans_q = q; }
                --q;
            } else {
                t = -t;
                if (t < ans) { ans = t; ans_p = p; ans_q = q; }
                ++p;
            }
        }
        printf("%d %d\n", a[ans_p], a[ans_q]);
    }
    return 0;
}
