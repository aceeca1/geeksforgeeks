#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n), b(n), ans(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            b[i] = i;
        }
        while (b.size() > 1) {
            int z = 0;
            for (int i = 0; i < b.size(); i += 2) {
                if (i + 1 < b.size()) {
                    ++ans[b[i]];
                    ++ans[b[i + 1]];
                }
                if (b.size() <= i + 1 || a[b[i]] > a[b[i + 1]])
                    b[z++] = b[i];
                else b[z++] = b[i + 1];
            }
            b.resize(z);
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", ans[i]);
        }
        printf("\n");
    }
    return 0;
}
