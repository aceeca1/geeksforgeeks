#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[1100];
        int k, a[26]{}, b[26]{}, aU = 0, bU = 0, ans = 0;
        scanf("%s%d", s, &k);
        int p = 0, qA = 0, qB = 0;
        while (s[p]) {
            while (s[qA] && aU < k) {
                int &asqA = a[s[qA++] - 'a'];
                aU -= bool(asqA);
                aU += bool(++asqA);
            }
            while (s[qB] && bU <= k) {
                int &bsqB = b[s[qB++] - 'a'];
                bU -= bool(bsqB);
                bU += bool(++bsqB);
            }
            ans += qB + (bU <= k) - qA - (aU < k);
            int &asp = a[s[p] - 'a'];
            aU -= bool(asp);
            aU += bool(--asp);
            int &bsp = b[s[p++] - 'a'];
            bU -= bool(bsp);
            bU += bool(--bsp);
        }
        printf("%d\n", ans);
    }
    return 0;
}
