#include <cstdio>
#include <cstdint>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        scanf("%d", &x);
        int ans = 0;
        for (int i = 0; i < n; ++i)
            ans = (int64_t(ans) * x + a[i]) % 1000000007;
        printf("%d\n", ans);
    }
    return 0;
}
