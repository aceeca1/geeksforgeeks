#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        scanf("%d", &m);
        int p = 0, q = 0;
        for (;;) {
            while (q < n && a[q] == 1) ++q;
            if (q == n || !m--) break;
            ++q;
        }
        int ans = 0;
        for (;;) {
            int t = q - p;
            if (t > ans) ans = t;
            if (q == n) break;
            ++q;
            while (q < n && a[q] == 1) ++q;
            while (a[p] == 1) ++p;
            ++p;
        }
        printf("%d\n", ans);
    }
    return 0;
}
