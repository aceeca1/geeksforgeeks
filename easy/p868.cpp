#include <cstdio>
using namespace std;

constexpr int M = 1000000007;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, max = 0, max_n = 0, min = 0x7fffffff, min_n = 0;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a < min) { min = a; min_n = 1; }
            else if (a == min) ++min_n;
            if (a > max) { max = a; max_n = 1; }
            else if (a == max) ++max_n;
        }
        int ans1 = 1, ans2 = 1;
        while (max_n--) ans1 = (ans1 + ans1) % M;
        while (min_n--) ans2 = (ans2 + ans2) % M;
        printf("%d %d\n", (ans1 + M - 1) % M, (ans2 + M - 1) % M);
    }
    return 0;
}
