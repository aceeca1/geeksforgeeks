#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int m, n;
        scanf("%d%d", &m, &n);
        if (m == n) { printf("%d\n", m); continue; }
        int mn = m & n;
        int h = 0, ans = -1;
        while ((m ^ n) != 1) {
            if (!(m & 1)) ans &= (m + 1) << h;
            if (n & 1) ans &= (n - 1) << h;
            ++h;
            m >>= 1;
            n >>= 1;
        }
        printf("%d\n", ans & mn);
    }
    return 0;
}
