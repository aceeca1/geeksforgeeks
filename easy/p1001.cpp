#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 1;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a > ans) break;
            ans += a;
        }
        printf("%d\n", ans);
        while (n-- > 0) scanf("%*d");
    }
    return 0;
}
