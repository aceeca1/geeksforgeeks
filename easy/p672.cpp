#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    const char d[] = "0123456789ABCDEF";
    while (t--) {
        int n;
        char s[11];
        scanf("%d%s", &n, s);
        int k = 3;
        while (k >= 0 && s[k] == d[n - 1]) s[k--] = '0';
        if (k == -1) printf("10000\n");
        else {
            s[k] = s[k] == '9' ? 'A' : s[k] + 1;
            printf("%s\n", s);
        }
    }
    return 0;
}
