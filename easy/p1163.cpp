#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (n == 1) {
            printf("1\n");
            continue;
        }
        double logF = log((M_PI + M_PI) * n) * 0.5 + log(n) * n - n;
        printf("%.0f\n", ceil(logF / M_LN10));
    }
    return 0;
}
