#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        bool head = true;
        for (int i = 0; i < n; ++i) if (~a[i] & 1) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        for (int i = 0; i < n; ++i) if (a[i] & 1) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
