#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 0;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a > ans) ans = a;
        }
        printf("%d\n", ans);
    }
    return 0;
}
