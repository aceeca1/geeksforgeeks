#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k, x;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        scanf("%d%d", &k, &x);
        x = lower_bound(a.begin(), a.end(), x) - a.begin();
        k >>= 1;
        int lb = x - k, ub = x + k;
        if (lb < 0) { ub -= lb; lb = 0; }
        if (ub >= n) { lb -= ub - (n - 1); ub = n - 1; }
        if (lb < 0) lb = 0;
        bool head = true;
        for (int i = lb; i <= ub; ++i) {
            if (i == x) continue;
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
