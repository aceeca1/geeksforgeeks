#include <cstdio>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d", &n);
        unordered_map<int, int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            ++a[b];
        }
        scanf("%d", &k);
        int ans = 0;
        for (auto& i: a) {
            auto t = a.find(i.first + k);
            if (t == a.end()) continue;
            ans += t->second * i.second;
        }
        printf("%d\n", ans);
    }
    return 0;
}
