#include <cstdio>
#include <vector>
#include <bitset>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, s = 0;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            s += a[i];
        }
        if (s & 1) { printf("NO\n"); continue; }
        s >>= 1;
        bitset<100000> b;
        b.set(0);
        for (int i = 0; i < n; ++i) b |= b << a[i];
        printf("%s\n", b.test(s) ? "YES" : "NO");
    }
    return 0;
}
