#include <cstdio>
#include <cstdint>
#include <cstring>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11];
        scanf("%s", s);
        int n, r;
        sscanf(s, "%d", &n);
        reverse(s, s + strlen(s));
        sscanf(s, "%d", &r);
        int ans = 1;
        for (; r; r >>= 1) {
            if (r & 1) ans = int64_t(ans) * n % 100000007;
            n = int64_t(n) * n % 100000007;
        }
        printf("%d\n", ans);
    }
    return 0;
}
