#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[110000];
        scanf("%s", s);
        int p = -1, z = strlen(s), xy = 0, yx = 0, xyx = 0;
        for (int i = 0; i <= z; ++i) if (p == -1) {
            if (s[i] == 'X' || s[i] == 'Y') p = i;
        } else if (s[i] + s[i - 1] != 'X' + 'Y') {
            if (i - p >= 5) xy = yx = 1;
            else if (i - p >= 3) ++xyx;
            else if (i - p >= 2)
                if (s[i - 1] == 'Y') xy = 1;
                else yx = 1;
            if (s[i] == 'X' || s[i] == 'Y') p = i; else p = -1;
        }
        printf("%s\n", xy + yx + xyx >= 2 ? "YES" : "NO");
    }
    return 0;
}
