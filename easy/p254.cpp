#include <cstdio>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        unordered_map<int, int> a;
        for (int i = 0; i < n; ++i) {
            int b;
            scanf("%d", &b);
            ++a[b];
        }
        int ans = n * (n - 1) >> 1;
        for (auto& i: a)
            ans -= i.second * (i.second - 1) >> 1;
        printf("%d\n", ans);
    }
    return 0;
}
