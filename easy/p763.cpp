#include <cstdio>
#include <vector>
#include <utility>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x = 0;
        scanf("%d", &n);
        vector<int> a(n + 2);
        for (int i = 0; i < n + 2; ++i) {
            scanf("%d", &a[i]);
            x ^= a[i];
        }
        for (int i = 1; i <= n; ++i) x ^= i;
        x &= -x;
        int x1 = 0, x2 = 0;
        for (int i = 0; i < n + 2; ++i)
            if (a[i] & x) x1 ^= a[i];
            else x2 ^= a[i];
        for (int i = 1; i <= n; ++i)
            if (i & x) x1 ^= i;
            else x2 ^= i;
        for (int i = 0; i < n + 2; ++i) {
            if (a[i] == x1) break;
            if (a[i] == x2) { swap(x1, x2); break; }
        }
        printf("%d %d\n", x1, x2);
    }
    return 0;
}
