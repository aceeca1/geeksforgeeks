#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        if (n < 2) { printf("0\n"); continue; }
        int min = a[0], max = a[0];
        for (int i: a) {
            if (i < min) min = i;
            if (i > max) max = i;
        }
        if (min == max) return 0;
        int k = (max - min + n - 2) / (n - 1);
        vector<pair<int, int>> b(
            (max - min) / k + 1, {0x7fffffff, 0x80000000});
        for (int i: a) {
            int p = (i - min) / k;
            int &minP = b[p].first, &maxP = b[p].second;
            if (i < minP) minP = i;
            if (i > maxP) maxP = i;
        }
        int ans = 0;
        max = 0x80000000;
        for (int i = 0; i < b.size(); ++i) {
            if (b[i].first != 0x7fffffff) {
                int k = b[i].first - max;
                if (k > ans) ans = k;
            }
            if (b[i].second > max) max = b[i].second;
        }
        printf("%d\n", ans);
    }
    return 0;
}
