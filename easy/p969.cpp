#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        double ans = 0;
        while (n--) {
            double a;
            scanf("%lf", &a);
            ans += log(a);
        }
        printf("%d\n", int(exp(fmod(ans + 1e-12, log(10)))));
    }
    return 0;
}
