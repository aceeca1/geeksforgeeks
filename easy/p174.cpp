#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int x, y;
        scanf("%d%d", &x, &y);
        vector<int> a1(x), a2(y), a(x + y);
        for (int i = 0; i < x; ++i) scanf("%d", &a1[i]);
        for (int i = 0; i < y; ++i) scanf("%d", &a2[i]);
        merge(a1.rbegin(), a1.rend(), a2.rbegin(), a2.rend(), a.rbegin());
        bool head = true;
        for (int i = 0; i < a.size(); ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%d", a[i]);
        }
        printf("\n");
    }
    return 0;
}
