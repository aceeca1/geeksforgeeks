// Almost the same as p882
#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, s;
        scanf("%d%d", &n, &s);
        if (s > n * 9) {
            printf("-1\n");
            continue;
        }
        for (; s >= 9; s -= 9) { putchar('9'); --n; }
        if (s) { putchar('0' + s); --n; }
        while (n--) putchar('0');
        printf("\n");
    }
    return 0;
}
