#include <cstdio>
#include <cstring>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    char s[110000];
    fgets(s, 110000, stdin);
    while (t--) {
        fgets(s, 110000, stdin);
        int z = strlen(s), p = 0, q = 0;
        for (;;) {
            auto v = strstr(s + p, "/*");
            q = v ? v - s : z;
            const char *close = "*/";
            v = strstr(s + p, "//");
            int q1 = v ? v - s : z;
            if (q1 < q) { q = q1; close = "\\n"; }
            s[q] = 0;
            printf("%s", s + p);
            if (q == z) break;
            p = q + 2;
            q = strstr(s + p, close) - s;
            p = q + 2;
        }
    }
    return 0;
}
