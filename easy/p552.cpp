#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, x, i = 0;
        scanf("%d%d", &n, &x);
        unordered_set<int> a;
        for (; i < n; ++i) {
            int b;
            scanf("%d", &b);
            if (a.count(x - b)) break;
            a.emplace(b);
        }
        printf("%s\n", i == n ? "No" : "Yes");
        for (++i; i < n; ++i) scanf("%*d");
    }
    return 0;
}
