#include <cstdio>
#include <cstring>
#include <cctype>
using namespace std;

int numlt(const char* s1, int z1, const char* s2, int z2) {
    return z1 < z2 || z1 == z2 && strncmp(s1, s2, z1) < 0;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        char s[11000];
        scanf(" %[^\n]", s);
        int z = strlen(s), maxP = 0, maxZ = 0, p = -1;
        bool nine = false;
        for (int i = 0; i <= z; ++i)
            if (isdigit(s[i]) && (s[i] != '0' || p != -1)) {
                if (p == -1) p = i;
                if (s[i] == '9') nine = true;
            } else if (p != -1) {
                int cZ = i - p;
                if (!nine && numlt(s + maxP, maxZ, s + p, cZ)) {
                    maxP = p;
                    maxZ = cZ;
                }
                p = -1;
                nine = false;
            }
        s[maxP + maxZ] = 0;
        printf("%s\n", s + maxP);
    }
    return 0;
}
