#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m = 0;
        scanf("%d", &n);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            if (a[i] > m) m = a[i];
        }
        m = 1 << (ilogb(m + 2) + 1);
        vector<int> b(m + m);
        int ans = 0;
        for (int i = 0; i < n; ++i)
            for (int k = a[i] + m; k > 1; k >>= 1) {
                if (~k & 1) ans += b[k + 1];
                ++b[k];
            }
        printf("%d\n", ans);
    }
    return 0;
}
