#include <cstdio>
#include <unordered_map>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k, ans = 0;
        scanf("%d%d", &n, &k);
        unordered_map<int, int> a;
        while (n--) {
            int b;
            scanf("%d", &b);
            auto t = a.find(k - b);
            if (t != a.end()) ans += t->second;
            ++a[b];
        }
        printf("%d\n", ans);
    }
    return 0;
}
