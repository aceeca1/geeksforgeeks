#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        int64_t pp = 1;
        vector<int64_t> p(n);
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            p[i] = i ? p[i - 1] * a[i - 1] : 1;
        }
        for (int i = n - 1; i >= 0; --i) {
            p[i] *= pp;
            pp *= a[i];
        }
        bool head = true;
        for (int i = 0; i < n; ++i) {
            if (!head) putchar(' ');
            head = false;
            printf("%" PRId64, p[i]);
        }
        printf("\n");
    }
    return 0;
}
