#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        int v, c = 0;
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            if (c && v == a[i]) ++c;
            else if (!c) v = a[i], ++c;
            else --c;
        }
        c = 0;
        for (int i = 0; i < n; ++i) c += v == a[i];
        if (c <= n >> 1) printf("NO Majority Element\n");
        else printf("%d\n", v);
    }
    return 0;
}
