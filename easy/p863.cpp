#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        int a = 1, len = 1;
        while (a) {
            a = (a * 10 + 1) % n;
            ++len;
        }
        printf("%d\n", len);
    }
    return 0;
}
