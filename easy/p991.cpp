#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        if (!n) { printf("10\n"); continue; }
        if (n == 1) { printf("11\n"); continue; }
        string s;
        int i = 9;
        for (int i = 9; i >= 2; --i) {
            while (!(n % i)) { s += '0' + i; n /= i; }
            if (n == 1) break;
        }
        if (n != 1) printf("-1\n");
        else {
            if (s.size() < 2) s += '1';
            reverse(s.begin(), s.end());
            printf("%s\n", s.c_str());
        }
    }
    return 0;
}
