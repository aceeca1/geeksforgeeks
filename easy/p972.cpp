#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        long double a, b, m, n;
        scanf("%Lf%Lf%Lf%Lf", &a, &b, &m, &n);
        auto ab = log(a) * b;
        auto mn = log(m) * n;
        printf("%d\n", ab > mn ? 1 : ab < mn ? 0 : -1);
    }
    return 0;
}
