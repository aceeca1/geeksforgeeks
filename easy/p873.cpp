#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        vector<int> a(n);
        for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
        sort(a.begin(), a.end());
        int ans1 = 0, ans2 = 0;
        k = (n + k) / (k + 1);
        for (int i = 0; i < k; ++i) ans1 += a[i];
        for (int i = n - 1; i >= n - k; --i) ans2 += a[i];
        printf("%d %d\n", ans1, ans2);
    }
    return 0;
}
