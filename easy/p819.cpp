#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, m = 0x7fffffff;
        scanf("%d", &n);
        while (n--) {
            int a;
            scanf("%d", &a);
            if (a < m) m = a;
        }
        printf("%d\n", m);
    }
    return 0;
}
